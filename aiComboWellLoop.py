# from AIComboWell2 import *
from AIComboWell3 import *
import timeit

def AILoopComboWellTest(heuristic):
    # print(heuristic)
    this_pieces = 40 # enough pieces not to top out
    stone_y = 0 # dont change this

    total_score = 0
    combo_threshold = 10

    # games = len(all_sequences)
    games = 1


    for i in range(games):
        # print(i)
        sequence = Logic.c2_sequence()
        # sequence = all_sequences[i]
        # sequence = "ILIZZSOSLSOTILSOLSLTSZSLIOZJOLJLOTLZTJOJJSTL"
        past_index = 0
        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)

        max_comboable_lines = 0

        board = Logic.new_board()

        stone = Logic.tetris_shapes[stone_index]
        score = 0

        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)


            all_moves = f_evaluate_combowell(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            # if h_death(board) != 0:
            #     total_score = -999999
            #     break

            well_prop = h_calc_combowell(board)
            if well_prop[0] > max_comboable_lines and well_prop[1] >= 6:
                max_comboable_lines = well_prop[0]




            #score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]

        total_score += max_comboable_lines
        # if max_comboable_lines >= combo_threshold:
        #     total_score += 1



        # well_prop = h_calc_combowell(board)
        # total_score += well_prop[0]


    # return float(total_score / games)
    return float(total_score)
    # return score


def AILoopComboWell(heuristic):
    # print(heuristic)
    this_pieces = 40 # enough pieces not to top out
    stone_y = 0 # dont change this

    total_score = 0
    combo_threshold = 10
    all_sequences1 = ["ILOTIOTZISOZJOSJLITISTLOOIOLIZSTIOILILZLOZOT",
                          "ITLJTLSIZOTJOLOJSOILSOISTZOZLTOSZTTJTLTIOJLI",
                          "ILZTILZSOSTIIJJSLOZJIOTIJTZSZZJOLTOJLISOIZJI",
                          "ILISLTOJZIOZJTZSJOZTIZTZITOSLZJTJOIIZOZTILSL",
                          "IZSLZLSJTZIOSJZOTIJSOZJZJSOTTSZLSZJZSJLIOLSO",
                          "ITIOSJZLSSTJSOIZSTLIZLOISTJIOITJTSTLSJSISZJT",
                          "ISTJOISIOSLZLSZIJOIITIZTSZTJSZJLOSZJLTZOIOTS",
                          "IIJOILSJTZIJSJITJSJISOTLIJLOJIJSZOOIJITTSZOL",
                          "IOITTLOLJILTSOZJIJLISTSJLOJILJZLIZSLSZOJSOTZ",
                          "IITLZSILTJIOILOJTOSZSLJLJZIZSLSJTLOIZSLJOTIS",
                          "IJSLISZOZJOSLZOJITLJTLOJIZITJOILOJOISTJOSZTZ",
                          "ILTTJJLTSLZTTLJISTJIOZTOLSTLOSITZITSOTJTZIJS",
                          "ITIOJSOTZOZLSOIJTLSTOZJSLZLTLJSZOTZSLJZSIJIO",
                          "IZTSOZOLLILJZIOJSZTLZSOJLISJOJLILJOTOLTSZLOT",
                          "ITZSOIOOJZTZITJTSTSJSIZZJSLTJLTITOJISTOIJLTO",
                          "ITOILILZTOTLIJSSJZSOTJSIJSSLIZZLJSZTISLSLITS",
                          "IOIJOSZOLJOLZJZJLOZSJSLJITJLSLJIZSTZIJZJSLIJ",
                          "IZLTILSJLJSOLSOZSJOSSZOITZOILSZILOIIJOJSLZLO",
                          "IZJITZTZOZLJIJOSTJZSTZOZJJZSILSOZLJISTSJSIZL",
                          "ITSZTOIOLTLSISOISTTSLSOTZTLIZLTSLTZLOZITOSLZ",
                          "IZTSITSZIOSZSJISITJLJTJZLOLTOISTIOTOTJSJLSIO",
                          "IOZITZLSJZLJLILSTJZSZIZSOLJZJLSZTZLTJOTZSTOT",
                          "IZOSZITZSOITSSTZTOIJILJTOSIZOTIOJZSZSTLISSIO",
                          "IZJLZLSITOSLZITLSOJLTZTLJOJILTZIOJLTJLZSLIOT",
                          "IZOSTZIZJSOZOJSOTLSTSZJLTZTZOLZLOJLSOJSOSILO",
                          "ISOIZJTLOLSLSZOSZLSJILOLTJOSSOLOTZOTSLSJLOLZ",
                          "IZTLSZOLZSZTJIZLTOJLSJOSOILISOOITJOILZJTLOTL",
                          "IJTTIZIJIOTILISJOJLJZTOOSLTOJTJLISTSIJZTZITS",
                          "ILITLTSIIJSZOJOISLOJJOIOSOISSOZSZSOOJLOZISJZ",
                          "IZTIZSOSJZSTZTOISJLILSZOILSZOJIOIZOSOLJLZLZS",
                          "ILTZTOTZSITSOZOJOSLTZOISOZZJZIZJTZLSJLITOSIL",
                          "IJILTZOJIOZOSJOJOZJZSLJSOOLSOLTLITSLTOJLLSOZ",
                          "IOJLISLSZOIZZOZJLOZTOTILZJLZSILJTZZLOITJSIOJ"]


    all_sequences2 = ["ITIZJOSZIOISJSTZLOZILTJJLSLJTZLIJSLIZSIJOLIJ",
                        "ITSILLITTLOTSZJLOZIOZLJLTSZSILJTSLJSTSLIOIOS",
                        "IJTJLOSTOILTZTJOTZSTLIISTIOZLOLOLLISITLSOLJJ",
                        "IITTOTZILOZTLOITJOSTSZJITSLSLSOJOTOTLZSLOJIJ",
                        "IIJLJSOTLJISLSTLZTJZOIJOJILTSZTIJTSZJOSLZSIL",
                        "IIIZLSOTIJLZZSTZOJLOLTTJLZISOLTSOTOZITIZLOSI",
                        "ITIZILSJSLZTJSTOZJTIZITZLSJTSZISTZLOOZLIZIOI",
                        "IJTIZJLZOJSISJLZIOSOLZJLOSLOSZSOLZIZSLSOLSIZ",
                        "IOSOLJSZSLJSIOOLJSOTZLOJTIJZLJOLJSTSJIOTJSTZ",
                        "ISTOLTSOTZJZISZITSTZTLTJZLILZLITZJOSSILJZJLT",
                        "IJTLJJSSIOLJLSISZZIJSZJSZOILJTZTSILOLISZISTS",
                        "ILSIOJZJSTZLZJOTZOLOZISZLZJSLZOIJLITIJLZIJZI",
                        "IZOSJLOSLJTOJZSTSJSTSOJZSISIOJITZLSOLZOISJSL",
                        "IISOLTLZSILZSIJSOIJIZIOJOOSOZLSZIZLIIIOJOZZJ",
                        "ISOJLZOTOZIZIJOJSOJZOOOSTOZOSLJSZZSOZJOJLSZL",
                        "ISOTSZIJITJITSIJLSSTSJSILSJZJTZIJLJLIZZTSISJ",
                        "ILIOITOIZOLJLOJZILZTZSLOSOTOJISLJILOZLTOTIJZ",
                        "IZLJIZSLSTSJSOJSOZSZTLTSOZLISOZSZOSOLILZSLOJ",
                        "ILTZOLZIOZJZTZSOIITZIZIOJTZOSIOTJOSTLITJZJIL",
                        "ILSOJISLJTSTJLZSZITJLOJLZLJZLTJOZIZSJZOJOJJO",
                        "IILSTLLOZIOZZJSZILISLJSLILOLJLSZJOSILJOIOTSZ",
                        "IJZLJTIZOTSLZLSZOJTOZITLOJLTOTSOIZTJLIJIZSOJ",
                        "ISOLTIJZLJSOTILSLLOSJSOOLZJSOLTLZOJILJOLSZTI",
                        "IZJSLTJLOZOLSJSTJSJLZLSOJIZTLTISLOZLTZOSZZOI",
                        "IJSTZSOJTJTZOZTLSILSOJIJLSOJLOIOSLTJLIJZSILT",
                        "ISJOLSJSZJTZJOLJOZSJZOZJTLOZOJZTOLIOJLTOJTTZ",
                        "ILTSJOIJJJOISLOJSOIOJTIJOILTJSLZOSZIJTSJTLJS",
                        "IZLOLTZISZISLISOZSOJLTJZJISZJZOITLSTISJTSOJT",
                        "ISLJZILTJIOTJZOLOJLZOTZSZSJLSJLLSLIIOISZIOTO",
                        "ISTLSJISLTITZLITOIZSOITZSOZSOOITZLJSLJSSOTLS",
                        "ITOIJIITJLISZTZILSJSOJITJLLZILISJLOSLTSLJTSI",
                        "ISIOSTTSTOZJSOSJTILJOSZLJLSZTLTZIJSZOSTOITZL",
                        "IJLZIJLIOZSLISILOLISZJTOLIJSSOJSLTOLZIZTSLIS"]

    all_sequences3 = ["IZOTJISLSITOTSOLJOZTZJZOZJZLOTILTLSITILITISL",
                    "IOZSLOJOLITLJISLTISTJLOZSOJTSILLOSLOJTJZOSJO",
                    "ILOTTSTZILLTISTZTLZTZOTSOLLZOILSLZOJIZITOLOS",
                    "IILJTSZOSTTSJZOJIJLOZIJLZJITTSIZJTLJOTTZTOJI",
                    "IJLSJZLITOIJTTIOZOJSOLIOSOJOZITJIJZTZSZISSZO",
                    "IZJOZSITOJSLZLISTZLJZJTLZIZOJZSJZOIOZSILOZJI",
                    "IOTLZSZIJLZIZTIOJTLOLJOLJILZLJOZSLSTLJSOILOZ",
                    "ISJLSZJIZIJITOSTLIZOLSITJZSTIJLTOTOIZLOSJSTZ",
                    "IITSZLIZOSIJZLTSZILJTSOZILSLSJILSLTJLIJITSZT",
                    "IILSZLOZISOIZTLZLTJISLITJOLZILTIZOIZLTJZZLJS",
                    "ILOJLLTJSIZOISTOITOTLZOSJJOZJOJTZIZLZLZJTJIO",
                    "ILOSSZLSTSZIOSTLOSLTLSLOZZLJZOZSTITZJLJITSLL",
                    "IJZOZISJIJILZILZOIJOZSTOZJSLZTOJZOTZLTSSJOJZ",
                    "ISOZISOZOJLSLSLTSLZSOJLOSLJSZLZOLJOLSJTSLOSZ",
                    "IZJTOIZIJLZIJTJLZTSLTIOLZJTJJSIITZTIOSOLIJLT",
                    "ISZLLIOSOZLSJOISZIJZLLSOTZJIZOOIZTOZJLSTJILZ",
                    "IOZOJLSTSLILSOTLTIOZSOTJSZLOSILJOSTSTISTLSLJ",
                    "IJISJZOZOITZLIZLSILJOZLTZJIJIIOZLTSOTIZOTLSZ",
                    "ISLILSTOZSTOSLZLZLOLSITLOZLIZJTTZLSOLOITOJTO",
                    "IOLOSTZJIZJSIZJLSTIZOTSOLZJOISIJZLOLIJISZOZI",
                    "IIISOTTLTJSLZOSOZZOTLOSTLIZTSIOJSOJTISTOZJIT",
                    "IZSTLJZZSJLJLIJOZJZTZLSJZILIOZJZOTOSTZSTSIOL",
                    "IOLTJTJSOISOZOTJTOIZSLOOZSTIJOSTLZOIJTJIOTIT",
                    "ISTJIOZTSOZTOJSTOZOILJTLZSSZJOLSJSTITJZTILST",
                    "ISLIJITZTLZTJZTTSSOIZTSZSJLZOITZOLJLLSTJLOIO",
                    "IOSTOJLSISOJZILJZOZSLOSIOSJLTOILOZSTLIJSLOTJ",
                    "IJOISLTZJSTOZZTZLZOTZTZLSZZSIZLITSTSTSJZSTTO",
                    "IOSOTZLZTILZSZOJSLITZTLIOTIJJZTSOZJSJZZOLJLI",
                    "IZTJIOZJSILZOJTLJZSIOLOTJSTISTJZIZLJTOTJOTSL",
                    "ISTOJLZJTZOTTZJIZTLIOSTJZSJIJIOTTJOITZLIOZJZ",
                    "ITJIZSISTZOTZZLOSLJOSZZISZLTOSZZOSTLSOJILTZL",
                    "IZLJIZTSLIOJZTISZLZIJTLOTZOTLSTSJILTZSZTJITL",
                    "IZLSIZSTZIZOILZJLIZJLOSOZSOSTSOSJLOIZTLJOZTL"]
    # all_sequences = all_sequences1[-10:] + all_sequences2[-10:] + all_sequences3[-10:]
    all_sequences = all_sequences1 + all_sequences2 + all_sequences3
    games = len(all_sequences)
    # games = 1


    for i in range(games):
        # print(i)
        # sequence = Logic.c2_sequence()
        sequence = all_sequences[i]
        # sequence = "ILIZZSOSLSOTILSOLSLTSZSLIOZJOLJLOTLZTJOJJSTL"
        past_index = 0
        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)

        max_comboable_lines = 0

        board = Logic.new_board()

        stone = Logic.tetris_shapes[stone_index]
        score = 0

        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)


            all_moves = f_evaluate_combowell(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            # if h_death(board) != 0:
            #     total_score = -999999
            #     break

            well_prop = h_calc_combowell(board)
            if well_prop[0] > max_comboable_lines and well_prop[1] >= 6:
                max_comboable_lines = well_prop[0]




            #score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]

        # total_score += max_comboable_lines
        if max_comboable_lines >= combo_threshold:
            total_score += 1



        # well_prop = h_calc_combowell(board)
        # total_score += well_prop[0]


    # return float(total_score / games)
    return float(total_score)
    # return score

def AILoopComboWell2(heuristic):
    # print(heuristic)
    this_pieces = 30 # enough pieces not to top out
    stone_y = 0 # dont change this

    total_score = 0
    combo_threshold = 10
    all_sequences1 = ["ILOTIOTZISOZJOSJLITISTLOOIOLIZSTIOILILZLOZOT",
                          "ITLJTLSIZOTJOLOJSOILSOISTZOZLTOSZTTJTLTIOJLI",
                          "ILZTILZSOSTIIJJSLOZJIOTIJTZSZZJOLTOJLISOIZJI",
                          "ILISLTOJZIOZJTZSJOZTIZTZITOSLZJTJOIIZOZTILSL",
                          "IZSLZLSJTZIOSJZOTIJSOZJZJSOTTSZLSZJZSJLIOLSO",
                          "ITIOSJZLSSTJSOIZSTLIZLOISTJIOITJTSTLSJSISZJT",
                          "ISTJOISIOSLZLSZIJOIITIZTSZTJSZJLOSZJLTZOIOTS",
                          "IIJOILSJTZIJSJITJSJISOTLIJLOJIJSZOOIJITTSZOL",
                          "IOITTLOLJILTSOZJIJLISTSJLOJILJZLIZSLSZOJSOTZ",
                          "IITLZSILTJIOILOJTOSZSLJLJZIZSLSJTLOIZSLJOTIS",
                          "IJSLISZOZJOSLZOJITLJTLOJIZITJOILOJOISTJOSZTZ",
                          "ILTTJJLTSLZTTLJISTJIOZTOLSTLOSITZITSOTJTZIJS",
                          "ITIOJSOTZOZLSOIJTLSTOZJSLZLTLJSZOTZSLJZSIJIO",
                          "IZTSOZOLLILJZIOJSZTLZSOJLISJOJLILJOTOLTSZLOT",
                          "ITZSOIOOJZTZITJTSTSJSIZZJSLTJLTITOJISTOIJLTO",
                          "ITOILILZTOTLIJSSJZSOTJSIJSSLIZZLJSZTISLSLITS",
                          "IOIJOSZOLJOLZJZJLOZSJSLJITJLSLJIZSTZIJZJSLIJ",
                          "IZLTILSJLJSOLSOZSJOSSZOITZOILSZILOIIJOJSLZLO",
                          "IZJITZTZOZLJIJOSTJZSTZOZJJZSILSOZLJISTSJSIZL",
                          "ITSZTOIOLTLSISOISTTSLSOTZTLIZLTSLTZLOZITOSLZ",
                          "IZTSITSZIOSZSJISITJLJTJZLOLTOISTIOTOTJSJLSIO",
                          "IOZITZLSJZLJLILSTJZSZIZSOLJZJLSZTZLTJOTZSTOT",
                          "IZOSZITZSOITSSTZTOIJILJTOSIZOTIOJZSZSTLISSIO",
                          "IZJLZLSITOSLZITLSOJLTZTLJOJILTZIOJLTJLZSLIOT",
                          "IZOSTZIZJSOZOJSOTLSTSZJLTZTZOLZLOJLSOJSOSILO",
                          "ISOIZJTLOLSLSZOSZLSJILOLTJOSSOLOTZOTSLSJLOLZ",
                          "IZTLSZOLZSZTJIZLTOJLSJOSOILISOOITJOILZJTLOTL",
                          "IJTTIZIJIOTILISJOJLJZTOOSLTOJTJLISTSIJZTZITS",
                          "ILITLTSIIJSZOJOISLOJJOIOSOISSOZSZSOOJLOZISJZ",
                          "IZTIZSOSJZSTZTOISJLILSZOILSZOJIOIZOSOLJLZLZS",
                          "ILTZTOTZSITSOZOJOSLTZOISOZZJZIZJTZLSJLITOSIL",
                          "IJILTZOJIOZOSJOJOZJZSLJSOOLSOLTLITSLTOJLLSOZ",
                          "IOJLISLSZOIZZOZJLOZTOTILZJLZSILJTZZLOITJSIOJ"]


    all_sequences2 = ["ITIZJOSZIOISJSTZLOZILTJJLSLJTZLIJSLIZSIJOLIJ",
                        "ITSILLITTLOTSZJLOZIOZLJLTSZSILJTSLJSTSLIOIOS",
                        "IJTJLOSTOILTZTJOTZSTLIISTIOZLOLOLLISITLSOLJJ",
                        "IITTOTZILOZTLOITJOSTSZJITSLSLSOJOTOTLZSLOJIJ",
                        "IIJLJSOTLJISLSTLZTJZOIJOJILTSZTIJTSZJOSLZSIL",
                        "IIIZLSOTIJLZZSTZOJLOLTTJLZISOLTSOTOZITIZLOSI",
                        "ITIZILSJSLZTJSTOZJTIZITZLSJTSZISTZLOOZLIZIOI",
                        "IJTIZJLZOJSISJLZIOSOLZJLOSLOSZSOLZIZSLSOLSIZ",
                        "IOSOLJSZSLJSIOOLJSOTZLOJTIJZLJOLJSTSJIOTJSTZ",
                        "ISTOLTSOTZJZISZITSTZTLTJZLILZLITZJOSSILJZJLT",
                        "IJTLJJSSIOLJLSISZZIJSZJSZOILJTZTSILOLISZISTS",
                        "ILSIOJZJSTZLZJOTZOLOZISZLZJSLZOIJLITIJLZIJZI",
                        "IZOSJLOSLJTOJZSTSJSTSOJZSISIOJITZLSOLZOISJSL",
                        "IISOLTLZSILZSIJSOIJIZIOJOOSOZLSZIZLIIIOJOZZJ",
                        "ISOJLZOTOZIZIJOJSOJZOOOSTOZOSLJSZZSOZJOJLSZL",
                        "ISOTSZIJITJITSIJLSSTSJSILSJZJTZIJLJLIZZTSISJ",
                        "ILIOITOIZOLJLOJZILZTZSLOSOTOJISLJILOZLTOTIJZ",
                        "IZLJIZSLSTSJSOJSOZSZTLTSOZLISOZSZOSOLILZSLOJ",
                        "ILTZOLZIOZJZTZSOIITZIZIOJTZOSIOTJOSTLITJZJIL",
                        "ILSOJISLJTSTJLZSZITJLOJLZLJZLTJOZIZSJZOJOJJO",
                        "IILSTLLOZIOZZJSZILISLJSLILOLJLSZJOSILJOIOTSZ",
                        "IJZLJTIZOTSLZLSZOJTOZITLOJLTOTSOIZTJLIJIZSOJ",
                        "ISOLTIJZLJSOTILSLLOSJSOOLZJSOLTLZOJILJOLSZTI",
                        "IZJSLTJLOZOLSJSTJSJLZLSOJIZTLTISLOZLTZOSZZOI",
                        "IJSTZSOJTJTZOZTLSILSOJIJLSOJLOIOSLTJLIJZSILT",
                        "ISJOLSJSZJTZJOLJOZSJZOZJTLOZOJZTOLIOJLTOJTTZ",
                        "ILTSJOIJJJOISLOJSOIOJTIJOILTJSLZOSZIJTSJTLJS",
                        "IZLOLTZISZISLISOZSOJLTJZJISZJZOITLSTISJTSOJT",
                        "ISLJZILTJIOTJZOLOJLZOTZSZSJLSJLLSLIIOISZIOTO",
                        "ISTLSJISLTITZLITOIZSOITZSOZSOOITZLJSLJSSOTLS",
                        "ITOIJIITJLISZTZILSJSOJITJLLZILISJLOSLTSLJTSI",
                        "ISIOSTTSTOZJSOSJTILJOSZLJLSZTLTZIJSZOSTOITZL",
                        "IJLZIJLIOZSLISILOLISZJTOLIJSSOJSLTOLZIZTSLIS"]

    all_sequences3 = ["IZOTJISLSITOTSOLJOZTZJZOZJZLOTILTLSITILITISL",
                    "IOZSLOJOLITLJISLTISTJLOZSOJTSILLOSLOJTJZOSJO",
                    "ILOTTSTZILLTISTZTLZTZOTSOLLZOILSLZOJIZITOLOS",
                    "IILJTSZOSTTSJZOJIJLOZIJLZJITTSIZJTLJOTTZTOJI",
                    "IJLSJZLITOIJTTIOZOJSOLIOSOJOZITJIJZTZSZISSZO",
                    "IZJOZSITOJSLZLISTZLJZJTLZIZOJZSJZOIOZSILOZJI",
                    "IOTLZSZIJLZIZTIOJTLOLJOLJILZLJOZSLSTLJSOILOZ",
                    "ISJLSZJIZIJITOSTLIZOLSITJZSTIJLTOTOIZLOSJSTZ",
                    "IITSZLIZOSIJZLTSZILJTSOZILSLSJILSLTJLIJITSZT",
                    "IILSZLOZISOIZTLZLTJISLITJOLZILTIZOIZLTJZZLJS",
                    "ILOJLLTJSIZOISTOITOTLZOSJJOZJOJTZIZLZLZJTJIO",
                    "ILOSSZLSTSZIOSTLOSLTLSLOZZLJZOZSTITZJLJITSLL",
                    "IJZOZISJIJILZILZOIJOZSTOZJSLZTOJZOTZLTSSJOJZ",
                    "ISOZISOZOJLSLSLTSLZSOJLOSLJSZLZOLJOLSJTSLOSZ",
                    "IZJTOIZIJLZIJTJLZTSLTIOLZJTJJSIITZTIOSOLIJLT",
                    "ISZLLIOSOZLSJOISZIJZLLSOTZJIZOOIZTOZJLSTJILZ",
                    "IOZOJLSTSLILSOTLTIOZSOTJSZLOSILJOSTSTISTLSLJ",
                    "IJISJZOZOITZLIZLSILJOZLTZJIJIIOZLTSOTIZOTLSZ",
                    "ISLILSTOZSTOSLZLZLOLSITLOZLIZJTTZLSOLOITOJTO",
                    "IOLOSTZJIZJSIZJLSTIZOTSOLZJOISIJZLOLIJISZOZI",
                    "IIISOTTLTJSLZOSOZZOTLOSTLIZTSIOJSOJTISTOZJIT",
                    "IZSTLJZZSJLJLIJOZJZTZLSJZILIOZJZOTOSTZSTSIOL",
                    "IOLTJTJSOISOZOTJTOIZSLOOZSTIJOSTLZOIJTJIOTIT",
                    "ISTJIOZTSOZTOJSTOZOILJTLZSSZJOLSJSTITJZTILST",
                    "ISLIJITZTLZTJZTTSSOIZTSZSJLZOITZOLJLLSTJLOIO",
                    "IOSTOJLSISOJZILJZOZSLOSIOSJLTOILOZSTLIJSLOTJ",
                    "IJOISLTZJSTOZZTZLZOTZTZLSZZSIZLITSTSTSJZSTTO",
                    "IOSOTZLZTILZSZOJSLITZTLIOTIJJZTSOZJSJZZOLJLI",
                    "IZTJIOZJSILZOJTLJZSIOLOTJSTISTJZIZLJTOTJOTSL",
                    "ISTOJLZJTZOTTZJIZTLIOSTJZSJIJIOTTJOITZLIOZJZ",
                    "ITJIZSISTZOTZZLOSLJOSZZISZLTOSZZOSTLSOJILTZL",
                    "IZLJIZTSLIOJZTISZLZIJTLOTZOTLSTSJILTZSZTJITL",
                    "IZLSIZSTZIZOILZJLIZJLOSOZSOSTSOSJLOIZTLJOZTL"]
    all_sequences = all_sequences1[-10:] + all_sequences2[-10:] + all_sequences3[-10:]
    # all_sequences = all_sequences1 + all_sequences2 + all_sequences3
    games = len(all_sequences)
    # games = 1


    for i in range(games):
        # print(i)
        # sequence = Logic.c2_sequence()
        sequence = all_sequences[i]
        # sequence = "ILIZZSOSLSOTILSOLSLTSZSLIOZJOLJLOTLZTJOJJSTL"
        past_index = 0
        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)

        max_comboable_lines = 0

        board = Logic.new_board()

        stone = Logic.tetris_shapes[stone_index]
        score = 0

        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)


            all_moves = f_evaluate_combowell(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)


            # if h_death(board) != 0:
            #     total_score = -999999
            #     break
            # well_prop = bot_switch_heuristics(columns, board)

            # if well_prop[1] > max_comboable_lines:
            #     max_comboable_lines = well_prop[1]



            #score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]

        # total_score += max_comboable_lines
        # if max_comboable_lines >= combo_threshold:
        #     total_score += 1

        columns = getAllColumns(board)
        well_prop = bot_switch_heuristics(columns, board)
        total_score += well_prop[1]
        # well_prop = h_calc_combowell(board)
        #


    return float(total_score / games)
    # return float(total_score)

# a = AILoopComboWell([-0.4524333394072162, 0.41820889108478343, -2.639218614679871, 4.931321971799857, -2.2121372198875324, -2.8057247687075835, -2.9151152772515276, -6.406792761595582, 1.0])
# print(a)


# start = timeit.default_timer()
# # #
# print(AILoopComboWell2([-1.8445273002780116, -1.4845498199222806, -1.9846761182152672, 6.993202685179536, -1.8382654021577496, -2.5965827192343127, 2.1299520184997416, -6.413076774171903, -3.231461518594695, -0.06634672244458084, -0.566026794084606, 1.1978121716839647, 1.0]))
# stop = timeit.default_timer()
# print('Time: ', stop - start)
