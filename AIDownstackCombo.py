import Logic
from random import randrange as rand
from random import sample
import GUI
import numpy as np
#from sklearn import preprocessing
from operator import xor
from math import factorial
# allow the bot to create a lip when stacking high, calculate number of 3+ sized wells
# evaluation_function = [
#     'number_of_holes',
#     'parity',
#     'bumpiness',
#     'hole_wells',
#     'stack_height',
#     'row_trans_above_hole_1',
#     'cells_above_garbage_hole_1',
#     'cells_above_garbage_hole_2'
#
#     # 'cells_above_pieces_hole_1',
#     # 'cells_above_pieces_hole_2'
# ]

#
# evaluation_function = [
#     'number_of_holes',
#     'parity',
#     'bumpiness',
#     'hole_wells',
#     'stack_height',
#     'holes_in_stack',
#     'row_trans_above_hole_1',
#     'cells_above_garbage_hole_1',
#     'cells_above_garbage_hole_2'
#
#     # 'cells_above_pieces_hole_1',
#     # 'cells_above_pieces_hole_2'
# ]

evaluation_function = [
    'number_of_holes',
    'parity',
    'bumpiness',
    'hole_wells',
    'stack_height',
    'holes_in_stack',
    'sum_wells_above_three',
    'current_combo_score',
    'row_trans_above_hole_1',
    'cells_above_garbage_hole_1',
    'cells_above_garbage_hole_2'

    # 'cells_above_pieces_hole_1',
    # 'cells_above_pieces_hole_2'
]

#number of row trans at surface
#number of row trans above 2 highest holes
def getColumn(board, column):
    return [board[i][column] for i in range(len(board))]


def getAllColumns(board):
    return [getColumn(board, i) for i in range(len(board[0]))]

def highest_cell_above_hole(col):
    for i in range(len(col)):
        if col[i] !=  0:
            return i
    else:
        return len(col)


def all_highest_holes(data):
    # if garbage == True:
    #     data = [item for item in data if highest_garbage_hole_index[0] <= item[0]]
    # elif garbage == False:
    #     data = [item for item in data if highest_garbage_hole_index[0] > item[0]]
    count = 0
    max_depth = 2
    cells_above_holes = [0 for i in range(max_depth)]
    # cells_above_holes = [0,0]
    for i in range(len(data)):
        if i != 0 and data[i][0] != data[i-1][0]:
            count += 1
        if count == max_depth: break
        cells_above_holes[count] += data[i][1]

    return cells_above_holes

def total_hole_row_trans(data):
    # if garbage == True:
    #     data = [item for item in data if highest_garbage_hole_index[0] <= item[0]]
    # elif garbage == False:
    #     data = [item for item in data if highest_garbage_hole_index[0] > item[0]]
    count = 0
    max_depth = 1
    total_row_trans = [0 for i in range(max_depth)]
    # cells_above_holes = [0,0]
    for i in range(len(data)):
        if i != 0 and data[i][0] != data[i-1][0]:
            count += 1
        if count == max_depth: break
        total_row_trans[count] += len(data[i][1])

    return total_row_trans

def one_column_sizes(columns):
    tetris_column_sizes = []
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        column_size = 0
        for j in reversed(range(0, matrix_height)):

            if i == Logic.config['cols'] - 1:
                if columns[i][j] == 0 and columns[i - 1][j] != 0:
                    column_size += 1
            elif i == 0:
                if columns[i][j] == 0 and columns[i + 1][j] != 0:
                    column_size += 1
            else:
                if columns[i][j] == 0 and columns[i + 1][j] != 0 and columns[i - 1][j] != 0:
                    column_size += 1
        tetris_column_sizes.append((column_size, i))
    return tetris_column_sizes


def calc_well_cells(board, x, y):
    total = 0
    first_garbage = False
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x > -1:
        if y == 0:
            if board[x][y + 1] != 0:
                total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:
            if board[x][y - 1] != 0:
                total += 1
            else:
                break
        else:
            if board[x][y + 1] != 0 and board[x][y - 1] != 0 :
                total += 1
            else:
                break
        x -= 1
    return total

def hole_well_cells(board, x, y):
    total = 0
    first_garbage = False
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x < Logic.config['rows']:
        if board[x][y] == 0:
            total += 1
            x += 1
        else:
            break
    return total

def garbage_number(board):
    for i in range(len(board)):
        if board[i].count(8) == Logic.config['cols'] - 1:
            return len(board) - i
    else:
        return 0

def row_transition_indexes(row):
    # return index of all row transitions in row
    index_list = []
    new_row = [1] + row + [1]
    for y in range(len(new_row)-1):
        index_list.append(y - 1) if xor(new_row[y] != 0, new_row[y + 1] != 0) else None
    return index_list

def combo_score(line_clear, combo_timer, stack_holes):
    assert (line_clear <= 4)
    score = 0
    if combo_timer == 1:
        if line_clear == 0:
            score = -2
        elif line_clear == 1:
            score = 6
        elif line_clear == 2:
            score = 3
        elif line_clear == 3:
            score = -1
        elif line_clear == 4:
            score = -4

    if combo_timer == 0:
        if line_clear == 1:
            score = -2
        elif line_clear == 2:
            score = 3
        elif line_clear == 3:
            score = 6
        elif line_clear == 4:
            score = 8

    return score-(stack_holes*2)

def stack_hole_number(columns, board):

    highest_garbage_hole_index = [Logic.config['rows'], 0]
    all_stack_holes = []

    matrix_height = Logic.config['rows']

    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height
        column_hole = 0
        stack_hole = 0


        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            if columns[i][j] != 0:
                if highest_cell_index == matrix_height :
                    highest_cell_index = j  # only change this at 1st iteration

            if highest_cell_index < matrix_height and j != matrix_height - 1:
                if board[j + 1][i] == 0 and board[j][i] != 0:
                    if board[j + 1].count(8) != Logic.config['cols'] - 1:
                        stack_hole += 1

        else:
            all_stack_holes.append(stack_hole)

    return sum(all_stack_holes)


def h_eval(columns, board, line_clear, combo_timer):
    black_squares = 0
    white_squares = 0
    bumpiness = 0
    bumpiness_minus_minimum = 0
    heights_list = []
    cells_above_holes = []
    column_holes = []
    highest_garbage_hole_index = [Logic.config['rows'], 0]
    all_well_cells = []
    all_hole_row_trans = []
    all_stack_holes = []
    all_surface_well_cells = []
    # highest_well = 0

    matrix_height = Logic.config['rows']

    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height
        column_hole = 0
        stack_hole = 0


        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            # if board[j].count(0) == 1 and highest_well == 0:
            #     highest_well = j

            if columns[i][j] != 0:
                if highest_cell_index == matrix_height :
                    surface_well_cells = calc_well_cells(board, j - 1, i)
                    all_surface_well_cells.append(surface_well_cells)
                    highest_cell_index = j  # only change this at 1st iteration
                    heights_list.append(matrix_height - j)  # for column height variance

                    if i % 2 == 0:
                        if j % 2 == 0:
                            if columns[i][j] != 0: black_squares += 1
                        if j % 2 != 0:
                            if columns[i][j] != 0: white_squares += 1
                    if i % 2 != 0:
                        if j % 2 != 0:
                            if columns[i][j] != 0: black_squares += 1
                        if j % 2 == 0:
                            if columns[i][j] != 0: white_squares += 1

            if highest_cell_index < matrix_height and j != matrix_height - 1:
                if board[j + 1][i] == 0 and board[j][i] != 0:
                    row_trans_above_hole = row_transition_indexes(board[j])
                    all_hole_row_trans.append((j + 1,row_trans_above_hole))
                    well_cells = hole_well_cells(board, j + 1, i)
                    # print(j + 1, i)
                    all_well_cells.append(well_cells)
                    column_hole += 1
                    hole_cell_count = j+1 - highest_cell_above_hole(columns[i][:j+1])
                    cells_above_holes.append((j + 1,hole_cell_count))
                    if board[j + 1].count(8) != Logic.config['cols'] - 1:
                        stack_hole += 1

        else:
            if highest_cell_index == matrix_height :  # colmun of empty cells
                heights_list.append(0)  # add 0 to heights if no block detected
                if i % 2 == 0:
                    black_squares += 1
                if i % 2 != 0:
                    white_squares += 1
                inempty_well_cells = calc_well_cells(board, Logic.config['rows'] - 1, i)
                all_surface_well_cells.append(inempty_well_cells)
            column_holes.append(column_hole)
            all_stack_holes.append(stack_hole)



    for x in range(Logic.config['cols'] - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])




    # print(all_surface_well_cells)
    sum_wells_above_three = sum([x for x in all_surface_well_cells if x > 2])
    stack_height = highest_garbage_hole_index[0] - (matrix_height - max(heights_list))
    sorted_all_hole_row_trans = sorted(all_hole_row_trans)


    # height_spread = max(heights_list) - min(heights_list)
    # print(sum_wells_above_three)




    # print(test)
    # print(total_hole_row_trans(sorted_all_hole_row_trans))
    punish_hole_wells = [x for x in all_well_cells if x > 1 ]
    # print(all_well_cells)


    # heights_list.remove(min(heights_list))
    #
    # for x in range(Logic.config['cols'] - 2):
    #     bumpiness_minus_minimum += abs(heights_list[x] - heights_list[x + 1])
    # print(all_stack_holes)
    parity = abs(black_squares - white_squares)
    assert (black_squares + white_squares) == 10

    sorted_cell_above_holes = sorted(cells_above_holes)
    # print(sorted_cell_above_holes)

    all_cell_above_holes = all_highest_holes(sorted_cell_above_holes)

    current_combo_score = combo_score(line_clear, combo_timer, sum(all_stack_holes))

    return [sum(column_holes),
               parity ,
            bumpiness,
            sum(punish_hole_wells),
            stack_height,
            sum(all_stack_holes),
            sum_wells_above_three,
            current_combo_score
            ] \
           + total_hole_row_trans(sorted_all_hole_row_trans) \
           + all_cell_above_holes













def h_death(board):

    if board[0].count(0) != Logic.config['cols']:
        return -9999999
    else:
        return 0


def f_score(line_clear, args):
    score, time_left, counter = args

    if time_left <= 0:
        counter = 0
        time_left = 0

    counter_to_lineclear = [0, 0, 0, 0, 0, 0, 1, 8, 80, 500, 5000, 30000, 30000, 30000, 30000]
    # counter_to_lineclear = [0, 0, 0, 0, 1, 1, 2, 3, 4, 6, 10, 30000, 30000, 30000, 30000]
    base_time = [0, 6, 4, 2, 1, 0, -4, -4, -4, -6, -7, -8, -9, -10, -10]
    bonus_time = [0, 12, 6, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    loss_time = 16

    if counter > 14:
        counter = 14
    if line_clear == 0:
        if base_time[counter] < 0:
            time_left -= loss_time - base_time[counter]
        else:
            time_left -= loss_time
        return score, time_left, counter
    else:
        counter += 1
        time_left = time_left + base_time[counter] + (bonus_time[counter] * line_clear)
        score += counter_to_lineclear[counter]
        return score, time_left, counter




def f_compute_score(heuristics, board, line_clear, counter, combo_timer):
    # print(board)
    columns = getAllColumns(board)
    eval_data = h_eval(columns, board, line_clear, combo_timer)


    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)

    # new_line_clear = 5 if line_clear >= 1 else 0
    # heuristics[-2] = 0
    # values = eval_data + [line_clear] + [counter] + [h_death(board)]
    values = eval_data + [h_death(board)]

    assert (len(heuristics) == len(values))

    position_score = sum(x * y for x, y in zip(heuristics, values)) #evaluation for current board state

    return position_score






def f_evaluate(board_old, shape_indexes, heuristics, counter, combo_timer):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)


    first_layer_scores_and_move = []
    first_layer_depth = 5

    scores_and_move = []

    for boards in firstlayer_boards:
        score = f_compute_score(heuristics, boards[0], boards[1], counter, combo_timer)
        first_layer_scores_and_move.append([score] + boards)
        # first_layer_scores_and_move
        # new_board = Logic.all_moves_first(boards[0], next_shape_index)

    first_layer_scores_and_move.sort()
    top_boards = first_layer_scores_and_move[-first_layer_depth:]
    # print(top_boards)

    for boards in top_boards:
        if h_death(boards[1]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
            continue
        new_board = Logic.all_moves_first(boards[1], next_shape_index)
        for new in new_board:
            # eval here
            # score = f_compute_score(heuristics, new[0], new[1] + boards[2], counter, combo_timer) # new[1] + boards[1] is line clear from old board + line clear from new board
            score = f_compute_score(heuristics, new[0], new[1], counter, combo_timer) # new[1] only

            scores_and_move.append([score, boards[2], boards[3], boards[4]])

    return scores_and_move




