from random import randrange as rand
import sys
import GUI
from random import choice
from random import randint

config = {
	'cell_size':	25,
	'cols':		10,
	'rows':		18,
	'delay':	750,
	'maxfps':	30
}

colors = [
(0,   0,   0  ),
(255, 0,   0  ),
(0,   150, 0  ),
(0,   0,   255),
(255, 120, 0  ),
(255, 255, 0  ),
(180, 0,   255),
(0,   220, 220)
]


tetris_shapes = [
	[[0, 1, 0],
	 [1, 1, 1]],
	
	[[0, 2, 2],
	 [2, 2, 0]],
	
	[[3, 3, 0],
	 [0, 3, 3]],
	
	[[4, 0, 0],
	 [4, 4, 4]],
	
	[[0, 0, 5],
	 [5, 5, 5]],
	
	[[6, 6, 6, 6]],
	
	[[7, 7],
	 [7, 7]]
]

piece_to_index = ['T',
                  'S',
                  'Z',
                  'J',
                  'L',
                  'I',
                  'O'
                  ]
tetris_states = [4,2,2,4,4,2,1]

def rotate_clockwise(shape):
	return [ [ shape[y][x]
			for y in range(len(shape)) ]
		for x in range(len(shape[0]) - 1, -1, -1) ]

def check_collision(board, shape, offset):
	off_x, off_y = offset
	for cy, row in enumerate(shape):
		for cx, cell in enumerate(row):
			try:
				if cell and board[ cy + off_y ][ cx + off_x ]:
					return True
			except IndexError:
				return True
	return False


def remove_row(board, row):
	del board[row]
	return [[0 for i in range(config['cols'])]] + board
	
def join_matrixes(mat1, mat2, mat2_off):
	off_x, off_y = mat2_off
	for cy, row in enumerate(mat2):
		for cx, val in enumerate(row):
			mat1[cy+off_y-1	][cx+off_x] += val
	return mat1

def new_board():
	board = [ [ 0 for x in range(config['cols']) ]
			for y in range(config['rows']) ]
	#board += [[ 1 for x in range(config['cols'])]]
	return board

def draw(board):
    canvas,master = GUI.initialise_grid()
    for r in range(config['rows']):
        for c in range(config['cols']):
            if board[r][c] != 0:
                GUI.colour_in(c,r,'red',canvas)
                
def print_matrix(board):
    
##    for r in range(3):
##        for c in board[r]:
##    print (board)
##    return 
    for r in board:
        for c in r:
            print("{:0>1d}".format(c),end=' ')
        print()

def all_moves_first(board_old, shape_index):
    # returns LIST of [board, line_clear, new_x, num_rotations]
    possible_boards = []
    stone_y = 0
    num_rotations = 0
    
    stone = tetris_shapes[shape_index]
    for j in range(tetris_states[shape_index]):
        new_x = 0
        while new_x <= config['cols'] - len(stone[0]):
            board = [row[:] for row in board_old]
            board, line_clear = drop_piece(board,stone,new_x,stone_y)
            possible_boards.append([board, line_clear, new_x, num_rotations])
            new_x += 1
        num_rotations += 1    
        stone = rotate_clockwise(stone)

    return possible_boards

def all_moves_second(board_old, shape_indexes):
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = all_moves_first(board_old, current_shape_index)
    secondlayer_boards = []
    # keep only first new_x and num_rotations 
    for boards in firstlayer_boards:
        
        new_board = all_moves_first(boards[0], next_shape_index)
        for temp in new_board:
            temp[1] += boards[1]
            temp[2] = boards[2]
            temp[3] = boards[3]

        secondlayer_boards += new_board
        # secondlayer_boards.append(new_board)

    return secondlayer_boards
        
        
def drop_piece(board,stone,stone_x,stone_y):

    line_clear = 0
    row_to_remove = []
    while not check_collision(board, stone,(stone_x, stone_y)):
        stone_y += 1
        
    board = join_matrixes(board,stone,(stone_x,stone_y))

    while True:
        for i, row in enumerate(board[:]):
            if 0 not in row:
##                row_to_remove.append(i)
                board = remove_row(board, i)
                line_clear += 1
                
        else:
            break

    return board, line_clear


def add_garbage(board):
    del board[0]
    random = rand(config['cols'])
    return board + [[0 if i==random else 8 for i in range(config['cols'])]]

def garbage_testboard():
    board = [[0 for x in range(config['cols'])]
             for y in range(config['rows'] - 18)]

    # Current piece is  1
    # Best move is  [-790.3497994495195, 0, 1, 0]

    board = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
             [7, 7, 4, 0, 2, 6, 0, 0, 0, 0],
             [7, 7, 1, 0, 2, 6, 0, 0, 0, 0],
             [7, 7, 1, 1, 2, 6, 0, 0, 0, 0],
             [7, 7, 1, 5, 4, 6, 6, 6, 0, 0],
             [1, 5, 5, 5, 4, 6, 6, 6, 0, 0],
             [1, 1, 2, 4, 4, 6, 6, 6, 0, 0],
             [1, 3, 2, 2, 5, 6, 6, 6, 0, 0],
             [3, 3, 3, 2, 5, 6, 6, 6, 0, 0],
             [3, 3, 3, 2, 5, 5, 6, 6, 0, 0],
             [1, 3, 2, 2, 2, 4, 6, 2, 0, 0],
             [1, 1, 2, 2, 2, 4, 6, 2, 0, 0],
             [1, 3, 3, 2, 4, 4, 7, 7, 0, 0],
             [7, 7, 3, 3, 7, 7, 7, 7, 0, 0],
             [7, 7, 4, 4, 7, 7, 6, 3, 0, 0],
             [8, 8, 8, 8, 8, 8, 8, 0, 8, 8],
             [8, 8, 8, 0, 8, 8, 8, 8, 8, 8],
             [8, 8, 8, 8, 8, 8, 0, 8, 8, 8]
             ]
    return board

def garbage_testboard2():
    board = [[0 for x in range(config['cols'])]
             for y in range(config['rows'] - 9)]

    board += [[ 8,8,8,8,8,8,8,8,8,0]]
    board += [[ 8,8,0,8,8,8,8,8,8,8]]
    board += [[ 8,8,8,8,0,8,8,8,8,8]]
    board += [[ 8,8,8,8,8,8,8,0,8,8]]
    board += [[ 8,8,0,8,8,8,8,8,8,8]]
    board += [[ 8,8,8,0,8,8,8,8,8,8]]
    board += [[ 8,8,8,8,8,0,8,8,8,8]]
    board += [[ 8,8,0,8,8,8,8,8,8,8]]
    board += [[ 8,8,8,8,8,8,0,8,8,8]]
    return board


def history_randomiser(history):
    piece_to_choose = [x for x in range(7) for y in range(3)]
    for piece in history:
        if piece in piece_to_choose:
            piece_to_choose.remove(piece)

    if len(history) > 14:
        history = history[1:]
        
    return history, choice(piece_to_choose)

def c2_sequence():
    with open('pieces.txt', 'r') as file:
        sequence = file.read().replace('\n', '')

    seed = randint(0, int(len(sequence) / 4))
    #seed = 0
    sequence = sequence[seed:]

    return sequence

def c2_randomizer(past_index, sequence):

    return past_index + 1, piece_to_index.index(sequence[past_index + 1])

def peek_next_piece(past_index, sequence):
    return piece_to_index.index(sequence[past_index + 1])

def loop_history():
    sequence = c2_sequence()

    past_index = 0
    for i in range(100):
        past_index, choice = c2_randomizer(past_index, sequence)
        print(choice, end = "")
        # history.append(choice)

# loop_history()

