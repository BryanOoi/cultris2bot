from AIDownstackCombo import *


def AICombo(heuristic):
    a = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [3, 3, 5, 5, 5, 7, 7, 0, 0, 0],
         [3, 3, 5, 2, 2, 7, 7, 0, 0, 0],
         [3, 3, 2, 2, 4, 4, 3, 6, 0, 0],
         [3, 3, 7, 7, 4, 3, 3, 6, 0, 0],
         [3, 3, 7, 7, 4, 3, 5, 6, 0, 0],
         [3, 4, 4, 4, 5, 5, 5, 6, 0, 0],
         [2, 5, 5, 4, 1, 1, 1, 2, 0, 0],
         [2, 2, 5, 7, 7, 1, 1, 2, 2, 0],
         [0, 2, 5, 7, 7, 1, 1, 1, 2, 0],
         [8, 8, 8, 8, 8, 8, 0, 8, 8, 8],
         [8, 8, 0, 8, 8, 8, 8, 8, 8, 8],
         [0, 8, 8, 8, 8, 8, 8, 8, 8, 8],
         [8, 8, 8, 8, 8, 8, 8, 8, 0, 8],
         [0, 8, 8, 8, 8, 8, 8, 8, 8, 8],
         [8, 8, 8, 8, 8, 8, 0, 8, 8, 8],
         [8, 8, 0, 8, 8, 8, 8, 8, 8, 8],
         [8, 8, 8, 8, 8, 8, 8, 0, 8, 8]]

    b = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [3, 3, 5, 5, 5, 7, 7, 0, 0, 0],
         [3, 3, 5, 2, 2, 7, 7, 0, 0, 0],
         [3, 3, 7, 7, 4, 3, 3, 6, 0, 0],
         [3, 3, 7, 7, 4, 3, 5, 6, 0, 0],
         [3, 4, 4, 4, 5, 5, 5, 6, 0, 0],
         [2, 5, 5, 4, 1, 1, 1, 2, 0, 0],
         [2, 2, 5, 7, 7, 1, 1, 2, 2, 0],
         [0, 2, 5, 7, 7, 1, 1, 1, 2, 0]]

    good_boards = [

        [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 2, 0, 0, 0, 0, 0, 0, 0, 0], [0, 2, 2, 0, 0, 1, 0, 0, 0, 0],
         [4, 4, 2, 0, 1, 1, 1, 0, 0, 0], [4, 3, 3, 5, 5, 7, 7, 0, 0, 0], [4, 1, 3, 3, 5, 7, 7, 0, 0, 0],
         [1, 1, 7, 7, 5, 3, 6, 0, 0, 0], [2, 1, 7, 7, 3, 3, 6, 0, 0, 0], [2, 2, 3, 3, 3, 6, 6, 0, 0, 0],
         [2, 2, 0, 3, 3, 6, 6, 0, 0, 0], [2, 2, 1, 1, 1, 6, 4, 0, 0, 0], [5, 2, 1, 1, 5, 6, 4, 6, 0, 0],
         [5, 1, 1, 1, 5, 4, 4, 6, 0, 0], [5, 5, 3, 3, 5, 5, 3, 6, 0, 0], [7, 7, 2, 3, 3, 3, 3, 6, 0, 0],
         [7, 7, 2, 2, 2, 3, 5, 2, 0, 0], [7, 7, 4, 2, 2, 2, 5, 2, 2, 0], [7, 7, 4, 4, 4, 2, 5, 5, 2, 0]],
        [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 0, 0, 0, 4, 0, 0, 0],
         [0, 0, 2, 2, 0, 0, 4, 0, 0, 0], [5, 5, 5, 2, 0, 4, 4, 0, 0, 0], [5, 1, 3, 3, 0, 5, 2, 0, 0, 0],
         [1, 1, 1, 3, 3, 5, 2, 2, 0, 0], [1, 1, 1, 3, 3, 5, 5, 2, 0, 0], [1, 1, 7, 7, 3, 3, 3, 6, 0, 0],
         [1, 1, 7, 7, 6, 3, 3, 6, 0, 0], [1, 1, 7, 7, 6, 3, 3, 6, 0, 0], [1, 1, 7, 7, 6, 3, 3, 6, 0, 0],
         [2, 1, 4, 4, 6, 3, 1, 6, 0, 0], [2, 2, 4, 4, 4, 1, 1, 6, 0, 0], [4, 2, 4, 4, 4, 1, 1, 6, 0, 0],
         [4, 4, 4, 4, 4, 1, 1, 6, 1, 0], [7, 7, 4, 4, 4, 1, 1, 1, 1, 0], [7, 7, 4, 4, 4, 1, 1, 1, 1, 0]],
        a,
        b
    ]

    this_pieces = 15
    stone_y = 0 # dont change thi
    # piece_chance = 5
    dropped_pieces = 0
    current_pieces = 0
    highest_counter = 0
    total_combo = 0
    total_score = 0
    all_sequences = ["IZOTJISLSITOTSOLJOZTZJZOZJZLOTILTLSITILITISL",
                    "IOZSLOJOLITLJISLTISTJLOZSOJTSILLOSLOJTJZOSJO",
                    "ILOTTSTZILLTISTZTLZTZOTSOLLZOILSLZOJIZITOLOS",
                    "IILJTSZOSTTSJZOJIJLOZIJLZJITTSIZJTLJOTTZTOJI",
                    "IJLSJZLITOIJTTIOZOJSOLIOSOJOZITJIJZTZSZISSZO",
                    "IZJOZSITOJSLZLISTZLJZJTLZIZOJZSJZOIOZSILOZJI",
                    "IOTLZSZIJLZIZTIOJTLOLJOLJILZLJOZSLSTLJSOILOZ",
                    "ISJLSZJIZIJITOSTLIZOLSITJZSTIJLTOTOIZLOSJSTZ",
                    "IITSZLIZOSIJZLTSZILJTSOZILSLSJILSLTJLIJITSZT",
                    "IILSZLOZISOIZTLZLTJISLITJOLZILTIZOIZLTJZZLJS",
                    "ILOJLLTJSIZOISTOITOTLZOSJJOZJOJTZIZLZLZJTJIO",
                    "ILOSSZLSTSZIOSTLOSLTLSLOZZLJZOZSTITZJLJITSLL",
                    "IJZOZISJIJILZILZOIJOZSTOZJSLZTOJZOTZLTSSJOJZ",
                    "ISOZISOZOJLSLSLTSLZSOJLOSLJSZLZOLJOLSJTSLOSZ",
                    "IZJTOIZIJLZIJTJLZTSLTIOLZJTJJSIITZTIOSOLIJLT",
                    "ISZLLIOSOZLSJOISZIJZLLSOTZJIZOOIZTOZJLSTJILZ",
                    "IOZOJLSTSLILSOTLTIOZSOTJSZLOSILJOSTSTISTLSLJ",
                    "IJISJZOZOITZLIZLSILJOZLTZJIJIIOZLTSOTIZOTLSZ",
                    "ISLILSTOZSTOSLZLZLOLSITLOZLIZJTTZLSOLOITOJTO",
                    "IOLOSTZJIZJSIZJLSTIZOTSOLZJOISIJZLOLIJISZOZI",
                    "IIISOTTLTJSLZOSOZZOTLOSTLIZTSIOJSOJTISTOZJIT",
                    "IZSTLJZZSJLJLIJOZJZTZLSJZILIOZJZOTOSTZSTSIOL",
                    "IOLTJTJSOISOZOTJTOIZSLOOZSTIJOSTLZOIJTJIOTIT",
                    "ISTJIOZTSOZTOJSTOZOILJTLZSSZJOLSJSTITJZTILST",
                    "ISLIJITZTLZTJZTTSSOIZTSZSJLZOITZOLJLLSTJLOIO",
                    "IOSTOJLSISOJZILJZOZSLOSIOSJLTOILOZSTLIJSLOTJ",
                    "IJOISLTZJSTOZZTZLZOTZTZLSZZSIZLITSTSTSJZSTTO",
                    "IOSOTZLZTILZSZOJSLITZTLIOTIJJZTSOZJSJZZOLJLI",
                    "IZTJIOZJSILZOJTLJZSIOLOTJSTISTJZIZLJTOTJOTSL",
                    "ISTOJLZJTZOTTZJIZTLIOSTJZSJIJIOTTJOITZLIOZJZ",
                    "ITJIZSISTZOTZZLOSLJOSZZISZLTOSZZOSTLSOJILTZL",
                    "IZLJIZTSLIOJZTISZLZIJTLOTZOTLSTSJILTZSZTJITL",
                    "IZLSIZSTZIZOILZJLIZJLOSOZSOSTSOSJLOIZTLJOZTL"]
    games = len(good_boards)

    for i in range(games):
        # sequence = Logic.c2_sequence()
        sequence = all_sequences[i]
        past_index = 0

        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        # past_index, stone_index = Logic.history_randomiser(history)
        # history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0
        # board = Logic.garbage_testboard()
        board = good_boards[i]
        # dropped_pieces = 0


        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)
            # history.append(stone2_index)
            combo_timer = 1 if time_left > 0 else 0

            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter, combo_timer)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            # Logic.print_matrix(board)

            if h_death(board) != 0:
                break

            columns = getAllColumns(board)
            stack_holes = stack_hole_number(columns,board)

            current_score = combo_score(line_clear, combo_timer, stack_holes)
            # print(line_clear, combo_timer, current_score)
            total_score += current_score
            dropped_pieces += 1

            # if rand(piece_chance) == 1: board = Logic.add_garbage(board)
            # if dropped_pieces % piece_chance == 0:  board = Logic.add_garbage(board)# just add garbage randomly



            score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            if counter > highest_counter:
                highest_counter = counter
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]
        # print(dropped_pieces - current_pieces)


        # print(highest_counter, total_combo)

    return float((total_score) / games)

# print(AICombo([-5.496923755990245, -0.6713294512087589, -1.7342472010365668, -7.276207903391384, -5.266904928831249, -13.527758071757077, -2.5430216675011863, 0, -2.473876991451801, -0.24169818965840806, -0.8384364379495515, -0.6682138454965387, 1.0]))
