#from AIComboWell import *
from AIComboWell3 import *
import pygame, sys

config = {
    'cell_size': 25,
    'cols': 10,
    'rows': 18,
    'delay': 0,
    'maxfps': 3
}

colors = [
    (0, 0, 0),
    (245, 54, 249),
    (70, 249, 54),
    (249, 54, 54),
    (54, 99, 249),
    (249, 165, 54),
    (56, 231, 255),
    (249, 239, 42),
    (135, 130, 129)
]

tetris_shapes = [
    [[0, 1, 0],
     [1, 1, 1]],

    [[0, 2, 2],
     [2, 2, 0]],

    [[3, 3, 0],
     [0, 3, 3]],

    [[4, 0, 0],
     [4, 4, 4]],

    [[0, 0, 5],
     [5, 5, 5]],

    [[6, 6, 6, 6]],

    [[7, 7],
     [7, 7]]
]


##canvas,master = GUI.initialise_grid()
##score = 0
##time_left = 0
##counter = 0
##board = Logic.new_board()
##stone_y = 1
##
##
##stone_index = rand(len(Logic.tetris_shapes))
##stone = Logic.tetris_shapes[stone_index]
##
##for i in range(5):
##    stone2_index = rand(len(Logic.tetris_shapes))
##    ##stone = tetris_shapes[rand(len(tetris_shapes))]
##
##
##    all_moves = f_evaluate(board,(stone_index,stone2_index),test_heuristic)
##    best_move = max(all_moves)
##
##    for i in range(best_move[3]):
##        stone = Logic.rotate_clockwise(stone)
##    board,line_clear = Logic.drop_piece(board,stone,best_move[2],stone_y)
##    print(f_compute_score(test_heuristic, board, line_clear,'yes'))
##
##    #Logic.print_matrix(board)
##    draw(board,canvas)
##    score, time_left, counter = f_score(line_clear, (score, time_left, counter))
##    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
##    stone_index = stone2_index
##    stone = Logic.tetris_shapes[stone_index]
##

class TetrisApp(object):
    def __init__(self):
        pygame.init()
        pygame.key.set_repeat(250, 25)
        self.width = config['cell_size'] * config['cols']
        self.height = config['cell_size'] * config['rows']

        self.screen = pygame.display.set_mode((self.width + 150, self.height + 100))
        pygame.event.set_blocked(pygame.MOUSEMOTION)  # We do not need
        # mouse movement
        # events, so we
        # block them.
        self.low_game_count = 0
        self.mid_game_count = 0
        self.high_game_count = 0

        self.all_sequences = ["ILOTIOTZISOZJOSJLITISTLOOIOLIZSTIOILILZLOZOT",
         "ITLJTLSIZOTJOLOJSOILSOISTZOZLTOSZTTJTLTIOJLI",
         "ILZTILZSOSTIIJJSLOZJIOTIJTZSZZJOLTOJLISOIZJI",
         "ILISLTOJZIOZJTZSJOZTIZTZITOSLZJTJOIIZOZTILSL",
         "IZSLZLSJTZIOSJZOTIJSOZJZJSOTTSZLSZJZSJLIOLSO",
         "ITIOSJZLSSTJSOIZSTLIZLOISTJIOITJTSTLSJSISZJT",
         "ISTJOISIOSLZLSZIJOIITIZTSZTJSZJLOSZJLTZOIOTS",
         "IIJOILSJTZIJSJITJSJISOTLIJLOJIJSZOOIJITTSZOL",
         "IOITTLOLJILTSOZJIJLISTSJLOJILJZLIZSLSZOJSOTZ",
         "IITLZSILTJIOILOJTOSZSLJLJZIZSLSJTLOIZSLJOTIS",
         "IJSLISZOZJOSLZOJITLJTLOJIZITJOILOJOISTJOSZTZ",
         "ILTTJJLTSLZTTLJISTJIOZTOLSTLOSITZITSOTJTZIJS",
         "ITIOJSOTZOZLSOIJTLSTOZJSLZLTLJSZOTZSLJZSIJIO",
         "IZTSOZOLLILJZIOJSZTLZSOJLISJOJLILJOTOLTSZLOT",
         "ITZSOIOOJZTZITJTSTSJSIZZJSLTJLTITOJISTOIJLTO",
         "ITOILILZTOTLIJSSJZSOTJSIJSSLIZZLJSZTISLSLITS",
         "IOIJOSZOLJOLZJZJLOZSJSLJITJLSLJIZSTZIJZJSLIJ",
         "IZLTILSJLJSOLSOZSJOSSZOITZOILSZILOIIJOJSLZLO",
         "IZJITZTZOZLJIJOSTJZSTZOZJJZSILSOZLJISTSJSIZL",
         "ITSZTOIOLTLSISOISTTSLSOTZTLIZLTSLTZLOZITOSLZ",
         "IZTSITSZIOSZSJISITJLJTJZLOLTOISTIOTOTJSJLSIO",
         "IOZITZLSJZLJLILSTJZSZIZSOLJZJLSZTZLTJOTZSTOT",
         "IZOSZITZSOITSSTZTOIJILJTOSIZOTIOJZSZSTLISSIO",
         "IZJLZLSITOSLZITLSOJLTZTLJOJILTZIOJLTJLZSLIOT",
         "IZOSTZIZJSOZOJSOTLSTSZJLTZTZOLZLOJLSOJSOSILO",
         "ISOIZJTLOLSLSZOSZLSJILOLTJOSSOLOTZOTSLSJLOLZ",
         "IZTLSZOLZSZTJIZLTOJLSJOSOILISOOITJOILZJTLOTL",
         "IJTTIZIJIOTILISJOJLJZTOOSLTOJTJLISTSIJZTZITS",
         "ILITLTSIIJSZOJOISLOJJOIOSOISSOZSZSOOJLOZISJZ",
         "IZTIZSOSJZSTZTOISJLILSZOILSZOJIOIZOSOLJLZLZS",
         "ILTZTOTZSITSOZOJOSLTZOISOZZJZIZJTZLSJLITOSIL",
         "IJILTZOJIOZOSJOJOZJZSLJSOOLSOLTLITSLTOJLLSOZ",
         "IOJLISLSZOIZZOZJLOZTOTILZJLZSILJTZZLOITJSIOJ"]
        self.total_games = 0
        self.init_game()



    def new_stone(self):
        self.stone_y = 0
        self.past_index, self.stone_index = Logic.c2_randomizer(self.past_index, self.sequence)
        # self.history.append(self.stone_index)
        self.stone = Logic.tetris_shapes[self.stone_index]


        ##        self.stone2_index = Logic.history_randomiser[self.history]
        ##        self.history.append(stone2_index)
        ##      self.stone = tetris_shapes[rand(len(tetris_shapes))]
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0]) / 2)


        if Logic.check_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
            self.gameover = True

    def init_game(self):
        self.max_comboable_lines = 0
        self.current_comboable_lines = 0
        self.board = Logic.new_board()
        self.sequence = Logic.c2_sequence()
        # self.sequence = self.all_sequences[self.total_games]
        # self.sequence = "ILIZZSOSLSOTILSOLSLTSZSLIOZJOLJLOTLZTJOJJSTL"
        # self.board = Logic.garbage_testboard()
        self.past_index = 0
        self.combo_threshold = 10
        self.new_stone()
        self.chance = 1000000
        self.max_pieces_allowed = 30




        # self.test_heuristic = [-1.8108083582364949, 0.06718761640149484, -1.6848279668694497, 5.238292340368663, -4.879422280310628, -2.9736692788019305, -6.843726017391311, 1.0]#3well best so far
        # self.test_heuristic = [-0.5823071034456513, -1.2042392807917868, -0.8348948057763271, 3.430287693369177, -7.456876651139379, -1.7396494987406825, -1.7172809309539354, -7.230802594630394, 1.0]#3well best so far
        # self.test_heuristic = [3.8669696040759227, 0.9527322363721766, -2.786120192696747, 7.37535933809343, -8.927608033690424, -0.4604447698505538, -8.957589184398458, -7.802382454502866, 1, 1, 1, 1.0]#2well best so far
        # self.test_heuristic = [4.115293486583469, -0.34633896870302316, -1.099069620422449, 9.995468768902583, -7.637545449372233, -3.0394993047812653, -3.839333734812174, -7.5916858709344, 0, 0, 0, 2, 1, 1, 1]#2well best so far
        # self.test_heuristic = [2.724592567052916, -0.3234327758896809, -1.0305346552139294, 11.434391616988897, -10.677127628806883, -3.4973314804077145, -3.156447818553091, -12.374627578557606, 0.4023201806500176, -9.791468901529132, -3.2661075195402995, 1.8492265146159963, 0.9060199214720575, 1.9504787599239632, 0.29742946909910906]#2well best so far
        # self.test_heuristic = [5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413]#2well best so far
        # self.test_heuristic = [-5.688814879757615, -0.5405970739097725, -0.357891059651708, 14.573525500449534, 3.1086490594738114, -2.131618732652844, 3.3311405068196738, 10.245494012332395, -3.8438341212545537, -5.667564978970729, -1.9725906355840621, -5.152974958697402, -5.636801085497634, 1.0]
        # self.test_heuristic = [-18.39662050666626, 0.45368682259806814, -1.5227408175492023, 6.3591642531361865, -7.749564617440845, -1.7310223837225212, -1.3092066279962826, -4.971970654050358, -15.688058982582925, -6.421841364822997, 2.722887714487167, -8.22808722559403, -4.321262881128452, 1.0]
        # self.test_heuristic = [-3.8901826140196825, -0.8100972961872714, -0.2566802231165499, 15.003049394775257, 5.0077358260345575, -3.5125137652767933, 5.688883164422631, 10.571564979392207, -6.298614072954353, -5.329989628686396, -4.239130790096382, -4.630033743966033, -5.09161270393321, 1.0]
        # self.test_heuristic = [-3.3097613234732095, -0.44550117806097056, -0.6262938878912774, 14.175852503529713, 2.831215949886526, -1.2643179570456606, 6.157171756837603, 10.40051182431171, -4.529122974478024, -5.4713151161989035, -4.060992408402506, -4.709780649791912, -6.007332623466413, 1.0]
        self.test_heuristic = [-5.407781696573501, -0.5069668929330104, -3.5293868591324737, 13.166399359741805, 2.4340850647407297, -3.7300429666208297, 4.202315965499364, 8.848798425255318, -6.787816291262772, -2.5427781520957264, -4.188100227996165, -2.576878630065499, -8.797005722646954, 1.0]
        self.dropped_pieces = 0
        self.score = 0
        self.time_left = 0
        self.counter = 0
        # self.mode = f_compute_score # different AI behaviours based on heuristic
        # self.mode_evaluate = f_evaluate # different AI behaviours based on heuristic
        self.mode = f_compute_score_combowell
        self.mode_evaluate = f_evaluate_combowell


        self.printed = False
        self.past_neg = False

    def center_msg(self, msg):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height // 2 - msgim_center_y + i * 22))

    def below_msg(self, msg, offset_below):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height + offset_below - msgim_center_y + i * 22))

    def draw_matrix(self, matrix, offset):
        off_x, off_y = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                try:
                    colors[val]
                except:
                    val = 0

                if val:
                    pygame.draw.rect(
                        self.screen,
                        colors[val],
                        pygame.Rect(
                            (off_x + x) *
                            config['cell_size'],
                            (off_y + y) *
                            config['cell_size'],
                            config['cell_size'],
                            config['cell_size']), 0)

    def quit(self):
        self.center_msg("Exiting...")
        pygame.display.update()
        pygame.quit()
        sys.exit()

    def output_matrix(self):
        print(self.board)

    def toggle_pause(self):
        self.paused = not self.paused

    def start_game(self):
        if self.gameover:
            self.init_game()
            self.gameover = False

    def run(self):
        key_actions = {
            'ESCAPE': self.quit,
            'p': self.toggle_pause,
            'SPACE': self.start_game,
            'm' : self.output_matrix
        }

        self.gameover = False
        self.paused = False

        pygame.time.set_timer(pygame.USEREVENT + 1, config['delay'])
        dont_burn_my_cpu = pygame.time.Clock()
        while 1:
            if self.gameover != True:
                self.past_index, self.stone2_index = Logic.c2_randomizer(self.past_index, self.sequence)

                # self.history, self.stone2_index = Logic.history_randomiser(self.history)
                # self.history.append(self.stone2_index)

                # candidate_moves = Logic.all_moves_second(self.board, (self.stone_index, self.stone2_index))
                # for i in range(10):
                #     print(candidate_moves[i])

                # all_moves = f_evaluate(candidate_moves, self.test_heuristic, self.counter)

                all_moves = self.mode_evaluate(self.board, (self.stone_index, self.stone2_index), self.test_heuristic, self.counter)


                best_move = max(all_moves)


                #print out bad moves
                # if best_move[0] < -500 and best_move[0] > -900 and self.printed == False:
                # if best_move[0] > 300 and self.printed == False:
                #         print('Current board is ', self.board)
                #         print('Current piece is ' , self.stone_index)
                #         print('Next piece is ' , self.stone2_index)
                #         print('Best move is ' , best_move)
                #         self.printed = True

                # print(self.sequence[self.past_index], end="")

                for i in range(best_move[3]):
                    self.stone = Logic.rotate_clockwise(self.stone)
                self.board, line_clear = Logic.drop_piece(self.board, self.stone, best_move[2], self.stone_y)
                self.dropped_pieces += 1


                # if rand(self.chance) == 1: # just add garbage randomly
                #     self.board = Logic.add_garbage(self.board)

                self.score, self.time_left, self.counter = f_score(line_clear,
                                                                   (self.score, self.time_left, self.counter))

                self.stone_index = self.stone2_index
                self.stone = Logic.tetris_shapes[self.stone_index]



                columns = getAllColumns(self.board)
                self.current_comboable_lines = bot_switch_heuristics(columns, self.board)[1]

                if self.current_comboable_lines > self.max_comboable_lines:
                    self.max_comboable_lines = self.current_comboable_lines



            self.screen.fill((0, 0, 0))

            if h_death(self.board) != 0 or self.dropped_pieces == self.max_pieces_allowed:
                # self.gameover = True
                # columns = getAllColumns(self.board)
                # print(h_eval(columns, self.board))
                # print('Max comboable lines is: ', self.max_comboable_lines, self.combo_threshold)
                # print('Sequence is : I', self.sequence[1:self.max_pieces_allowed + 4])

                # self.low_game_count = 0
                # self.mid_game_count = 0
                # self.high_game_count = 0

                # print()
                # print(self.high_game_count, self.mid_game_count, self.low_game_count)
                # print()

                if self.max_comboable_lines >= self.combo_threshold and self.high_game_count <= -1:
                    self.high_game_count += 1
                    print('Max comboable lines is: ' , self.max_comboable_lines, self.combo_threshold)
                    print('"I' + self.sequence[1:self.max_pieces_allowed + 4] + '",')
                elif self.max_comboable_lines < self.combo_threshold - 1 and self.max_comboable_lines >= 6 and self.mid_game_count <= -1:
                    self.mid_game_count += 1
                    print('Max comboable lines is: ' , self.max_comboable_lines, self.combo_threshold)
                    print('"I' + self.sequence[1:self.max_pieces_allowed + 4] + '",')
                elif self.max_comboable_lines < 5 and self.low_game_count <= 20:
                    self.low_game_count += 1
                    print('Max comboable lines is: ' , self.max_comboable_lines, self.combo_threshold)
                    print('"I' + self.sequence[1:self.max_pieces_allowed + 4] + '",')

                if self.high_game_count > 10 and self.mid_game_count > 10 and self.low_game_count > 10:
                # if self.high_game_count > 1:
                    quit()
                self.total_games += 1
                self.init_game()
                self.printed = False


            if self.gameover:
                self.center_msg("""Game Over!
Press space to continue""")
                self.below_msg(
                    'Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(self.counter),
                    20)
                self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)
            else:
                if self.paused:
                    self.center_msg("Paused")
                else:
                    self.draw_matrix(self.board, (0, 0))
                    self.draw_matrix(self.stone,
                                     (self.stone_x,
                                      self.stone_y))
                    pygame.draw.line(self.screen, (255, 255, 255), (self.width, 0), (self.width, self.height), 5)
                    next_index = Logic.peek_next_piece(self.past_index, self.sequence)
                    self.draw_matrix(Logic.tetris_shapes[next_index],
                                     (self.stone_x + 8,
                                      self.stone_y + 1))
                    self.below_msg('Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(
                        self.counter), 20)
                    self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)

                    self.below_msg(
                        'Comboable lines: ' + str(self.current_comboable_lines),
                        60)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                elif event.type == pygame.KEYDOWN:
                    for key in key_actions:
                        if event.key == eval("pygame.K_"
                                             + key):
                            key_actions[key]()

            dont_burn_my_cpu.tick(config['maxfps'])


if __name__ == '__main__':
    App = TetrisApp()
    App.run()
