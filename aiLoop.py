import aiComboWellLoop
import timeit



population = []
total_population = 100
generations = 70
pieces = 100

def random_heuristic():
    ls = [float(rand(-10000000,10000001)/10000000) for i in range(len(evaluation_function))]
    return np.interp(ls, (min(ls), max(ls)), (-1, +1)).tolist()
        
def weighted_average(arg1, arg2):
    score1,heu1 = arg1
    score2,heu2 = arg2
    addscore = score1+score2
    if addscore == 0:
        return random_heuristic()
    t1 = [float(score1/addscore) * x for x in heu1]
    t2 = [float(score2/addscore) * x for x in heu2]
    
    new_average = [round(x + y,7) for x, y in zip(t1, t2)]
    return new_average
##    return np.interp(new_average, (min(new_average), max(new_average)), (-1, +1)).tolist()

def mutation(new_gen):
    if rand(0,100) < 5:
        new_gen[rand(0,len(new_gen))] += float(rand(-200,200)/1000)
    return new_gen
##    return np.interp(new_gen, (min(new_gen), max(new_gen)), (-1, +1)).tolist()
    
def AILoop(heuristic):
    history = []
    total_score = 0
    time_left = 0 
    counter = 0
    
    stone_y = 1

    board = Logic.new_board()
    history, stone_index = Logic.history_randomiser(history)
    history.append(stone_index)
    stone = Logic.tetris_shapes[stone_index]
    score = 0
    for i in range(pieces):
        history, stone2_index = Logic.history_randomiser(history)
        history.append(stone2_index)

        all_moves = f_evaluate(board,(stone_index,stone2_index),heuristic,counter) #returns score for all moves
        best_move = max(all_moves)
        
        for i in range(best_move[3]):
            stone = Logic.rotate_clockwise(stone)
        board,line_clear = Logic.drop_piece(board,stone,best_move[2],stone_y)

        score, time_left, counter = f_score(line_clear, (score, time_left, counter))
##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))    
        stone_index = stone2_index
        stone = Logic.tetris_shapes[stone_index]
    return score


def AILoopComboWell(heuristic):

    this_pieces = 40 # enough pieces not to top out
    stone_y = 1 # dont change this
    games = 5
    total_score = 0



    for i in range(games):
        history = []

        time_left = 0
        counter = 0
        max_comboable_lines = 0

        board = Logic.new_board()
        history, stone_index = Logic.history_randomiser(history)
        history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0


        # if_temp_punish = 5 # Change this later haha, just to test out punish
        for i in range(this_pieces):
            history, stone2_index = Logic.history_randomiser(history)
            history.append(stone2_index)


            all_moves = f_evaluate_combowell(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for i in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            well_prop = h_calc_combowell(board)
            if well_prop[0] > max_comboable_lines:
                max_comboable_lines = well_prop[0]



            #score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]



        total_score += max_comboable_lines


    return total_score
    # return score


def AILoopComboScore(heuristic):

    this_pieces = 40
    stone_y = 1 # dont change this
    total_score = 0


    history = []

    time_left = 0
    counter = 0

    max_comboable_lines = 0
    board = Logic.new_board()
    history, stone_index = Logic.history_randomiser(history)
    history.append(stone_index)
    stone = Logic.tetris_shapes[stone_index]
    score = 0

    # if_temp_punish = 5 # Change this later haha, just to test out punish
    for i in range(this_pieces):
        history, stone2_index = Logic.history_randomiser(history)
        history.append(stone2_index)

        all_moves = f_evaluate_combowell(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
        best_move = max(all_moves)

        for i in range(best_move[3]):
            stone = Logic.rotate_clockwise(stone)
        board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

        well_prop = h_calc_combowell(board)
        if well_prop[0] > max_comboable_lines:
            max_comboable_lines = well_prop[0]


        score, time_left, counter = f_score(line_clear, (score, time_left, counter))
        ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
        stone_index = stone2_index
        stone = Logic.tetris_shapes[stone_index]

    well_prop = h_calc_combowell(board)
    total_score += well_prop[0]

    # print(max_comboable_lines)
    return max_comboable_lines, total_score


# 2 seconds to finish this

# start = timeit.default_timer()
# #
# # #Your statements here
# a = AILoopComboScore([0.7470785983491033, 0.67636622498353, 0.10636696105371568, -0.5196528324002738, -0.3042285749146507, -0.8852919029479736, 0.9308415692534793, -0.827640697209958/
# 4, 0.7033194573664425, -0.7113840423885942, -0.050809357242120434, -0.7119959597579986, -0.33110196059898866, 0.3243994556562002, 0.08473745159936352, -0.23975001359792913, -0.44396075479/
# 290364, 0.7425387311984151, -0.008305588524983643, 0.8729836430992695, 0.42632992805797665, -0.24695515726533346, 1])
# print(a)
# #
# stop = timeit.default_timer()
# #
# print('Time: ', stop - start)
def how_many_well_fails(test_heu):
    threshold = 9.9
    # threshold = 0.9
    good_scores = 0
    games = 1000
    # games = 1
    for i in range(games):
        score = aiComboWellLoop.AILoopComboWellTest(test_heu)
        print(score)
        if score >= threshold:
            good_scores += 1
    print('Total percentage good scores is' ,good_scores/games * 100)
    return good_scores


# how_many_well_fails([4.115293486583469, -0.34633896870302316, -1.099069620422449, 9.995468768902583, -7.637545449372233, -3.0394993047812653, -3.839333734812174, -7.5916858709344, 2, 1, 1, 1, 1.0]) 85%>
# how_many_well_fails([2.4068978733344077, -1.203238895677271, -3.623050217693911, 11.7639255235033, -7.8606075566205655, -0.7289431202201868, -5.543366859855454, -6.200139630682983, 3, 2.575288165163772, 3.2282155643071153, 2.111335401183984]) 81%
# how_many_well_fails([2.4474823140225928, -1.1519013531771152, -1.1870657541579508, 10.583786214306915, -11.016567499630202, -3.034976788252345, -2.549491805436854, -12.397975271312262, -0.9164696166142505, -10.005352473576146, -3.447299818346771, 1.924435876259496, 0.2529600990039702, 4.381589326833766, 0.7261299640290197]) 87.4
# how_many_well_fails([2.724592567052916, -0.3234327758896809, -1.0305346552139294, 11.434391616988897, -10.677127628806883, -3.4973314804077145, -3.156447818553091, -12.374627578557606, 0.4023201806500176, -9.791468901529132, -3.2661075195402995, 1.8492265146159963, 0.9060199214720575, 1.9504787599239632, 0.29742946909910906])  #89.4
# how_many_well_fails([5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413])  #86.3
how_many_well_fails([5.338045186462913, -0.9163466930302153, -1.0582243682397956, 10.197972241546662, -10.61000635124749, -1.40959477626774, -4.1875651273986145, -13.970666785342054, -1.3100124836782427, -0.2893401440824729, -10.406022316049683, -6.082756245852789, 2.5691627576797913, 0.9305127735978753, 1.739790376902608, 0.5643664531999042])  #86.3

def tries(test_heu):
    temp = []
    for i in range(10):
        temp.append(how_many_well_fails(test_heu))

    print('Average over 100 games ',sum(temp)/10)

def main_loop():
    population = []
    for q in range(total_population):
        new_heuristic = random_heuristic()
        population.append([-99999,new_heuristic])


    for i in range(generations):
        for popul in population:
            popul[0] = AILoop(popul[1])


        #print('Population; ',population)
        new_gen_pool = []
        while len(new_gen_pool) < int(0.3 * len(population)):
            subpool = sample(population, int(len(population)/10))
            subpool.sort(key=takeFirst)
            new_gen = weighted_average(subpool[len(subpool)-2],subpool[len(subpool)-1])
            new_gen = mutation(new_gen)
            new_gen_pool.append(new_gen)

        population.sort(key=takeFirst)

        #new population
        population = [[-99999,x] for x in new_gen_pool] + population[int(0.3 * len(population)):len(population)]

        for p in range(3):
            print('3 Best Solution; ',population[len(population)-(p + 1)])
        print("Generation :" , i)



    f= open("allData.txt","w+")
    f.write(population)
    f.close()


