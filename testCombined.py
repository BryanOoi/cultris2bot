#from AIComboWell import *
import AIComboWell2
import AIDownstack2
import Logic
import pygame, sys

config = {
    'cell_size': 25,
    'cols': 10,
    'rows': 18,
    'delay': 0,
    'maxfps': 2
}

colors = [
    (0, 0, 0),
    (245, 54, 249),
    (70, 249, 54),
    (249, 54, 54),
    (54, 99, 249),
    (249, 165, 54),
    (56, 231, 255),
    (249, 239, 42),
    (135, 130, 129)
]

tetris_shapes = [
    [[0, 1, 0],
     [1, 1, 1]],

    [[0, 2, 2],
     [2, 2, 0]],

    [[3, 3, 0],
     [0, 3, 3]],

    [[4, 0, 0],
     [4, 4, 4]],

    [[0, 0, 5],
     [5, 5, 5]],

    [[6, 6, 6, 6]],

    [[7, 7],
     [7, 7]]
]


##canvas,master = GUI.initialise_grid()
##score = 0
##time_left = 0
##counter = 0
##board = Logic.new_board()
##stone_y = 1
##
##
##stone_index = rand(len(Logic.tetris_shapes))
##stone = Logic.tetris_shapes[stone_index]
##
##for i in range(5):
##    stone2_index = rand(len(Logic.tetris_shapes))
##    ##stone = tetris_shapes[rand(len(tetris_shapes))]
##
##
##    all_moves = f_evaluate(board,(stone_index,stone2_index),test_heuristic)
##    best_move = max(all_moves)
##
##    for i in range(best_move[3]):
##        stone = Logic.rotate_clockwise(stone)
##    board,line_clear = Logic.drop_piece(board,stone,best_move[2],stone_y)
##    print(f_compute_score(test_heuristic, board, line_clear,'yes'))
##
##    #Logic.print_matrix(board)
##    draw(board,canvas)
##    score, time_left, counter = f_score(line_clear, (score, time_left, counter))
##    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
##    stone_index = stone2_index
##    stone = Logic.tetris_shapes[stone_index]
##

class TetrisApp(object):
    def __init__(self):
        pygame.init()
        pygame.key.set_repeat(250, 25)
        self.width = config['cell_size'] * config['cols']
        self.height = config['cell_size'] * config['rows']
        self.total_games = 3

        self.screen = pygame.display.set_mode((self.width + 150, self.height + 100))
        pygame.event.set_blocked(pygame.MOUSEMOTION)  # We do not need
        # mouse movement
        # events, so we
        # block them.
        self.combo_garbage_threshold = 3
        self.combo_start_threshold = 10
        self.bot_mode = 'combo'
        self.piece_height_threshold = 15
        self.start_combo_height = 0
        self.started_combo = False
        self.init_game()

    def new_stone(self):
        self.stone_y = 0
        self.past_index, self.stone_index = Logic.c2_randomizer(self.past_index, self.sequence)
        # self.history.append(self.stone_index)
        self.stone = Logic.tetris_shapes[self.stone_index]


        ##        self.stone2_index = Logic.history_randomiser[self.history]
        ##        self.history.append(stone2_index)
        ##      self.stone = tetris_shapes[rand(len(tetris_shapes))]
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0]) / 2)

        if Logic.check_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
            self.gameover = True

    def init_game(self):


        self.total_evaluation_function = ['bumpiness', 'total_comboable_lines', 'max(heights_list)', 'average_height',
                                     'column_hole', 'well_holes', 'comparitive_weight',
                                     'bumpiness2', 'total_comboable_lines2', 'max(heights_list)2', 'average_height2',
                                     'column_hole2', 'well_holes2', 'comparitive_weight2']
        self.bot_switching_evaluation_function = [ 'bumpiness', 'total_comboable_lines', 'max(heights_list)' , 'average_height', 'column_hole', 'well_holes' ]




        self.test_heuristic = [-1.4136664680900068, 0.14307546996002435, -0.8967316343501648, 5.132924860202508, -5.208389760960912, -2.9869438573863993, -6.809137550258085, -15.497312405246426, -0.9937549258794329, -0.7655034395024503, -10.211289225570285, -2.212122641315562, -3.077517430713458, -5.7226027565074, -0.962507181755409, -0.27213381090837974, -1.6899525781886018, 6.402089026347131, -8.154895184670231, 2.74385404153574, 3.1415239464440896]        #
        #
        upstack_heuristic_length = len(AIComboWell2.evaluation_function)
        downstack_heuristic_length = len(AIDownstack2.evaluation_function)
        bot_switching_heuristic_length = len(self.bot_switching_evaluation_function)

        # self.upstack_heuristic = self.test_heuristic[:upstack_heuristic_length]
        # self.downstack_heuristic = self.test_heuristic[upstack_heuristic_length: upstack_heuristic_length + downstack_heuristic_length]
        # self.bot_switching_heuristic = self.test_heuristic[-bot_switching_heuristic_length:]

        # self.gooddownstack_heuristic = [-18.416011191928167, -2.478022465684638, -2.093717512880721, -11.165687411613462, -1.5602406271093106, -7.043252563877942, -2.8947492288645438, -2.410985717667043, -2.4667689688972185, -0.9649826164595063, 3.3339666081663086, 0]
        self.gooddownstack_heuristic = [-18.416011191928167, -2.478022465684638, -2.093717512880721, -11.165687411613462, -1.5602406271093106, -7.043252563877942, -2.8947492288645438, -2.410985717667043, -2.4667689688972185, -0.9649826164595063, 0, 0]

        self.downstack_heuristic = [-0.7021521198037833, 0.9204423097231564, -4.697917098729501, -3.49015002179225, -5.323720443133665, -3.631813598896044, 0.6075248348145657, -1.8198249569449407, 2.28691706928627, -2.2247578549433733, 2.977195766069702, -2.4728444849440745, 1.0]
        self.upstack_heuristic = [5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001,  -6.362534366131436, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413]


        self.bot_switching_heuristic = [-5.478123113145872, 3.402223383591709, -0.6105131066358918, -2.1131210139254177, 0.7168086229927058, -0.10459788337147047, -6.59180074500804, 2.4426511871542216, 5.1201622554777835, -0.38305290663577996, 1.1991814924223843, 1.4909142133658952, 9.337224154948123, -5.907362599319184]
        downstack_weights = self.bot_switching_heuristic[
                            :len(self.bot_switching_evaluation_function) + 1]  # + 1 for comparitive weight
        upstack_weights = self.bot_switching_heuristic[-len(self.bot_switching_evaluation_function) - 1:]

        self.downstack_comparitive_weight = downstack_weights[-1]
        self.upstack_comparitive_weight = upstack_weights[-1]

        self.downstack_weights = downstack_weights[:-1]
        self.upstack_weights = upstack_weights[:-1]

        self.all_sequences = ["IOJZOLTJZJTLIJZSLSZSLZTZSLISLLSILTJZOLLOJISTLTIZJOLZTOIJ",
                         "IOLZOJILTIOTSZIOJSSOIOOTLTILSOTISITJIZISJSLSLTIZJOLZTOIJ",
                         "ISLTOJIOSJTOIZOLTTOLZIOZOJTJJTTSSJZISTJOTISLLTIZJOLZTOIJ",
                         "IOTSIJLOZSOTSZLJTZJSTLILSSIJZLSOSITIZLSZJTZILTIZJOLZTOIJ",
                         "ILTLJLTIOSILZOZTJOLISTLZJSJZOTLITZLSOOZLJIOTLTIZJOLZTOIJ",
                         ]

        self.board = Logic.new_board()
        self.sequence = Logic.c2_sequence()
        # self.sequence = self.all_sequences[self.total_games]
        # self.board = Logic.garbage_testboard()
        self.past_index = 0
        self.new_stone()
        self.chance = 1000000
        self.max_pieces_allowed = 10000
        self.max_comboable_lines = 0



        # self.test_heuristic = [-6.788661060737925, -0.18071408346839576, -0.6129764319410476, -6.491684811213475, 0.9851195204040832, 1.0] #3well
        # self.test_heuristic = [-11.40723890543657, -0.7258316910211864, -0.9539790730596944, -4.0229236843267415, 1.052124530877725, 1.0] #3well
        # self.test_heuristic = [-9.856874254407058, -0.11834748877014567, -3.635737961083114, -2.150152850161014, -5.65631621604849, -6.509423163561167, 1.0]#3well
        # self.test_heuristic = [-4.18620188340078, -1.048353893587417, -5.298188122546413, 8.905815128975295, -0.789494579903903, -4.633350820817358, -3.5864721640866506, 1.0]#3well
                # self.test_heuristic = [1.43191266580701, 0.6421009127394393, -2.143319400575507, 7.290114798109965, 1.3097832446301547, -1.734966771804288, 0.9212255434928296, 1.0]#3well best so far
        # self.test_heuristic = [-8.512359642442782, 0.8049292884424848, -0.7137023837398611, -6.500455137554349, 1.7606033350777741, 1.0] #4well

        self.combo_dropped_pieces = 0
        self.dropped_pieces = 0
        self.score = 0
        self.time_left = 0
        self.counter = 0
        self.switch_flag = False

        # self.mode = f_compute_score # different AI behaviours based on heuristic
        # self.mode_evaluate = f_evaluate # different AI behaviours based on heuristic
        # self.mode = f_compute_score_combowell
        # self.mode_evaluate = f_evaluate_combowell


        self.printed = False
        self.past_neg = False

    def center_msg(self, msg):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height // 2 - msgim_center_y + i * 22))

    def below_msg(self, msg, offset_below):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height + offset_below - msgim_center_y + i * 22))

    def draw_matrix(self, matrix, offset):
        off_x, off_y = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                try:
                    colors[val]
                except:
                    val = 0

                if val:
                    pygame.draw.rect(
                        self.screen,
                        colors[val],
                        pygame.Rect(
                            (off_x + x) *
                            config['cell_size'],
                            (off_y + y) *
                            config['cell_size'],
                            config['cell_size'],
                            config['cell_size']), 0)

    def quit(self):
        self.center_msg("Exiting...")
        pygame.display.update()
        pygame.quit()
        sys.exit()

    def output_matrix(self):
        print(self.board)

    def toggle_pause(self):
        self.paused = not self.paused

    def start_game(self):
        if self.gameover:
            self.init_game()
            self.gameover = False

    def run(self):
        key_actions = {
            'ESCAPE': self.quit,
            'p': self.toggle_pause,
            'SPACE': self.start_game,
            'm' : self.output_matrix
        }

        self.gameover = False
        self.paused = False

        pygame.time.set_timer(pygame.USEREVENT + 1, config['delay'])
        dont_burn_my_cpu = pygame.time.Clock()
        while 1:
            if self.gameover != True:
                # print(self.sequence, self.total_games)

                columns = AIComboWell2.getAllColumns(self.board)
                self.past_index, self.stone2_index = Logic.c2_randomizer(self.past_index, self.sequence)

                bot_switch_eval = AIComboWell2.bot_switch_heuristics(columns, self.board)
                # final_switch_eval = bot_switch_eval + [combo_dropped_pieces] + [combo_timer]
                combo_timer = 1 if self.time_left > 0 else 0

                # print( bot_switch_eval[3], bot_switch_eval[1])

                if (bot_switch_eval[3] > 4 and bot_switch_eval[4] > 1) or AIDownstack2.garbage_number(self.board) >= self.combo_garbage_threshold:
                    downstack_moves = AIDownstack2.f_evaluate(self.board, (self.stone_index, self.stone2_index), self.gooddownstack_heuristic,
                                                              self.counter)
                    best_move = max(downstack_moves)

                else:

                    if self.bot_mode == 'combo':

                        combo_moves = AIComboWell2.f_evaluate_combowell(self.board, (self.stone_index, self.stone2_index),
                                                                         self.upstack_heuristic,
                                                                         self.counter)
                        best_move = max(combo_moves)
                        # print(well_prop[0],  best_move[0])
                        self.switch_flag = False
                        self.started_combo = False

                        # if well_prop[0] >= self.combo_start_threshold or AIDownstack.garbage_number(self.board) > self.combo_garbage_threshold or pile_height >= self.piece_height_threshold:
                        if bot_switch_eval[3] >= 14 or bot_switch_eval[1] >= 13:
                            self.bot_mode = 'downstack'
                            self.start_combo_height = self.combo_start_threshold

                    if self.bot_mode == 'downstack':
                        # all_moves = AIDownstack2.f_evaluate(all_moves, self.test_heuristic, self.counter)
                        downstack_moves = AIDownstack2.f_evaluate(self.board, (self.stone_index, self.stone2_index), self.downstack_heuristic,
                                                                  self.counter)
                        best_move = max(downstack_moves)

                        if self.switch_flag == False:
                            self.switch_flag = True
                            self.time_left = 0
                            self.counter = 0

                        if combo_timer != 0: self.started_combo = True
                        # print(best_move)
                        # if AIDownstack2.garbage_number(self.board) <= self.combo_garbage_threshold and well_prop[0] == 0:
                        #     self.bot_mode = 'combo'
                        if self.start_combo_height > 1 and combo_timer == 0 and self.started_combo == True:
                            self.bot_mode = 'combo'
                            self.start_combo_height = 0



                print(self.bot_mode, combo_timer )





                if bot_switch_eval[1] > self.max_comboable_lines:
                    self.max_comboable_lines = bot_switch_eval[1]

                for i in range(best_move[3]):
                    self.stone = Logic.rotate_clockwise(self.stone)

                self.board, line_clear = Logic.drop_piece(self.board, self.stone, best_move[2], self.stone_y)

                # print(AIComboWell2.h_death(self.board))
                self.dropped_pieces += 1
                # print('running')

                # if rand(self.chance) == 1: # just add garbage randomly
                #     self.board = Logic.add_garbage(self.board)

                self.score, self.time_left, self.counter = AIComboWell2.f_score(line_clear,
                                                                   (self.score, self.time_left, self.counter))

                self.stone_index = self.stone2_index
                self.stone = Logic.tetris_shapes[self.stone_index]

            self.screen.fill((0, 0, 0))

            if AIComboWell2.h_death(self.board) != 0 or self.dropped_pieces == self.max_pieces_allowed:
                # self.gameover = True
                # columns = getAllColumns(self.board)
                # print(h_eval(columns, self.board))
                self.total_games += 1
                self.init_game()
                self.printed = False



            if self.gameover:
                self.center_msg("""Game Over!
Press space to continue""")
                self.below_msg(
                    'Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(self.counter),
                    20)
            #     self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)
            else:
                if self.paused:
                    self.center_msg("Paused")
                else:
                    self.draw_matrix(self.board, (0, 0))
                    self.draw_matrix(self.stone,
                                     (self.stone_x,
                                      self.stone_y))
                    pygame.draw.line(self.screen, (255, 255, 255), (self.width, 0), (self.width, self.height), 5)
                    next_index = Logic.peek_next_piece(self.past_index, self.sequence)
                    self.draw_matrix(Logic.tetris_shapes[next_index],
                                     (self.stone_x + 8,
                                      self.stone_y + 1))
                    self.below_msg('Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(
                        self.counter), 20)
                    # self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)
                    columns = AIComboWell2.getAllColumns(self.board)
                    total_comboable_lines = AIComboWell2.bot_switch_heuristics(columns, self.board)
                    self.below_msg(
                        'Comboable lines: ' + str(total_comboable_lines[1]),
                        60)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                elif event.type == pygame.KEYDOWN:
                    for key in key_actions:
                        if event.key == eval("pygame.K_"
                                             + key):
                            key_actions[key]()

            dont_burn_my_cpu.tick(config['maxfps'])


if __name__ == '__main__':
    App = TetrisApp()
    App.run()
