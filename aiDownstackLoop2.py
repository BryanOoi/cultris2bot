from AIDownstack2 import *


def AISurvivor(heuristic):
    this_pieces = 1500
    stone_y = 0 # dont change this
    games = 5
    piece_chance = 5
    dropped_pieces = 0
    current_pieces = 0


    for i in range(games):
        sequence = Logic.c2_sequence()
        past_index = 0

        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        # past_index, stone_index = Logic.history_randomiser(history)
        # history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0
        board = Logic.new_board()
        # dropped_pieces = 0


        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)
            # history.append(stone2_index)


            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)
            # Logic.print_matrix(board)
            # print()
            if h_death(board) != 0:
                break

            dropped_pieces += 1

            # if rand(piece_chance) == 1: board = Logic.add_garbage(board)
            if dropped_pieces % piece_chance == 0:  board = Logic.add_garbage(board)# just add garbage randomly



            score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]
        # print(dropped_pieces - current_pieces)
        current_pieces = dropped_pieces


    return float(dropped_pieces/games)


def AICombo(heuristic):
    this_pieces = 15
    stone_y = 0 # dont change thi
    # piece_chance = 5
    dropped_pieces = 0
    current_pieces = 0
    highest_counter = 0
    total_combo = 0
    all_sequences = ["IZOTJISLSITOTSOLJOZTZJZOZJZLOTILTLSITILITISL",
                    "IOZSLOJOLITLJISLTISTJLOZSOJTSILLOSLOJTJZOSJO",
                    "ILOTTSTZILLTISTZTLZTZOTSOLLZOILSLZOJIZITOLOS",
                    "IILJTSZOSTTSJZOJIJLOZIJLZJITTSIZJTLJOTTZTOJI",
                    "IJLSJZLITOIJTTIOZOJSOLIOSOJOZITJIJZTZSZISSZO",
                    "IZJOZSITOJSLZLISTZLJZJTLZIZOJZSJZOIOZSILOZJI",
                    "IOTLZSZIJLZIZTIOJTLOLJOLJILZLJOZSLSTLJSOILOZ",
                    "ISJLSZJIZIJITOSTLIZOLSITJZSTIJLTOTOIZLOSJSTZ",
                    "IITSZLIZOSIJZLTSZILJTSOZILSLSJILSLTJLIJITSZT",
                    "IILSZLOZISOIZTLZLTJISLITJOLZILTIZOIZLTJZZLJS",
                    "ILOJLLTJSIZOISTOITOTLZOSJJOZJOJTZIZLZLZJTJIO",
                    "ILOSSZLSTSZIOSTLOSLTLSLOZZLJZOZSTITZJLJITSLL",
                    "IJZOZISJIJILZILZOIJOZSTOZJSLZTOJZOTZLTSSJOJZ",
                    "ISOZISOZOJLSLSLTSLZSOJLOSLJSZLZOLJOLSJTSLOSZ",
                    "IZJTOIZIJLZIJTJLZTSLTIOLZJTJJSIITZTIOSOLIJLT",
                    "ISZLLIOSOZLSJOISZIJZLLSOTZJIZOOIZTOZJLSTJILZ",
                    "IOZOJLSTSLILSOTLTIOZSOTJSZLOSILJOSTSTISTLSLJ",
                    "IJISJZOZOITZLIZLSILJOZLTZJIJIIOZLTSOTIZOTLSZ",
                    "ISLILSTOZSTOSLZLZLOLSITLOZLIZJTTZLSOLOITOJTO",
                    "IOLOSTZJIZJSIZJLSTIZOTSOLZJOISIJZLOLIJISZOZI",
                    "IIISOTTLTJSLZOSOZZOTLOSTLIZTSIOJSOJTISTOZJIT",
                    "IZSTLJZZSJLJLIJOZJZTZLSJZILIOZJZOTOSTZSTSIOL",
                    "IOLTJTJSOISOZOTJTOIZSLOOZSTIJOSTLZOIJTJIOTIT",
                    "ISTJIOZTSOZTOJSTOZOILJTLZSSZJOLSJSTITJZTILST",
                    "ISLIJITZTLZTJZTTSSOIZTSZSJLZOITZOLJLLSTJLOIO",
                    "IOSTOJLSISOJZILJZOZSLOSIOSJLTOILOZSTLIJSLOTJ",
                    "IJOISLTZJSTOZZTZLZOTZTZLSZZSIZLITSTSTSJZSTTO",
                    "IOSOTZLZTILZSZOJSLITZTLIOTIJJZTSOZJSJZZOLJLI",
                    "IZTJIOZJSILZOJTLJZSIOLOTJSTISTJZIZLJTOTJOTSL",
                    "ISTOJLZJTZOTTZJIZTLIOSTJZSJIJIOTTJOITZLIOZJZ",
                    "ITJIZSISTZOTZZLOSLJOSZZISZLTOSZZOSTLSOJILTZL",
                    "IZLJIZTSLIOJZTISZLZIJTLOTZOTLSTSJILTZSZTJITL",
                    "IZLSIZSTZIZOILZJLIZJLOSOZSOSTSOSJLOIZTLJOZTL"]
    games = len(all_sequences)

    for i in range(games):
        # sequence = Logic.c2_sequence()
        sequence = all_sequences[i]
        past_index = 0

        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        # past_index, stone_index = Logic.history_randomiser(history)
        # history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0
        board = Logic.garbage_testboard()
        # dropped_pieces = 0


        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)
            # history.append(stone2_index)


            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)
            # Logic.print_matrix(board)
            # print()
            if h_death(board) != 0:
                break

            dropped_pieces += 1


            # if rand(piece_chance) == 1: board = Logic.add_garbage(board)
            # if dropped_pieces % piece_chance == 0:  board = Logic.add_garbage(board)# just add garbage randomly



            score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            if counter > highest_counter:
                highest_counter = counter
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]
        # print(dropped_pieces - current_pieces)
        current_pieces = dropped_pieces
        total_combo += highest_counter
        # print(highest_counter, total_combo)

    return float((total_combo) / games)
# print(AICombo([-18.416011191928167, -2.478022465684638, -2.093717512880721, -11.165687411613462, -1.5602406271093106, -7.043252563877942, -2.8947492288645438, -2.410985717667043, -2.4667689688972185, -0.9649826164595063, 1.0]))

def AICheese(heuristic):

    this_pieces = 60 # enough pieces not to top out
    stone_y = 0 # dont change this
    games = 10
    combo_score = 0
    downstack_score = 0
    total_cells_left = 0
    # board_list = [Logic.garbage_testboard(), Logic.garbage_testboard2()]
    board_list = Logic.garbage_testboard2()
    count = 0
    total_pieces = 0




    total_score = 0
    current_pieces = 0

    for i in range(games):
        board = [row[:] for row in board_list]
        history = []
        sequence = Logic.c2_sequence()
        past_index = 0

        time_left = 0
        counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        # history, stone_index = Logic.history_randomiser(history)
        # history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0



        # if_temp_punish = 5 # Change this later haha, just to test out punish
        for j in range(this_pieces):
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)


            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)
            total_pieces += 1
            # Logic.print_matrix(board)
            # print()
            # print(i, total_pieces)

            if garbage_number(board) == 0:
                break

            # if h_death(board) != 0:
            #     total_pieces += 100
            #     break


            # score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]

        # print(total_pieces - current_pieces)
        current_pieces = total_pieces

        # Logic.print_matrix(board)
        # print(score)
        # print(total_cells_left)
        total_score += score
        # total_cells_left += count_cells(board)
    # if count == 0:combo_score += total_score
    # elif count == 1: downstack_score += total_score
    count += 1

    # return total_cells_left ,combo_score, downstack_score,
    return total_pieces
    # return score

# print(AISurvivor([-14.615564925738749, -0.5733085076786004, -2.4165907574695513, -5.786048420877837, 0.8096842034990313, -9.411100830992275, -4.442391962413825, -2.9829188713925605, -5.554172405128296, -1.4495149228913702, 1.0]))
# print(AICheese([-0.9967178965926706, -0.06913007555990358, -0.5730157908848899, -0.8880383872775097, -0.4384798822212428, 0.34555514522426356, 1.0]))
