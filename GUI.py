from tkinter import *


matrix_height = 18
matrix_width = 10
grid_width = 25
canvas_width = grid_width * matrix_width
canvas_height = grid_width * matrix_height
master = None


def initialise_grid():
    master = Tk()
    #Button(master, text="Quit", command=master.destroy).pack()
    canvas = Canvas(master, 
           width=canvas_width,
           height=canvas_height)
    canvas.pack()
    canvas.configure(background='black')
    for i in range(18):
        canvas.create_line(0, grid_width * i, canvas_width, grid_width * i, fill="white")
    for i in range(10):
        canvas.create_line(grid_width * i, 0, grid_width * i, canvas_height, fill="white")
    return canvas,master
        
def colour_in(x,y,colour,canvas):
    canvas.create_rectangle(x * grid_width + 1, grid_width + y * grid_width - 1 , grid_width + x * grid_width - 1, y * grid_width + 1, fill=colour)

def quit():
    master.destroy()
    
#canvas,master = initialise_grid()
#colour_in(1,1,'grey',canvas)
#mainloop()
