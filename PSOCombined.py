#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import operator
import random

import numpy

from aiCombinedLoop import *

from deap import base
from deap import benchmarks
from deap import creator
from deap import tools

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Particle", list, fitness=creator.FitnessMax, speed=list,
               smin=None, smax=None, best=None)


def generate(size, pmin, pmax, smin, smax):
    upstack_heuristic = [-1.8108083582364949, 0.06718761640149484, -1.6848279668694497, 5.238292340368663,
                              -4.879422280310628, -2.9736692788019305, -6.843726017391311]
    downstack_heuristic = [-16.290754887515817, 0.19238326474753897, -2.1958628485819096, -10.296035984217246,
                                -2.283786062669602, -2.230615759651896, -4.290557279764613, -2.212799795970108,
                                -3.0184366695277536, -1.5526381594510072]
    bot_switching_heuristic = [7.698523987826089, -11.417229084339112, 4.238687213934263, 5.617614859312784]

    # part = creator.Particle(
    #     [-6.969116270288395, -1.3570825338443548, -2.015892171622093, 7.127730336613408, 1.5274513319506218,
    #      -7.2944303039403575, -5.360962887895775, -0.5702399832798122, -1.104977781930062, -3.3533337544085944,
    #      -11.93472094557804, -17.163237818935634, -6.66944016907069, -5.605872836666904, -2.520585844918764,
    #      2.9568291483507414, -0.26222455047446314])

    # part = creator.Particle(upstack_heuristic + downstack_heuristic + [random.uniform(pmin, pmax) for _ in range(len(bot_switching_evaluation_function))])
    # part = creator.Particle(upstack_heuristic + downstack_heuristic + bot_switching_heuristic)

    part = creator.Particle(random.uniform(pmin, pmax) for _ in range(size))

    part.speed = [random.uniform(smin, smax) for _ in range(size)]
    part.smin = smin
    part.smax = smax
    return part


def updateParticle(part, best, phi1, phi2):
    u1 = (random.uniform(0, phi1) for _ in range(len(part)))
    u2 = (random.uniform(0, phi2) for _ in range(len(part)))
    v_u1 = map(operator.mul, u1, map(operator.sub, part.best, part))
    v_u2 = map(operator.mul, u2, map(operator.sub, best, part))
    part.speed = list(map(operator.add, part.speed, map(operator.add, v_u1, v_u2)))
    for i, speed in enumerate(part.speed):
        if speed < part.smin:
            part.speed[i] = part.smin
        elif speed > part.smax:
            part.speed[i] = part.smax
    part[:] = list(map(operator.add, part, part.speed))

def evalOneMax(individual):
    return AICombined(individual),

toolbox = base.Toolbox()
toolbox.register("particle", generate, size=len(total_evaluation_function), pmin=-11, pmax=11, smin=-3, smax=3)
# toolbox.register("particle", generate, size=len(bot_switching_evaluation_function), pmin=-11, pmax=11, smin=-3, smax=3)
toolbox.register("population", tools.initRepeat, list, toolbox.particle)
toolbox.register("update", updateParticle, phi1=2.0, phi2=2.0)
toolbox.register("evaluate", evalOneMax)


def main():
    pop = toolbox.population(n=150)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    logbook = tools.Logbook()
    logbook.header = ["gen", "evals"] + stats.fields

    GEN = 1000
    best = None

    for g in range(GEN):
        for part in pop:
            part.fitness.values = toolbox.evaluate(part)
            if not part.best or part.best.fitness < part.fitness:
                part.best = creator.Particle(part)
                part.best.fitness.values = part.fitness.values
            if not best or best.fitness < part.fitness:
                best = creator.Particle(part)
                best.fitness.values = part.fitness.values
        for part in pop:
            toolbox.update(part, best)

        # Gather all the fitnesses in one list and print the stats
        logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
        print(logbook.stream)
        print(best)

    return pop, logbook, best


# part = creator.Particle(
#     [-7.785941479849217, -1.3289324919502445, -1.4978688810744512, 9.550378483768574, -0.39211687322053024,
#      -6.315376253696585, -11.354763539370138, -3.214049548039288, -0.3293455577366933, -4.473791357962756,
#      -8.997039285308196, -16.614838050576274, -3.728402997075377, -7.423406068086689, -3.722326800389593,
#      3.8941852338059895, 1.907339496546241] + [random.uniform(11, 12) for _ in range(len(bot_switching_evaluation_function))])
#
# print(len(part))
# print(len(evaluation_function) + len(bot_switching_evaluation_function))


if __name__ == "__main__":
    main()