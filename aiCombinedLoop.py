import AIComboWell2
import AIDownstack2
import Logic

# evaluation_function = [
#     'bumpiness',
#     'total_comboable_lines ',
#     'garbage_height ',
#     'height',
#     'combo_timer'
#
# ]

total_evaluation_function = ['bumpiness', 'total_comboable_lines', 'max(heights_list)' , 'average_height', 'column_hole', 'well_holes', 'comparitive_weight',
                             'bumpiness2', 'total_comboable_lines2', 'max(heights_list)2', 'average_height2', 'column_hole2', 'well_holes2', 'comparitive_weight2' ]

bot_switching_evaluation_function = [ 'bumpiness', 'total_comboable_lines', 'max(heights_list)' , 'average_height', 'column_hole', 'well_holes' ]

combined_evaluation_function = AIComboWell2.evaluation_function + AIDownstack2.evaluation_function

def AICombined(bot_switching_heuristic):
    downstack_weights = bot_switching_heuristic[:len(bot_switching_evaluation_function) + 1] # + 1 for comparitive weight
    upstack_weights = bot_switching_heuristic[-len(bot_switching_evaluation_function) - 1:]

    downstack_comparitive_weight = downstack_weights[-1]
    upstack_comparitive_weight = upstack_weights[-1]

    downstack_weights = downstack_weights[:-1]
    upstack_weights = upstack_weights[:-1]

    # print(bot_switching_heuristic)
    # print(downstack_weights, downstack_comparitive_weight)
    # print(upstack_weights, upstack_comparitive_weight)




    downstack_heuristic = [-16.899981984468326, -1.2222178996885265, -4.209828580275611, -10.109502163038567, -3.0396864880454073, -6.69422534642721, -1.0078881712181516, -2.2402429801710597, -4.540419421371857, -3.8344509679313763, 1.0]
    upstack_heuristic = [5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413]
    this_pieces = 50
    bot_change_piece_threshold = 30
    stone_y = 0 # dont change this

    total_score = 0
    # sum_comboable_lines = 0
    total_combo = 0
    sum_comboable_lines_2 = 0
    all_sequences = ["IOJZOLTJZJTLIJZSLSZSLZTZSLISLLSILTJZOLLOJISTLTIZJOLZTOIJ",
                    "IOLZOJILTIOTSZIOJSSOIOOTLTILSOTISITJIZISJSLSLTIZJOLZTOIJ",
                    "ISLTOJIOSJTOIZOLTTOLZIOZOJTJJTTSSJZISTJOTISLLTIZJOLZTOIJ",
                    "IOTSIJLOZSOTSZLJTZJSTLILSSIJZLSOSITIZLSZJTZILTIZJOLZTOIJ",
                    "ILTLJLTIOSILZOZTJOLISTLZJSJZOTLITZLSOOZLJIOTLTIZJOLZTOIJ",
                    ]
    games = len(all_sequences)


    # pieces_potential = {0: 2,
    #                     1: 2,
    #                     2: 2,
    #                     6: 2,
    #                     3: 3,
    #                     4: 3,
    #                     5: 4
    #                     }

    for i in range(games):

        # sequence = Logic.c2_sequence()
        sequence = all_sequences[i]
        # print(sequence, i)
        past_index = 0
        time_left = 0
        counter = 0
        highest_counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        combo_dropped_pieces = 0
        # dropped_pieces = 0
        max_comboable_lines = 0
        switch_flag = False



        board = Logic.new_board()

        stone = Logic.tetris_shapes[stone_index]
        score = 0


        for j in range(this_pieces):
            columns = AIDownstack2.getAllColumns(board)
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)




            # combo_timer = 1 if time_left > 0 else 0

            combo_timer = 1 if time_left > 0 else 0
            bot_switch_eval = AIComboWell2.bot_switch_heuristics(columns, board)
            # final_switch_eval = bot_switch_eval + [combo_dropped_pieces] + [combo_timer]
            final_switch_eval = bot_switch_eval

            assert (len(bot_switching_evaluation_function) == len(final_switch_eval))

            downstack_decision = sum(x * y for x, y in zip(downstack_weights, final_switch_eval))
            upstack_decision = sum(x * y for x, y in zip(upstack_weights, final_switch_eval))

            best_move = 'wrong'

            if downstack_decision > downstack_comparitive_weight:
                if switch_flag == False:
                    switch_flag = True
                    time_left = 0
                    counter = 0
                # combo_dropped_pieces = 0
                downstack_moves = AIDownstack2.f_evaluate(board, (stone_index, stone2_index), downstack_heuristic,
                                                          counter)
                best_move = max(downstack_moves)

            elif upstack_decision > upstack_comparitive_weight:
                combo_moves = AIComboWell2.f_evaluate_combowell(board, (stone_index, stone2_index), upstack_heuristic,
                                                                counter)
                best_move = max(combo_moves)
                switch_flag = False
                # combo_dropped_pieces += 1

            if best_move == 'wrong':
                return 0

            # decision = sum(x * y for x, y in zip(bot_switching_heuristic, final_switch_eval))
            #
            # if decision > -0.001:
            #     combo_moves = AIComboWell2.f_evaluate_combowell(board, (stone_index, stone2_index), upstack_heuristic,
            #                                                     counter)
            #     best_move = max(combo_moves)
            #     switch_flag = False
            #     combo_dropped_pieces += 1
            #
            # else:
            #     if switch_flag == False:
            #         switch_flag = True
            #         time_left = 0
            #         counter = 0
            #     combo_dropped_pieces = 0
            #     downstack_moves = AIDownstack2.f_evaluate(board, (stone_index, stone2_index), downstack_heuristic,
            #                                               counter)
            #     best_move = max(downstack_moves)

            if bot_switch_eval[1] > max_comboable_lines:
                max_comboable_lines = bot_switch_eval[1]




            # if dropped_pieces == bot_change_piece_threshold:
            #     bot_switch_eval = AIComboWell2.bot_switch_heuristics(columns, board)
            #     total_comboable_lines = bot_switch_eval[1]
            #     # print("BEFORE DOWNSTACK", total_comboable_lines)
            #     sum_comboable_lines += total_comboable_lines
            #     time_left = 0
            #     counter = 0
            #
            # if dropped_pieces < bot_change_piece_threshold:
            #     combo_moves = AIComboWell2.f_evaluate_combowell(board, (stone_index, stone2_index),
            #                                                     upstack_heuristic,
            #                                                     counter)
            #     # print(upstack_heuristic)
            #     best_move = max(combo_moves)
            # else:
            #     downstack_moves = AIDownstack2.f_evaluate(board, (stone_index, stone2_index), downstack_heuristic,
            #                                               counter)
            #     # print(downstack_heuristic)
            #     best_move = max(downstack_moves)


            # bumpiness = bot_switch[0]
            # total_comboable_lines = bot_switch[1]
            # garbage_height = bot_switch[2]

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            # dropped_pieces += 1



            # Logic.print_matrix(board)
            # print()
            # print(decision)


            score, time_left, counter = AIDownstack2.f_score(line_clear, (score, time_left, counter))
            if counter > highest_counter:
                highest_counter = counter
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]


        # print('Score is : ', score)
        # print('Game ' + str(i) + " ", well_prop[0])

        total_combo += highest_counter
        sum_comboable_lines_2 += max_comboable_lines
        # print(i)
        # print(total_combo)
        # print(sum_comboable_lines)

    # print(total_score)
    # return float((2*total_combo + sum_comboable_lines_2)/ games)
    return float((2*total_combo + sum_comboable_lines_2)/ games)
    # return score
    # return score


#train hard sequence along with random sequence (get hard sequence from upstack)
#make bot 2 wide
#make bot hybrid
#make bot switching based on bumpiness, total_comboable_lines, dropped_pieces, column_spread


# bot_switching_evaluation_function = [ 'bumpiness', 'total_comboable_lines', 'dropped_pieces']
def AICombinedFresh(heuristic):
    upstack_heuristic_length = len(AIComboWell2.evaluation_function)
    downstack_heuristic_length = len(AIDownstack2.evaluation_function)
    bot_switching_heuristic_length = len(bot_switching_evaluation_function)

    upstack_heuristic = heuristic[:upstack_heuristic_length]
    downstack_heuristic = heuristic[ upstack_heuristic_length: upstack_heuristic_length + downstack_heuristic_length]
    bot_switching_heuristic = heuristic[ -bot_switching_heuristic_length : ]

    # print(upstack_heuristic)
    # print(downstack_heuristic)
    # print(bot_switching_heuristic)

    # print(len(upstack_heuristic))
    # print(len(downstack_heuristic))
    # print(len(bot_switching_heuristic))
    # print(len(heuristic))
    # print(heuristic)


    this_pieces = 50
    bot_change_piece_threshold = 30
    stone_y = 0 # dont change this
    games = 5
    total_score = 0
    # sum_comboable_lines = 0
    total_combo = 0
    sum_comboable_lines_2 = 0



    # downstack_heuristic = [-17.276457248668144, -1.0277900160852036, -2.5972220372333967, -10.140870791169988, -3.215888862620477, -4.5936985028519155, -1.8333659869536767, -1.1033026898586424, -3.0377701711652683, -1.2974662440511118, 1.0]
    # upstack_heuristic = [-1.8108083582364949, 0.06718761640149484, -1.6848279668694497, 5.238292340368663, -4.879422280310628, -2.9736692788019305, -6.843726017391311, 1.0]

    # pieces_potential = {0: 2,
    #                     1: 2,
    #                     2: 2,
    #                     6: 2,
    #                     3: 3,
    #                     4: 3,
    #                     5: 4
    #                     }

    for i in range(games):
        sequence = Logic.c2_sequence()
        past_index = 0
        time_left = 0
        counter = 0
        highest_counter = 0
        past_index, stone_index = Logic.c2_randomizer(past_index, sequence)
        combo_dropped_pieces = 0
        # dropped_pieces = 0
        switch_flag = False

        max_comboable_lines = 0

        board = Logic.new_board()

        stone = Logic.tetris_shapes[stone_index]
        score = 0


        for j in range(this_pieces):
            columns = AIDownstack2.getAllColumns(board)
            past_index, stone2_index = Logic.c2_randomizer(past_index, sequence)




            # combo_timer = 1 if time_left > 0 else 0

            combo_timer = 1 if time_left > 0 else 0
            bot_switch_eval = AIComboWell2.bot_switch_heuristics(columns, board)
            final_switch_eval = bot_switch_eval + [combo_dropped_pieces] + [combo_timer]

            assert (len(bot_switching_evaluation_function) == len(final_switch_eval))
            decision = sum(x * y for x, y in zip(bot_switching_heuristic, final_switch_eval))

            if decision > -0.001:
                combo_moves = AIComboWell2.f_evaluate_combowell(board, (stone_index, stone2_index), upstack_heuristic,
                                                                counter)
                best_move = max(combo_moves)
                switch_flag = False
                combo_dropped_pieces += 1

            else:
                if switch_flag == False:
                    switch_flag = True
                    time_left = 0
                    counter = 0
                combo_dropped_pieces = 0
                downstack_moves = AIDownstack2.f_evaluate(board, (stone_index, stone2_index), downstack_heuristic,
                                                          counter)
                best_move = max(downstack_moves)

            if bot_switch_eval[1] > max_comboable_lines:
                max_comboable_lines = bot_switch_eval[1]




            # if dropped_pieces == bot_change_piece_threshold:
            #     bot_switch_eval = AIComboWell2.bot_switch_heuristics(columns, board)
            #     total_comboable_lines = bot_switch_eval[1]
            #     # print("BEFORE DOWNSTACK", total_comboable_lines)
            #     sum_comboable_lines += total_comboable_lines
            #     time_left = 0
            #     counter = 0
            #
            # if dropped_pieces < bot_change_piece_threshold:
            #     combo_moves = AIComboWell2.f_evaluate_combowell(board, (stone_index, stone2_index),
            #                                                     upstack_heuristic,
            #                                                     counter)
            #     # print(upstack_heuristic)
            #     best_move = max(combo_moves)
            # else:
            #     downstack_moves = AIDownstack2.f_evaluate(board, (stone_index, stone2_index), downstack_heuristic,
            #                                               counter)
            #     # print(downstack_heuristic)
            #     best_move = max(downstack_moves)


            # bumpiness = bot_switch[0]
            # total_comboable_lines = bot_switch[1]
            # garbage_height = bot_switch[2]

            for k in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            # dropped_pieces += 1



            # Logic.print_matrix(board)
            # print()
            # print(decision)


            score, time_left, counter = AIDownstack2.f_score(line_clear, (score, time_left, counter))
            if counter > highest_counter:
                highest_counter = counter
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]


        # print('Score is : ', score)
        # print('Game ' + str(i) + " ", well_prop[0])

        total_combo += highest_counter
        sum_comboable_lines_2 += max_comboable_lines
        # print(i)
        # print(total_combo)
        # print(sum_comboable_lines)

    # print(total_score)
    # return float((2*total_combo + sum_comboable_lines_2)/ games)
    return float((total_combo)/ games)
    # return score

# b = [-1.8108083582364949, 0.06718761640149484, -1.6848279668694497, 5.238292340368663, -4.879422280310628, -2.9736692788019305, -6.843726017391311]
# a = [-17.276457248668144, -1.0277900160852036, -2.5972220372333967, -10.140870791169988, -3.215888862620477, -4.5936985028519155, -1.8333659869536767, -1.1033026898586424, -3.0377701711652683, -1.2974662440511118]
# #
# print(AICombinedFresh([-6.969116270288395, -1.3570825338443548, -2.015892171622093, 7.127730336613408, 1.5274513319506218, -7.2944303039403575, -5.360962887895775, -0.5702399832798122, -1.104977781930062, -3.3533337544085944, -11.93472094557804, -17.163237818935634, -6.66944016907069, -5.605872836666904, -2.520585844918764, 2.9568291483507414, -0.26222455047446314]))