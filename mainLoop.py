
import AIDownstack
# import AIComboWell2
import AIDownstack2
import AIComboWell3
import AIDownstackCombo
import pygame, sys
from TetrisImageProcessing import *
from KeyboardEvent import *

config = {
    'cell_size': 25,
    'cols': 10,
    'rows': 18,
    'delay': 0,
    'maxfps': 3.3
}

colors = [
    (0, 0, 0),
    (245, 54, 249),
    (70, 249, 54),
    (249, 54, 54),
    (54, 99, 249),
    (249, 165, 54),
    (56, 231, 255),
    (249, 239, 42),
    (135, 130, 129)
]

tetris_shapes = [
    [[0, 1, 0],
     [1, 1, 1]],

    [[0, 2, 2],
     [2, 2, 0]],

    [[3, 3, 0],
     [0, 3, 3]],

    [[4, 0, 0],
     [4, 4, 4]],

    [[0, 0, 5],
     [5, 5, 5]],

    [[6, 6, 6, 6]],

    [[7, 7],
     [7, 7]]
]


##canvas,master = GUI.initialise_grid()
##score = 0
##time_left = 0 
##counter = 0
##board = Logic.new_board()
##stone_y = 1
##
##
##stone_index = rand(len(Logic.tetris_shapes))
##stone = Logic.tetris_shapes[stone_index]
##
##for i in range(5):
##    stone2_index = rand(len(Logic.tetris_shapes))
##    ##stone = tetris_shapes[rand(len(tetris_shapes))]
##
##
##    all_moves = f_evaluate(board,(stone_index,stone2_index),test_heuristic)
##    best_move = max(all_moves)
##    
##    for i in range(best_move[3]):
##        stone = Logic.rotate_clockwise(stone)
##    board,line_clear = Logic.drop_piece(board,stone,best_move[2],stone_y)
##    print(f_compute_score(test_heuristic, board, line_clear,'yes'))
##    
##    #Logic.print_matrix(board)
##    draw(board,canvas)
##    score, time_left, counter = f_score(line_clear, (score, time_left, counter))
##    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))    
##    stone_index = stone2_index
##    stone = Logic.tetris_shapes[stone_index]
##

class TetrisApp(object):
    def __init__(self):
        pygame.init()
        pygame.key.set_repeat(250, 25)
        self.width = config['cell_size'] * config['cols']
        self.height = config['cell_size'] * config['rows']

        self.rotate_r = 's'
        self.rotate_l = 'a'
        self.rotate_180 = 'd'
        self.hard_drop = 'spacebar'
        self.left = 'left_arrow'
        self.right = 'right_arrow'
        self.board = Logic.new_board()
        self.stone = Logic.tetris_shapes[6]
        self.stone_y = 1
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0]) / 2)

        self.combo_garbage_threshold = 3
        self.combo_start_threshold = 10

        self.piece_height_threshold = 15
        self.start_combo_height = 0

        self.screen = pygame.display.set_mode((self.width, self.height + 100))
        pygame.event.set_blocked(pygame.MOUSEMOTION)  # We do not need

        self.game_starts = False
        # mouse movement
        # events, so we
        # block them.
        # self.init_game()

    def new_stone(self, screen):
        self.stone_y = 1


        self.stone_index = detect_piece(screen, first_piece_location , ghost_piece_colours)
        # while self.stone_index == -1:
        #     self.stone_index = detect_piece(screen, first_piece_location, ghost_piece_colours)

        if self.stone_index == -1:
            # screen = screenShot()
            if_game_over = check_locations(screen, gameover_location)
            if_winner = check_locations(screen, winner_location)

            if if_game_over:print('gameover')
            if if_winner:print('winner')

        assert (self.stone_index != -1)

        self.stone = Logic.tetris_shapes[self.stone_index]


        self.stone_x = int(config['cols'] / 2 - len(self.stone[0]) / 2)

        if Logic.check_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
            self.gameover = True

    def init_game(self):
        # self.board = Logic.new_board()
        screen = screenGrab()
        self.new_stone(screen)
        self.score = 0
        self.time_left = 0
        self.counter = 0
        self.bot_mode = 'combo'
        self.started_combo = False


        #Combowell
        # self.combo_heuristic = [0.11962424268553473, -0.14433435149516605, -0.6462562655398616, -0.1871457461209567, -0.07709075265702214, -0.5120079082856313, -0.630768417220362, 0.2765981058202096, -0.09778777622972679, -0.6126352879562122, 0.5420380940295875, 0.6247756615106612, -0.585853564460656, 0.3882904943715848, 0.1971189465183909, -0.7499005947035491, 0.7788152772196213, 0.537224756389687, 0.8869159362373213, 0.11405984737793662, 0.7854698546414112, 0.8844917853773147, 0.9638300562979487, 0.483313947706965, 0.6513440780571946, 0.5025853800958919, -0.4696918007223019, -0.22775729632572062, 0.6308599858711976, 1.0]

        #downstack
        # self.test_heuristic = [-0.6083007424660973, 0.7641635860523348, 0.027712702606301542, -0.33626435277825206, 0.7089885280753359, 0.13398914921359206, -0.28817119859010876, 0.5374679121966275, 0.6356767005700121, 0.8858463287306075, 0.4108119879332768, 0.27897942933045816, 0.3305214147662576, 0.010236524278958381, -0.8706648050642543, -0.9808407133375403, -0.29378707111709645, 0.7014700553041127, 0.38252019405466275, -0.9601648925433155, -0.7296753184486149, -0.2148687297317471, 0.5499206251124991, 0.9325856247683495, 0.9723024641926317, 0.5156930360719532, 1.0]
        # self.test_heuristic = [-0.13866937501141074, 0.6348489739272136, -0.32006765248126934, 0.9720839141037461, 0.530357474170476, 0.9445677832154555, 0.6212954107039004, -0.9537191788067154, 0.36762491406234465, 0.25858315519252084, 0.35822561951868415, -0.32927282503675936, -0.7267244385615887, -0.4885744339444362, 0.34915502098614604, -0.33422887954847647, -0.1592268324208257, 0.3416704971355926, 0.686312997615776, -0.5980120474600352, -0.6571416740836731, 0.7952713125006021, 0.35566759351516297, 0.9311017652656381, -0.8834650220288938, -0.5019760599109284, 1.0]self.test_heuristic = [-0.13866937501141074, 0.6348489739272136, -0.32006765248126934, 0.9720839141037461, 0.530357474170476, 0.9445677832154555, 0.6212954107039004, -0.9537191788067154, 0.36762491406234465, 0.25858315519252084, 0.35822561951868415, -0.32927282503675936, -0.7267244385615887, -0.4885744339444362, 0.34915502098614604, -0.33422887954847647, -0.1592268324208257, 0.3416704971355926, 0.686312997615776, -0.5980120474600352, -0.6571416740836731, 0.7952713125006021, 0.35566759351516297, 0.9311017652656381, -0.8834650220288938, -0.5019760599109284, 1.0]
        # self.test_heuristic =[-18.416011191928167, -2.478022465684638, -2.093717512880721, -11.165687411613462, -1.5602406271093106, -7.043252563877942, -2.8947492288645438, -2.410985717667043, -2.4667689688972185, -0.9649826164595063, 1.0]
        # self.test_heuristic = [-14.88755157472949, -0.2531687777292899, -1.9218831485782024, -5.887400773698782,
        #                        -3.5983500674606566, -12.943185196856936, -5.813405914816728, -3.423241609460704,
        #                        -4.969498008918508, -1.0037445003159644, 1.0]  # (415 max~ pieces at 4 pieces 1 garbage)

        # self.gooddownstack_heuristic = [-18.416011191928167, -2.478022465684638, -2.093717512880721, -11.165687411613462, -1.5602406271093106, -7.043252563877942, -2.8947492288645438, -2.410985717667043, -2.4667689688972185, -0.9649826164595063, -0.4236493174444327, 0, 1.0]
        self.gooddownstack_heuristic = [-17.045569002213117, 0.058976177631637317, -2.3981153681321765, -10.516174182158453, -4.527542902312192, -7.136152217270677, -2.256001009593537, -4.872517339292467, -3.541791278179028, -0.27573349988485374, 1.0]
        # self.gooddownstack_heuristic = [-17.045569002213117, 0.058976177631637317, -2.3981153681321765, -10.516174182158453, -4.527542902312192, -7.136152217270677, -2.256001009593537, -4.872517339292467, -3.541791278179028, -0.27573349988485374, 0, 1.0]


        # self.downstack_heuristic = [-3.780082914493269, 5.1006863280910615, -6.198769848744979, -10.655992503950964, -9.585826113964568, -17.55422148317651, -9.362904543192275, 10.482153253964189, 2.9797561307884193, -6.351296257861085, -2.4111611673388906, 1.0]
        # self.downstack_heuristic = [-0.44786305948152094, 5.6864598417417636, -5.265310805003779, -8.537375746308626, -9.058989696415663, -17.232782508870013, -10.281311497459127, 14.052530666418766, 3.104551196145769, -5.051397031575696, -2.33117567110556, 1.0]
        self.downstack_heuristic = [-17.045569002213117, 0.058976177631637317, -2.3981153681321765, -10.516174182158453, -4.527542902312192, -7.136152217270677, -2.256001009593537, -4.872517339292467, -3.541791278179028, -0.27573349988485374, 1.0]

        # self.upstack_heuristic = [5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001,  -6.362534366131436, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413]
        self.upstack_heuristic = [-22.493164807426563, 5.6860534085319445, -4.422652785087381, 17.135306934797956, 4.0155283079217305, -6.031055253597078, -2.019601935692552, -1.8154341918618264, -8.767324159950496, -3.291706249650496, 5.649595395135265, 1.8313202734174499, -2.4756190069159203, 1.0]
        # self.upstack_heuristic = [-10.611626712271446, 0.10447001986041027, -8.753464627394811, 18.819153629876205, 4.330159651683704, -6.233828907494001, 0.7285168577891247, 8.014760418071834, -10.678808994749613, -4.308354418839296, -8.420930637724009, -9.553867815830937, -9.991848446672067, 1.0]




        self.mode = AIDownstack2.f_compute_score # different AI behaviours based on heuristic
        self.mode_evaluate = AIDownstack2.f_evaluate # different AI behaviours based on heuristic
        # self.mode = f_compute_score_combowell
        # self.mode_evaluate = f_evaluate_combowell


    def center_msg(self, msg):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height // 2 - msgim_center_y + i * 22))

    def below_msg(self, msg, offset_below):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height + offset_below - msgim_center_y + i * 22))

    def draw_matrix(self, matrix, offset):
        off_x, off_y = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                try:
                    colors[val]
                except:
                    val = 0

                if val:
                    pygame.draw.rect(
                        self.screen,
                        colors[val],
                        pygame.Rect(
                            (off_x + x) *
                            config['cell_size'],
                            (off_y + y) *
                            config['cell_size'],
                            config['cell_size'],
                            config['cell_size']), 0)

    def quit(self):
        self.center_msg("Exiting...")
        pygame.display.update()
        pygame.quit()
        sys.exit()

    def toggle_pause(self):
        self.paused = not self.paused

    def start_game(self):
        if self.gameover:

            self.gameover = False

    def run(self):
        key_actions = {
            'ESCAPE': self.quit,
            'p': self.toggle_pause,
            'SPACE': self.start_game
        }

        self.gameover = False
        self.paused = False

        pygame.time.set_timer(pygame.USEREVENT + 1, config['delay'])
        dont_burn_my_cpu = pygame.time.Clock()

        while 1:
            screen = screenGrab()
            if_count_down = check_locations(screen, countdown_location)
            if if_count_down:
                break

        while 1:
            screen = screenGrab()
            all_active = 0

            # if_not_black = not check_locations(screen, black_locations, 'black')
            # if if_not_black:
            #     all_active += if_not_black
            #     print("Not Black")

            if_count_down = check_locations(screen, countdown_location)
            if if_count_down and self.game_starts == False:
                all_active += if_count_down
                # print("Count Down")

            if_game_over = check_locations(screen, gameover_location)
            if_winner = check_locations(screen, winner_location) and check_locations(screen, black_winner_location, 'black')
            # print(if_game_over, if_winner)
            if if_game_over or if_winner:
                all_active += 1
                self.board = Logic.new_board()
                self.game_starts = False
                # print("INIT GAME")
                # print("INIT GAME")
                # print("INIT GAME")
                # print("INIT GAME")
                # print("INIT GAME")
            if all_active == 0:
                # print('running')
                if self.game_starts == False:
                    time.sleep(0.5)
                    self.init_game()
                    self.game_starts = True

                garbage_board = game_matrix(screen)[::-1]

                assert(len(erroneous) == 0)


                # print('active')
                # cut board here
                temp = self.board[0:len(self.board) - AIDownstack.garbage_number(self.board)] + garbage_board[
                                                                      len(self.board) - AIDownstack.garbage_number(garbage_board): len(
                                                                          self.board)]
                # count = 0
                # while len(temp) < Logic.config['rows']:
                #     count += 1
                #     if count == 5: break
                #     garbage_board = game_matrix(screen)[::-1]
                #     temp = self.board[0:len(self.board) - AIDownstack.garbage_number(self.board)] + garbage_board[
                #                                                                         len(
                #                                                                             self.board) - AIDownstack.garbage_number(
                #                                                                             garbage_board): len(
                #                                                                             self.board)]
                self.board = temp

                while len(self.board) > Logic.config['rows']: del self.board[0]


                if len(self.board) != 18:
                    check_gameover = debug_check_locations(screen, gameover_location)
                    check_win = debug_check_locations(screen, winner_location)

                    print(check_gameover)
                    print(check_win)

                    if check_gameover:
                        print('detected gameover')
                        continue

                    if check_win:
                        print('winner')
                        continue

                    # screen = screenShot()
                    print('doesnt detect any, fail')

                # if len(self.board) != 18:
                #     # screen = screenShot()
                #     if check_locations(screen, gameover_location):
                #         continue
                    # else:
                    #     # screen = screenShot()
                    #     for i in range(Logic.config['rows'] - len(self.board)):
                    #         self.board = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]] + self.board
                        # print(check_locations(screen, gameover_location))
                        # print(debug_check_locations(screen, gameover_location))
                        # print(debug_find_garbage(screen))
                        # print(debug_game_matrix(screen))
                        # print (self.board)
                        # print (game_matrix(screen)[::-1])
                        # print (garbage_board)

                if len(self.board) != 18:
                    print(self.board)
                    for i in range(Logic.config['rows'] - len(self.board)):
                        self.board = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]] + self.board

                assert (len(self.board) == 18)

                # for i in range(10):
                #     screen = screenGrab()
                #     self.stone2_index = detect_piece(screen, next_piece_location , next_piece_colours, ghost = False)
                #     if self.stone2_index != -1: break

                # screen = screenGrab()
                self.stone2_index = detect_piece(screen, next_piece_location , next_piece_colours, ghost = False)

                if self.stone2_index == -1:
                    screen = screenShot()
                    print(debug_detect_piece(screen, next_piece_location, next_piece_colours, ghost = False))

                assert(self.stone2_index != -1)

                best_move = None

                # well_prop = AIComboWell.h_calc_combowell(self.board)
                # pile_height = AIDownstack.pile_height(self.board)
                # combo = detect_combo(screen, combo_locations)

                # print(combo_time)


                columns = AIComboWell3.getAllColumns(self.board)
                # self.past_index, self.stone2_index = Logic.c2_randomizer(self.past_index, self.sequence)
                bot_switch_eval = AIComboWell3.bot_switch_heuristics(columns, self.board)
                # final_switch_eval = bot_switch_eval + [combo_dropped_pieces] + [combo_timer]
                combo_timer = detect_time(screen, time_location)


                mode_switch = False

                # if (bot_switch_eval[3] > 4 and bot_switch_eval[4] > 1) or AIDownstack2.garbage_number(self.board) >= 4 or bot_switch_eval[0] >= 15 :
                # print(self.started_combo, AIDownstack2.garbage_number(self.board), bot_switch_eval[4])
                # if self.started_combo == False and (AIDownstack2.garbage_number(self.board) >= 4 or bot_switch_eval[4] > 1):
                if self.bot_mode == 'downstack':
                    downstack_moves = AIDownstack2.f_evaluate(self.board, (self.stone_index, self.stone2_index), self.gooddownstack_heuristic,
                                                              self.counter)
                    best_move = max(downstack_moves)
                    # self.started_combo = True
                    self.start_combo_height = self.combo_start_threshold
                    print('downstack')
                    if (AIDownstack2.garbage_number(self.board) == 0 and bot_switch_eval[4] == 0):
                        self.bot_mode = 'combo'
                    # self.bot_mode = 'downstack'

                else:
                    mode_switch = True
                    if self.bot_mode == 'combo':

                        if self.started_combo == False:
                            combo_moves = AIComboWell3.f_evaluate_combowell(self.board, (self.stone_index, self.stone2_index),
                                                                             self.upstack_heuristic,
                                                                             self.counter)
                            best_move = max(combo_moves)
                            # print(well_prop[0],  best_move[0])
                            self.switch_flag = False
                            self.started_combo = False

                            # if well_prop[0] >= self.combo_start_threshold or AIDownstack.garbage_number(self.board) > self.combo_garbage_threshold or pile_height >= self.piece_height_threshold:
                            if bot_switch_eval[3] >= 13 or bot_switch_eval[1] >= 17 or AIDownstack2.garbage_number(self.board) >= 4 or bot_switch_eval[4] > 1:
                                self.bot_mode = 'combodownstack'
                                self.start_combo_height = self.combo_start_threshold
                        else:
                            self.bot_mode = 'combodownstack'

                    if self.bot_mode == 'combodownstack':
                        # all_moves = AIDownstack2.f_evaluate(all_moves, self.test_heuristic, self.counter)
                        # downstack_moves = AIDownstack2.f_evaluate(self.board, (self.stone_index, self.stone2_index), self.downstack_heuristic,
                        #                                           self.counter)

                        downstack_moves = AIDownstackCombo.f_evaluate(self.board, (self.stone_index, self.stone2_index),
                                                                      self.downstack_heuristic,
                                                                      self.counter, combo_timer)
                        best_move = max(downstack_moves)

                        if self.switch_flag == False:
                            self.switch_flag = True
                            self.time_left = 0
                            self.counter = 0

                        if combo_timer != 0: self.started_combo = True
                        # print(best_move)
                        # if AIDownstack2.garbage_number(self.board) <= self.combo_garbage_threshold and well_prop[0] == 0:
                        #     self.bot_mode = 'combo'
                        if self.start_combo_height > 1 and combo_timer == 0 and self.started_combo == True:

                            self.started_combo = False
                            self.start_combo_height = 0

                            if (AIDownstack2.garbage_number(self.board) >= 1 or bot_switch_eval[4] > 1):
                                self.bot_mode = 'downstack'
                            else:
                                self.bot_mode = 'combo'

                if mode_switch:
                    print(self.bot_mode)
                # self.bot_mode = 'downstack'
                # if self.bot_mode == 'downstack':
                #     # all_moves = AIDownstack2.f_evaluate(all_moves, self.test_heuristic, self.counter)
                #     all_moves = AIDownstack2.f_evaluate(self.board, (self.stone_index, self.stone2_index),
                #                                    self.test_heuristic, self.counter)
                #     best_move = max(all_moves)
                #     # print(best_move)
                #     if AIDownstack.garbage_number(self.board) <= self.combo_garbage_threshold and well_prop[0] == 0:
                #         self.bot_mode = 'combo'
                #     if self.start_combo_height > 1 and combo_time == 0:
                #         self.bot_mode = 'combo'
                #         self.start_combo_height = 0




                assert(best_move != None)
                offset = 3
                # move here
                if best_move[3] == 3:
                    press(self.rotate_r)
                    offset += 1
                elif best_move[3] == 2:
                    press(self.rotate_180)
                elif best_move[3] == 1:
                    press(self.rotate_l)
                    if self.stone_index == 5:
                        offset += 1


                if self.stone_index == 6:
                    offset = 4

                moves = best_move[2] - offset
                if moves > 0:
                    for i in range(abs(moves)): press(self.right)
                elif moves < 0:
                    for i in range(abs(moves)): press(self.left)

                press(self.hard_drop)

                for i in range(best_move[3]):
                    self.stone = Logic.rotate_clockwise(self.stone)
                self.board, line_clear = Logic.drop_piece(self.board, self.stone, best_move[2], self.stone_y)

                self.score, self.time_left, self.counter = AIDownstack.f_score(line_clear,
                                                                   (self.score, self.time_left, self.counter))

                self.stone_index = self.stone2_index
                self.stone = Logic.tetris_shapes[self.stone_index]

            self.screen.fill((0, 0, 0))

            if self.gameover:
                pass
            else:
                if self.paused:
                    self.center_msg("Paused")
                else:
                    self.draw_matrix(self.board, (0, 0))
                    # print_matrix(self.board)
                    # print()
                    self.draw_matrix(self.stone,
                                     (self.stone_x,
                                      self.stone_y))

            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                elif event.type == pygame.KEYDOWN:
                    for key in key_actions:
                        if event.key == eval("pygame.K_"
                                             + key):
                            key_actions[key]()

            dont_burn_my_cpu.tick(config['maxfps'])


if __name__ == '__main__':
    App = TetrisApp()
    App.run()
