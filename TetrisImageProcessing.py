from PIL import ImageGrab
import os
import time
import win32api, win32con
from PIL import ImageOps
from numpy import *
from array import *
import GUI
import Logic
from KeyboardEvent import *

# Globals
# ------------------
 
matrix_bottom_x = 99
matrix_bottom_y = 996
square_size = 44
mid_offset = square_size/2 
matrix_top_y = 208

matrix_bottom_right_x = 537

gameover_location = [(320, 540), (230, 535), (260, 670)] #(400, 670)]
winner_location = [(163, 600), (317, 600), (463, 625)]
black_winner_location = [(300, 610), (480, 610)]
countdown_location = [(328, 604)]
black_locations = [(116,222), (516, 219)] # check if it is a playfield matrix

first_piece_location = (295, 975)
next_piece_location = (670, 295)

ghost_piece_colours = [(42, 6, 47, 't'), (1, 45, 20, 's'), (42, 6, 8, 'z'), (9, 12, 47, 'j'), (38, 22, 8, 'l'), (3, 45, 47, 'i'),
                       (40, 45, 13, 'o')]

next_piece_colours = [(188, 41, 202, 't'), (13, 194, 27, 's'), (183, 38, 43, 'z'), (111, 119, 255, 'j'),
                      (255, 193, 64, 'l'), (29, 194, 199, 'i'),
                      (187, 201, 80, 'o')]

combo_locations = [(650, 480), (645, 510), (650, 545), (685, 480), (690, 510), (685, 545)]

time_location = [(668, 440)]


rising_garbage_pixel = 3

matrix = [[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0]]
# 100 + 5 + 44 * 10 , 996 - 5 - 44 * 18 each
#209 TOP
# BR 122


#Black = (12, 15, 17)
#Grey = (92, 95, 97)
#Orange = (78, 81, 242)

def screenGrab():
    box = ()
    im = ImageGrab.grab()
    # im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')

    return im

def screenShot():
    box = ()
    im = ImageGrab.grab()
    im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')

    return im

def main():
    screenGrab()



     
def get_cords():
    x,y = win32api.GetCursorPos()

    print (x,y)

def debug_find_garbage(screen):
    # ! major problems with orange, fix the colour detection!
    # screen = screenGrab()
    tolerance3 = 10


    grey_block_colours = (90, 90, 90)

    # handle situation when there are multiple garbage in col 1
    # handle situation when garbage hole in col 1 row 1/2/3
    garbage_range = 0
    for x in range(Logic.config['rows'] * 2 - 1):

        current_pos = matrix_top_y + mid_offset + (mid_offset*x)
        # print(current_pos)
        value = screen.getpixel((matrix_bottom_x + mid_offset, current_pos))
        side_val = screen.getpixel((matrix_bottom_x, current_pos))
        print(matrix_bottom_x + mid_offset, current_pos, value)
        if abs(value[0] - grey_block_colours[0]) < tolerance3 and abs(value[1] - grey_block_colours[1]) < tolerance3 and abs(
                value[2] - grey_block_colours[2]) < tolerance3 and side_val[0] > 200 and side_val[1] > 200:
            garbage_range = current_pos
            break

    print('Range' ,garbage_range)

    if garbage_range == 0:
        garbage_range = matrix_bottom_y

    highest_val = 0
    highest_pixel = 0
    for y in range(square_size):
        value = screen.getpixel((matrix_bottom_x + mid_offset, garbage_range - y))
        add_val = value[0] + value[1] + value[2]
        if add_val > highest_val:
            highest_val = add_val
            highest_pixel = garbage_range - y
    print('Highest Pixel' ,highest_pixel)

    highest_nextcol_pixel = highest_pixel
    for z in range(3):
        print(highest_pixel, mid_offset, square_size, z)
        # print((square_size * z))
        print('Side garbage check:' , matrix_bottom_x + square_size + mid_offset, highest_pixel - mid_offset - (square_size * z))
        check_above = screen.getpixel((matrix_bottom_x + square_size + mid_offset, highest_pixel - mid_offset - square_size * z))
        if abs(check_above[0] - grey_block_colours[0]) < tolerance3 and abs(check_above[1] - grey_block_colours[1]) < tolerance3 and abs(
                check_above[2] - grey_block_colours[2]) < tolerance3:
            highest_nextcol_pixel = highest_pixel - square_size * (z + 1)
        else:
            break

    return highest_nextcol_pixel if highest_pixel != highest_nextcol_pixel else highest_pixel


def find_garbage(screen):
    # ! major problems with orange, fix the colour detection!
    # screen = screenGrab()
    tolerance3 = 10

    grey_block_colours = (90, 90, 90)

    # handle situation when there are multiple garbage in col 1
    # handle situation when garbage hole in col 1 row 1/2/3
    garbage_range = 0
    for x in range(Logic.config['rows'] *2 - 1):

        current_pos = matrix_top_y + mid_offset + (mid_offset*x)
        # print(current_pos)
        value = screen.getpixel((matrix_bottom_x + mid_offset, current_pos))
        side_val = screen.getpixel((matrix_bottom_x, current_pos))
        # print(matrix_bottom_x + mid_offset, current_pos)
        if abs(value[0] - grey_block_colours[0]) < tolerance3 and abs(value[1] - grey_block_colours[1]) < tolerance3 and abs(
                value[2] - grey_block_colours[2]) < tolerance3 and side_val[0] > 200 and side_val[1] > 200:
            garbage_range = current_pos
            break

    # print('Range' ,garbage_range)

    if garbage_range == 0:
        garbage_range = matrix_bottom_y

    highest_val = 0
    highest_pixel = 0
    for y in range(square_size):
        value = screen.getpixel((matrix_bottom_x + mid_offset, garbage_range - y))


        add_val = value[0] + value[1] + value[2]
        if add_val > highest_val:
            highest_val = add_val
            highest_pixel = garbage_range - y

    highest_nextcol_pixel = highest_pixel
    for z in range(3):
        # print(highest_pixel, mid_offset, square_size, z)
        # print((square_size * z))
        # print('Side garbage check:' , matrix_bottom_x + square_size + mid_offset, highest_pixel - mid_offset - (square_size * z))
        check_above = screen.getpixel((matrix_bottom_x + square_size + mid_offset, highest_pixel - mid_offset - square_size * z))
        if abs(check_above[0] - grey_block_colours[0]) < tolerance3 and abs(check_above[1] - grey_block_colours[1]) < tolerance3 and abs(
                check_above[2] - grey_block_colours[2]) < tolerance3:
            highest_nextcol_pixel = highest_pixel - square_size * (z + 1)
        else:
            break

    return highest_nextcol_pixel if highest_pixel != highest_nextcol_pixel else highest_pixel


erroneous = []

def debug_game_matrix(screen):
    matrix = Logic.new_board()
    screen = screenGrab()

    if len(erroneous) > 1:
        print(erroneous)
        # print_matrix()
        # screen.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')

    garbage_pixel_temp = find_garbage(screen)
    garbage_height_pixel = matrix_bottom_y - garbage_pixel_temp
    bottom_offset = int(round(garbage_height_pixel % square_size))
    garbage_height_square = int(round((garbage_height_pixel - bottom_offset) / square_size))
    print('Garbage stats: ', garbage_height_square, garbage_pixel_temp, bottom_offset)
    for x in range(garbage_height_square):
        for y in range(10):
            #get pixels of the middle of each squares
            value = screen.getpixel((matrix_bottom_x + mid_offset + (square_size * y), matrix_bottom_y - bottom_offset - mid_offset - (square_size * x)))
            print (value, end=' ')
            if 0 <= value[0] <= 25:
                matrix[x][y] = 0
                # matrix[x][y] = value[0]
            elif 70 <= value[0] <= 110:
                matrix[x][y] = 8
                # matrix[x][y] = value[0]
            else:
                matrix[x][y] = 5
                erroneous.append([[x,y,value[0]],matrix_bottom_x + mid_offset + (square_size * y), matrix_bottom_y - bottom_offset - mid_offset - (square_size * x)])
                screen.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')
                print(garbage_height_square, matrix_bottom_y - garbage_height_pixel, bottom_offset, value)
                print(debug_find_garbage(screen))
                raise Exception('failed attempt detected')
                #matrix[x][y] = value[0]
        print()

    print('Bottom offset  ', bottom_offset)
    rising_line = [8,8,8,8,8,8,8,8,8,8]
    if bottom_offset > rising_garbage_pixel:
        for y in range(10):
            value = screen.getpixel((matrix_bottom_x + mid_offset + (square_size * y),
                                     matrix_bottom_y - bottom_offset + rising_garbage_pixel))
            print(matrix_bottom_x + mid_offset + (square_size * y), matrix_bottom_y - bottom_offset + rising_garbage_pixel, value)
            if value[0] < 80:
                rising_line[y] = 0

    if rising_line.count(0) == 1:
        return [rising_line] + matrix[:-1]
    else:
        return matrix

def game_matrix(screen):
    matrix = Logic.new_board()
    # screen = screenGrab()

    if len(erroneous) > 1:
        print(erroneous)
        # print_matrix()
        # screen.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')

    garbage_height_pixel = matrix_bottom_y - find_garbage(screen) 
    bottom_offset = int(round(garbage_height_pixel % square_size))
    garbage_height_square = int(round((garbage_height_pixel - bottom_offset) / square_size))
    # print(garbage_height_square, matrix_bottom_y - garbage_height_pixel, bottom_offset)
    for x in range(garbage_height_square):
        for y in range(10):
            #get pixels of the middle of each squares
            value = screen.getpixel((matrix_bottom_x + mid_offset + (square_size * y), matrix_bottom_y - bottom_offset - mid_offset - (square_size * x)))
            if 0 <= value[0] <= 25:
                matrix[x][y] = 0
                # matrix[x][y] = value[0]
            elif 70 <= value[0] <= 110:
                matrix[x][y] = 8
                # matrix[x][y] = value[0]
            else:
                matrix[x][y] = 5
                erroneous.append([[x,y,value[0]],matrix_bottom_x + mid_offset + (square_size * y), matrix_bottom_y - bottom_offset - mid_offset - (square_size * x)])
                screen.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')
                print(garbage_height_square, matrix_bottom_y - garbage_height_pixel, bottom_offset, value)
                print(debug_find_garbage(screen))
                raise Exception('failed attempt detected')
                #matrix[x][y] = value[0]

    rising_line = [8,8,8,8,8,8,8,8,8,8]
    if bottom_offset > rising_garbage_pixel:
        for y in range(10):
            value = screen.getpixel((matrix_bottom_x + mid_offset + (square_size * y),
                                     matrix_bottom_y - bottom_offset + rising_garbage_pixel))
            if value[0] < 80:
                rising_line[y] = 0

    if rising_line.count(0) == 1:
        return [rising_line] + matrix[:-1]
    else:
        return matrix



def get_rising_garbage(screen):
    garbage_height_pixel = matrix_bottom_y - find_garbage(screen)
    bottom_offset = int(round(garbage_height_pixel % square_size))
    garbage_height_square = int(round((garbage_height_pixel - bottom_offset) / square_size))

    rising_line = [8,8,8,8,8,8,8,8,8,8]
    if bottom_offset > rising_garbage_pixel:
        for y in range(10):
            value = screen.getpixel((matrix_bottom_x + mid_offset + (square_size * y),
                                     matrix_bottom_y - bottom_offset + rising_garbage_pixel))
            if value[0] < 80:
                rising_line[y] = 0

    if rising_line.count(0) == 1:
        return rising_line, garbage_height_square
    else:
        return False


def check_locations(screen, location, colour = 'white'):
    total_bool = 0
    for item in location:
        value = screen.getpixel(item)
        if colour == 'white':
            if value[0] == 255 and value[1] == 255 and value[2] == 255:
                total_bool += 1
        elif colour == 'black':
            if value[0] <= 200 and value[1] <= 200:
                total_bool += 1
    if total_bool == len(location):
        return True
    else:
        return False

def debug_check_locations(screen, location, colour = 'white'):
    total_bool = 0
    for item in location:
        value = screen.getpixel(item)
        print(value, item)
        if colour == 'white':
            if value[0] == 255 and value[1] == 255 and value[2] == 255:
                total_bool += 1
        elif colour == 'black':
            if value[0] <= 200 and value[1] <= 200 :
                total_bool += 1
    if total_bool == len(location):
        return True
    else:
        return False

def detect_piece(screen, piece_location, piece_colours, ghost = True):
    tolerance = 4
    if ghost == False:
         tolerance = 35
    value = screen.getpixel(piece_location)
    # print(value)
    for i in range(len(piece_colours)):
        if abs(value[0] - piece_colours[i][0]) < tolerance and abs(value[1] - piece_colours[i][1]) < tolerance and abs(value[2] - piece_colours[i][2]) < tolerance :
            return i
    else:
        print(value)
        return -1


def debug_detect_piece(screen, piece_location, piece_colours, ghost = True):
    tolerance = 4
    if ghost == False:
        tolerance = 35
    value = screen.getpixel(piece_location)

    for i in range(len(piece_colours)):
        print(value, piece_colours[i])
        if abs(value[0] - piece_colours[i][0]) < tolerance and abs(value[1] - piece_colours[i][1]) < tolerance and abs(value[2] - piece_colours[i][2]) < tolerance :
            return i
    else:
        return -1

def detect_combo(screen, locations):
    # for 0 combo, its first 6 locations
    total = []
    for pixel in locations:
        value = screen.getpixel(pixel)
        if value[0] == 255 and value[1] == 255 and value[2] == 255:
            total.append(1)
        else:
            total.append(0)

    if total[:6].count(1) == 6:
        return 0
    else:
        return -1

def detect_time(screen, locations):
    # for 0 combo, its first 6 locations
    total = []
    for pixel in locations:
        value = screen.getpixel(pixel)
        if value[0] > 100 :
            total.append(1)
        else:
            total.append(0)

    if total.count(1) == len(locations):
        return 1
    else:
        return 0


def game_loop(matrix):
    #WORKING



    last_board = [row[:] for row in matrix]
    number_of_garbage = -1
    while True:



        screen = screenGrab()
        all_active = 0
        for pixels in black_locations:
            all_active += not check_locations(screen, pixels, 'black')
        all_active += check_locations(screen, countdown_location)
        if check_locations(screen, gameover_location):
            all_active += check_locations(screen, gameover_location)
            matrix = Logic.new_board()
        if all_active == 0:
            # rising_garbage = get_rising_garbage(screen)
            # if rising_garbage != False:
            #     if rising_garbage[1] != number_of_garbage:
            #         print(rising_garbage)
            #         number_of_garbage = rising_garbage[1]
            if detect_piece(screen, next_piece_location, next_piece_colours, False) == -1:
                print('Fail')
            # matrix = game_matrix(screen)
            # if last_board != matrix:
            #     # print(matrix[0])
            #     print_matrix(matrix)
            #     print()
            #     last_board = [row[:] for row in matrix]


def print_matrix(matrix):
    for r in reversed(matrix):
        for c in r:
            print(c, end=' ')
            #print("{:0>3d}".format(c),end=' ')
        print()




# screen = screenGrab()
# print(debug_find_garbage(screen))

# game_matrix()
# print_matrix()
# get_garbage_loop()
# screenGrab()

# board = Logic.new_board()
# game_loop(board)

# for i in range(5):
#     press('left_arrow')


# print(detect_piece(screen, first_piece_location , ghost_piece_colours))
# screenShot()

if len(erroneous) > 1: print(erroneous)
if __name__ == '__main__':
    pass
