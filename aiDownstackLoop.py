from AIDownstack import *

board_list = [Logic.garbage_testboard(), Logic.garbage_testboard2()]
def AILoopDownstack(heuristic):

    this_pieces = 60 # enough pieces not to top out
    stone_y = 1 # dont change this
    games = 3
    combo_score = 0
    downstack_score = 0
    total_cells_left = 0
    # board_list = [Logic.garbage_testboard(), Logic.garbage_testboard2()]
    board_list = [Logic.garbage_testboard()]
    count = 0
    total_pieces = 0

    for board in board_list:

        total_score = 0
        for i in range(games):
            history = []

            time_left = 0
            counter = 0

            history, stone_index = Logic.history_randomiser(history)
            history.append(stone_index)
            stone = Logic.tetris_shapes[stone_index]
            score = 0


            # if_temp_punish = 5 # Change this later haha, just to test out punish
            for i in range(this_pieces):
                history, stone2_index = Logic.history_randomiser(history)
                history.append(stone2_index)


                all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
                best_move = max(all_moves)

                for i in range(best_move[3]):
                    stone = Logic.rotate_clockwise(stone)
                board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)
                total_pieces += 1

                if garbage_number(board) == 0:
                    break


                score, time_left, counter = f_score(line_clear, (score, time_left, counter))
                ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
                stone_index = stone2_index
                stone = Logic.tetris_shapes[stone_index]

            # Logic.print_matrix(board)
            # print(score)
            # print(total_cells_left)
            total_score += score
            # total_cells_left += count_cells(board)
        # if count == 0:combo_score += total_score
        # elif count == 1: downstack_score += total_score
        count += 1

    # return total_cells_left ,combo_score, downstack_score,
    return total_pieces
    # return score

def AISurvivor(heuristic):
    this_pieces = 500
    stone_y = 1 # dont change this
    games = 1
    piece_chance = 4
    dropped_pieces = 0


    for i in range(games):
        history = []

        time_left = 0
        counter = 0

        history, stone_index = Logic.history_randomiser(history)
        history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0
        board = Logic.new_board()
        # dropped_pieces = 0


        for i in range(this_pieces):
            history, stone2_index = Logic.history_randomiser(history)
            history.append(stone2_index)


            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for i in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            if h_death(board) != 0:
                break

            dropped_pieces += 1

            # if rand(piece_chance) == 1: board = Logic.add_garbage(board)
            if dropped_pieces % piece_chance == 0:  board = Logic.add_garbage(board)# just add garbage randomly



            score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]


    return dropped_pieces


def AIScore(heuristic):
    this_pieces = 500
    stone_y = 1 # dont change this
    games = 1
    piece_chance = 4
    dropped_pieces = 0
    total_score = 0


    for i in range(games):
        history = []

        time_left = 0
        counter = 0

        history, stone_index = Logic.history_randomiser(history)
        history.append(stone_index)
        stone = Logic.tetris_shapes[stone_index]
        score = 0
        board = Logic.new_board()
        # dropped_pieces = 0


        for i in range(this_pieces):
            history, stone2_index = Logic.history_randomiser(history)
            history.append(stone2_index)


            all_moves = f_evaluate(board, (stone_index, stone2_index), heuristic, counter)  # returns score for all moves with heuristic
            best_move = max(all_moves)

            for i in range(best_move[3]):
                stone = Logic.rotate_clockwise(stone)
            board, line_clear = Logic.drop_piece(board, stone, best_move[2], stone_y)

            if h_death(board) != 0:
                break

            dropped_pieces += 1

            # if rand(piece_chance) == 1: board = Logic.add_garbage(board)
            if dropped_pieces % piece_chance == 0:  board = Logic.add_garbage(board)# just add garbage randomly



            score, time_left, counter = f_score(line_clear, (score, time_left, counter))
            ##        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
            stone_index = stone2_index
            stone = Logic.tetris_shapes[stone_index]
        total_score += score

    return total_score