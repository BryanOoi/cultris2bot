#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import operator
import random

import numpy

from aiComboWellLoop import *

from deap import base
from deap import benchmarks
from deap import creator
from deap import tools

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Particle", list, fitness=creator.FitnessMax, speed=list,
               smin=None, smax=None, best=None)


def generate(size, pmin, pmax, smin, smax):
    part = creator.Particle(random.uniform(pmin, pmax) for _ in range(size))



    # part = creator.Particle([1.446163560451649, 0.7514749272906509, -3.8478244887890662, 9.624509275652732, -7.737139880145377, -1.3893610829138243, -5.168312674519807, -9.10995067801436, 2.0 , 2.903187760204879, 1.4732073895726225, 0.4374696744787173])
    # part = creator.Particle([3.067811429463266, -0.9018390249226429, -4.396940710226232, 11.77232352719496, -7.800834771881488, -0.9787845069544217, -4.717829247562678, -5.767347625546624] + [random.uniform(pmin, pmax)] + [ 3, 2.1578967854133824, 3.8997066250951056, 3.392670266313298])
    # part = creator.Particle([0.34162159271729653, 0.9248135636412558, -3.710418779326525, 8.355802382105763, -7.738649559378584, -1.1522936547323006, -3.8388009194219266, -9.560642225804383] + [random.uniform(pmin, pmax)] +  [2.4169454119413123, 4.499820642617982, 3.5709863161201922, 0.4204277790892883])
    # part = creator.Particle([4.115293486583469, -0.34633896870302316, -1.099069620422449, 9.995468768902583, -7.637545449372233, -3.0394993047812653, -3.839333734812174, -7.5916858709344] + [random.uniform(pmin, pmax) for _ in range(3)]  +[ 2, 1, 1, 1])
    # part = creator.Particle([2.724592567052916, -0.3234327758896809, -1.0305346552139294, 11.434391616988897, -10.677127628806883, -3.4973314804077145, -3.156447818553091, -12.374627578557606, 0.4023201806500176, -9.791468901529132, -3.2661075195402995, 1.8492265146159963, 0.9060199214720575, 1.9504787599239632, 0.29742946909910906])
    # part = creator.Particle([5.314042759828826, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -2.499873783517404, -2.3766354417756883, -14.293184986213001] + [random.uniform(pmin, pmax)] + [-0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.6498431034825654, 0.35686816971454594, 1.7228141888299153, 0.8923282169408413])
    # part = creator.Particle([random.uniform(-13, -20)] + [-0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246] + [random.uniform(pmin, pmax) for _ in range(2)] + [ -2.3766354417756883, -14.293184986213001,  -6.362534366131436, -0.3651911208960841, -10.599321135722828, -4.968669868994532]  )
    # part = creator.Particle([-18.85919204466599, -0.8995220792151031, -1.8951976067137666, 6.65845592586899, -10.417415561336076, -3.4442756209680336, -0.8156959396853276, -1.0117203983008514, -13.582670436476, -4.8817584363771385, -0.48653204087903834, -9.308291253798021, -4.590348849184542, 1.0])
    # part = creator.Particle([-19.171946909336846, -0.2723049291196826, -1.217893305740042, 9.303221161039286, -11.004401586063246, -1.309697333520246, -0.8059699082340117, -2.3766354417756883, -14.293184986213001, -6.362534366131436, -0.3651911208960841, -10.599321135722828, -4.968669868994532, 1.0])


    part = creator.Particle([-3.3097613234732095, -0.44550117806097056, -0.6262938878912774, 14.175852503529713, 2.831215949886526, -1.2643179570456606, 6.157171756837603, 10.40051182431171, -4.529122974478024, -5.4713151161989035, -4.060992408402506, -4.709780649791912, -6.007332623466413, 1.0])
    #part = creator.Particle([-19.68457265584123, 0.6734513126546031, -2.7300078141778505, 11.451576092598817, 1.0664192690185064, -3.412186773159428, -4.037876341818529, -3.826748846040021, -15.418948312087307, -2.982235209586406, -0.35231224643887105, -0.5008262579859859, -3.3961051492031604, 1.0])


    part.speed = [random.uniform(smin, smax) for _ in range(size)]
    part.smin = smin
    part.smax = smax
    # part.best = 11.0667
    return part


def updateParticle(part, best, phi1, phi2):
    u1 = (random.uniform(0, phi1) for _ in range(len(part)))
    u2 = (random.uniform(0, phi2) for _ in range(len(part)))
    v_u1 = map(operator.mul, u1, map(operator.sub, part.best, part))
    v_u2 = map(operator.mul, u2, map(operator.sub, best, part))
    part.speed = list(map(operator.add, part.speed, map(operator.add, v_u1, v_u2)))
    for i, speed in enumerate(part.speed):
        if speed < part.smin:
            part.speed[i] = part.smin
        elif speed > part.smax:
            part.speed[i] = part.smax
    part[:] = list(map(operator.add, part, part.speed))

def evalOneMax(individual):
    return AILoopComboWell2(individual),

toolbox = base.Toolbox()
# toolbox.register("particle", generate, size=len(evaluation_function) + len(threshold_punish) + len(weights), pmin=-10, pmax=10, smin=-3, smax=3)
toolbox.register("particle", generate, size=len(evaluation_function) , pmin=-10, pmax=10, smin=-3, smax=3)
toolbox.register("population", tools.initRepeat, list, toolbox.particle)
toolbox.register("update", updateParticle, phi1=2.0, phi2=2.0)
toolbox.register("evaluate", evalOneMax)


def main():
    pop = toolbox.population(n=40)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    logbook = tools.Logbook()
    logbook.header = ["gen", "evals"] + stats.fields

    GEN = 1000
    best = None

    for g in range(GEN):
        for part in pop:
            part.fitness.values = toolbox.evaluate(part)
            if not part.best or part.best.fitness < part.fitness:
                part.best = creator.Particle(part)
                part.best.fitness.values = part.fitness.values
            if not best or best.fitness < part.fitness:
                best = creator.Particle(part)
                best.fitness.values = part.fitness.values
        for part in pop:
            toolbox.update(part, best)

        # Gather all the fitnesses in one list and print the stats
        logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
        print(logbook.stream)
        print(best)

    return pop, logbook, best


if __name__ == "__main__":
    main()