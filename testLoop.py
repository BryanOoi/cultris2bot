#from AIComboWell import *
from AIDownstack2 import *
import pygame, sys

config = {
    'cell_size': 25,
    'cols': 10,
    'rows': 18,
    'delay': 0,
    'maxfps': 2
}

colors = [
    (0, 0, 0),
    (245, 54, 249),
    (70, 249, 54),
    (249, 54, 54),
    (54, 99, 249),
    (249, 165, 54),
    (56, 231, 255),
    (249, 239, 42),
    (135, 130, 129)
]

tetris_shapes = [
    [[0, 1, 0],
     [1, 1, 1]],

    [[0, 2, 2],
     [2, 2, 0]],

    [[3, 3, 0],
     [0, 3, 3]],

    [[4, 0, 0],
     [4, 4, 4]],

    [[0, 0, 5],
     [5, 5, 5]],

    [[6, 6, 6, 6]],

    [[7, 7],
     [7, 7]]
]


##canvas,master = GUI.initialise_grid()
##score = 0
##time_left = 0
##counter = 0
##board = Logic.new_board()
##stone_y = 1
##
##
##stone_index = rand(len(Logic.tetris_shapes))
##stone = Logic.tetris_shapes[stone_index]
##
##for i in range(5):
##    stone2_index = rand(len(Logic.tetris_shapes))
##    ##stone = tetris_shapes[rand(len(tetris_shapes))]
##
##
##    all_moves = f_evaluate(board,(stone_index,stone2_index),test_heuristic)
##    best_move = max(all_moves)
##
##    for i in range(best_move[3]):
##        stone = Logic.rotate_clockwise(stone)
##    board,line_clear = Logic.drop_piece(board,stone,best_move[2],stone_y)
##    print(f_compute_score(test_heuristic, board, line_clear,'yes'))
##
##    #Logic.print_matrix(board)
##    draw(board,canvas)
##    score, time_left, counter = f_score(line_clear, (score, time_left, counter))
##    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
##    stone_index = stone2_index
##    stone = Logic.tetris_shapes[stone_index]
##

class TetrisApp(object):
    def __init__(self):
        pygame.init()
        pygame.key.set_repeat(250, 25)
        self.width = config['cell_size'] * config['cols']
        self.height = config['cell_size'] * config['rows']

        self.screen = pygame.display.set_mode((self.width + 150, self.height + 100))
        pygame.event.set_blocked(pygame.MOUSEMOTION)  # We do not need
        # mouse movement
        # events, so we
        # block them.
        self.init_game()

    def new_stone(self):
        self.stone_y = 1
        self.past_index, self.stone_index = Logic.c2_randomizer(self.past_index, self.sequence)
        # self.history.append(self.stone_index)
        self.stone = Logic.tetris_shapes[self.stone_index]


        ##        self.stone2_index = Logic.history_randomiser[self.history]
        ##        self.history.append(stone2_index)
        ##      self.stone = tetris_shapes[rand(len(tetris_shapes))]
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0]) / 2)

        if Logic.check_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
            self.gameover = True

    def init_game(self):
        self.board = Logic.new_board()
        self.sequence = Logic.c2_sequence()
        # self.board = Logic.garbage_testboard()
        self.past_index = 0
        self.new_stone()
        self.chance = 7
        self.max_pieces_allowed = 10000



        #Combowell
        # self.test_heuristic = [0.11962424268553473, -0.14433435149516605, -0.6462562655398616, -0.1871457461209567, -0.07709075265702214, -0.5120079082856313, -0.630768417220362, 0.2765981058202096, -0.09778777622972679, -0.6126352879562122, 0.5420380940295875, 0.6247756615106612, -0.585853564460656, 0.3882904943715848, 0.1971189465183909, -0.7499005947035491, 0.7788152772196213, 0.537224756389687, 0.8869159362373213, 0.11405984737793662, 0.7854698546414112, 0.8844917853773147, 0.9638300562979487, 0.483313947706965, 0.6513440780571946, 0.5025853800958919, -0.4696918007223019, -0.22775729632572062, 0.6308599858711976, 1.0]


        #downstack 2
        # self.test_heuristic = [-0.5645902800341911, 0.8510391099223649, -0.1497977136545623, -0.652848196642041, -0.8535520285641982, 0.6337430024677313, -0.23530108569996555, -0.9375173594837267, 0.062470270749240164, -0.19910338462818888, -0.03557449279207536, -0.34609162496780166, -0.051198979902487896, -0.0023986933874435845, -0.2014826948555517, -0.6516702635142229, -0.9124238528033981, 0.08942102224288973, -0.07702371697834343, -0.8778290649903118, 1.0]
        # self.test_heuristic = [0.3411615436090085, -0.9034782294744932, -0.07088655981882197, -0.10192731793005594, -0.36859192317949185, -0.43842634134070857, -0.6874594071859819, 0.27911456941638724, -0.65606595668162, 0.2492797602635859, -0.9422968342495401, -0.5172457849665626, -0.4908571455231283, -0.27254476472108946, 0.3013796407445959, -0.7807242769085381, -0.9753225501381122, 0.6556724354076526, -0.3449849640950817, -0.9919235417063346, 1.0]
        #self.test_heuristic = [0.4698623635444028, 0.10271919100453686, 0.9974889821070922, 0.9036238047176068, 0.844262800246937, 0.6372331726600122, -0.8763657651890453, 0.6197589892595565, -0.9650587937167137, -0.7867278341759056, 0.5354540711080475, -0.6437015099059167, 0.11058724008226672, 0.8768536287901374, -0.6754247332357859, -0.8920488061168468, -0.44834893587984914, -0.7551498908282022, -0.030732927692697753, 0.7530529766130956, 1.0]

        # self.test_heuristic = [-0.11917722579679091, -0.818989701746569, 0.6360443917239895, -0.761301187268123, 0.4698623635444028, 0.10271919100453686, 0.9974889821070922, 0.9036238047176068, 0.844262800246937, 0.6372331726600122, -0.8763657651890453, 0.6197589892595565, -0.12042857333080925, -0.32057308076268587, 0.14114292954236674, 0.06082655620347177, -0.09815761814710289, -0.9057460001844111, -0.8305469645028369, -0.8920488061168468, -0.44834893587984914, -0.7551498908282022, -0.030732927692697753, 0.0, 1.0]
        # self.test_heuristic = [-0.9278185905658975, 0.0020680302055675615, -0.21364229796203338, -0.03509129031558622, -0.08258374769419907, 1.0]
        # self.test_heuristic = [-0.9967178965926706, -0.08293054550278689, -0.10389261047520981, -0.09884098144662379, -0.04191519716293968, -0.14938616169206664, 1.0]
        #self.test_heuristic = [-0.9967178965926706, -0.06913007555990358, -0.16685837264736159, -0.727112465135525, -0.420842309933416, 0.033912084677517385, 1.0] #(318.0, 418.0)
        # self.test_heuristic = [-0.9124238528033981, 0.08942102224288973, -0.07702371697834343, -0.625023392916294, -0.2883064074820372, 0.033912084677517385, 1.0] #(-163.0,)
        # self.test_heuristic = [-0.6977539280263136, 0.14636116379769892, -0.551392635720378, -0.35942025029356595, -0.1807855123633435, -0.26911061351411325, -0.31587779200552646, 1.0] #(96.0,)
        # self.test_heuristic = [-0.4421792589153206, -0.09884098144662379, -0.04191519716293968, -0.48713505145308833, -0.423949679087003, -0.9535660325262743, -0.08057478209800784, 0.056155245356444894, 1.0] #(217.0,)
        # self.test_heuristic = [-17.407127499424874, -0.9927970806933981, -2.524543926070613, -7.250891007801808, -1.8318898029844315, -9.933961511467402, -4.088205935821115, -2.226158858218747, -7.31622118309639, -0.01938194128375148, 1.0] #(942 max~ pieces at 5 pieces 1 garbage, BEST consistent result)
        # self.test_heuristic = [-14.88755157472949, -0.2531687777292899, -1.9218831485782024, -5.887400773698782, -3.5983500674606566, -12.943185196856936, -5.813405914816728, -3.423241609460704, -4.969498008918508, -1.0037445003159644, 1.0] #(415 max~ pieces at 4 pieces 1 garbage)
        self.test_heuristic =  [-17.045569002213117, 0.058976177631637317, -2.3981153681321765, -10.516174182158453, -4.527542902312192, -7.136152217270677, -2.256001009593537, -4.872517339292467, -3.541791278179028, -0.27573349988485374, -0.5786098508596833, 1.0]
        #gen 39 Average 720 5/1
        self.dropped_pieces = 0
        self.score = 0
        self.time_left = 0
        self.counter = 0
        self.mode = f_compute_score # different AI behaviours based on heuristic
        self.mode_evaluate = f_evaluate # different AI behaviours based on heuristic
        # self.mode = f_compute_score_combowell
        # self.mode_evaluate = f_evaluate_combowell


        self.printed = False
        self.past_neg = False

    def center_msg(self, msg):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height // 2 - msgim_center_y + i * 22))

    def below_msg(self, msg, offset_below):
        for i, line in enumerate(msg.splitlines()):
            msg_image = pygame.font.Font(
                pygame.font.get_default_font(), 12).render(
                line, False, (255, 255, 255), (0, 0, 0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (
                self.width // 2 - msgim_center_x,
                self.height + offset_below - msgim_center_y + i * 22))

    def draw_matrix(self, matrix, offset):
        off_x, off_y = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                try:
                    colors[val]
                except:
                    val = 0

                if val:
                    pygame.draw.rect(
                        self.screen,
                        colors[val],
                        pygame.Rect(
                            (off_x + x) *
                            config['cell_size'],
                            (off_y + y) *
                            config['cell_size'],
                            config['cell_size'],
                            config['cell_size']), 0)

    def quit(self):
        self.center_msg("Exiting...")
        pygame.display.update()
        pygame.quit()
        sys.exit()

    def output_matrix(self):
        print(self.board)

    def toggle_pause(self):
        self.paused = not self.paused

    def start_game(self):
        if self.gameover:
            self.init_game()
            self.gameover = False

    def run(self):
        key_actions = {
            'ESCAPE': self.quit,
            'p': self.toggle_pause,
            'SPACE': self.start_game,
            'm' : self.output_matrix
        }

        self.gameover = False
        self.paused = False

        pygame.time.set_timer(pygame.USEREVENT + 1, config['delay'])
        dont_burn_my_cpu = pygame.time.Clock()
        while 1:
            if self.gameover != True:
                self.past_index, self.stone2_index = Logic.c2_randomizer(self.past_index, self.sequence)
                # self.history, self.stone2_index = Logic.history_randomiser(self.history)
                # self.history.append(self.stone2_index)

                # candidate_moves = Logic.all_moves_second(self.board, (self.stone_index, self.stone2_index))
                # for i in range(10):
                #     print(candidate_moves[i])

                # all_moves = f_evaluate(candidate_moves, self.test_heuristic, self.counter)

                all_moves = self.mode_evaluate(self.board, (self.stone_index, self.stone2_index), self.test_heuristic, self.counter)


                best_move = max(all_moves)


                #print out bad moves
                # if best_move[0] < -500 and best_move[0] > -900 and self.printed == False:
                # if best_move[0] > 300 and self.printed == False:
                #         print('Current board is ', self.board)
                #         print('Current piece is ' , self.stone_index)
                #         print('Next piece is ' , self.stone2_index)
                #         print('Best move is ' , best_move)
                #         self.printed = True



                for i in range(best_move[3]):
                    self.stone = Logic.rotate_clockwise(self.stone)
                self.board, line_clear = Logic.drop_piece(self.board, self.stone, best_move[2], self.stone_y)
                self.dropped_pieces += 1


                if rand(self.chance) == 1: # just add garbage randomly
                    self.board = Logic.add_garbage(self.board)

                self.score, self.time_left, self.counter = f_score(line_clear,
                                                                   (self.score, self.time_left, self.counter))

                self.stone_index = self.stone2_index
                self.stone = Logic.tetris_shapes[self.stone_index]

            self.screen.fill((0, 0, 0))

            if h_death(self.board) != 0 or self.dropped_pieces == self.max_pieces_allowed:
                # self.gameover = True
                # columns = getAllColumns(self.board)
                # print(h_eval(columns, self.board))
                self.init_game()
                self.printed = False


            if self.gameover:
                self.center_msg("""Game Over!
Press space to continue""")
                self.below_msg(
                    'Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(self.counter),
                    20)
                self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)
            else:
                if self.paused:
                    self.center_msg("Paused")
                else:
                    self.draw_matrix(self.board, (0, 0))
                    self.draw_matrix(self.stone,
                                     (self.stone_x,
                                      self.stone_y))
                    pygame.draw.line(self.screen, (255, 255, 255), (self.width, 0), (self.width,self.height),5)


                    next_index = Logic.peek_next_piece(self.past_index, self.sequence)
                    self.draw_matrix(Logic.tetris_shapes[next_index],
                                     (self.stone_x + 8,
                                      self.stone_y + 1))
                    self.below_msg('Score: ' + str(self.score) + ' Time: ' + str(self.time_left) + ' Counter: ' + str(
                        self.counter), 20)
                    self.below_msg('Position Eval: ' + str(self.mode(self.test_heuristic, self.board, line_clear, self.counter)), 40)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                elif event.type == pygame.KEYDOWN:
                    for key in key_actions:
                        if event.key == eval("pygame.K_"
                                             + key):
                            key_actions[key]()

            dont_burn_my_cpu.tick(config['maxfps'])


if __name__ == '__main__':
    App = TetrisApp()
    App.run()
