import Logic
from random import randrange as rand
from random import sample
import GUI
import numpy as np
#from sklearn import preprocessing
from operator import xor
from math import ceil

# PROBLEM WITH COMBOWELL AI
# 1) make overhangs above combo well
# 2) fancy spins at top of well at top of matrix

# 4) too inconsistent (loops of 3 tries)
# 5) cannot finish a stack having a readied I stick
# 6) create holes at bottom of stack
# 7) time cannot be negative




'''To add new evaluation function, add the name to the array below and add the calculation to the managing function'''


evaluation_function = ['line_clear', #managed by h_eval
                       'well_cells',
                       'deep_wells',
                       'column_holes',
                       'weighted_column_holes',
                       'column_hole_depths',
                       'min_column_hole_depth',
                       'max_column_hole_depth',
                       'total_column_transition',
                       'total_row_transitions',
                       'total_column_heights',
                       'pile_height',
                       'column_height_spread',
                       'total_solid_cells',
                       'total_weighted_solid_cells',
                       'column_height_variance',


                       'counter', #managed by h_calc_combowell
                       'total_comboable_lines',
                       'start_index_of_well',
                       'total_well_size',
                       'total_weighted_well_size',
                       'average_well_width',
                       'next_piece',
                       'number_of_wells',
                       'number_of_stack_holes'
                       ]



def getColumn(board, column):
    return [board[i][column] for i in range(len(board))]


def getAllColumns(board):
    return [getColumn(board, i) for i in range(len(board[0]))]


def if_row_transition(board, x, y):
    if y == 0 or y == Logic.config['cols'] - 1:
        return board[x][y] == 0
    else:
        return xor(board[x][y] != 0, board[x][y + 1] != 0)



def row_transition_indexes(row):
    # return index of all row transitions in row
    index_list = []
    new_row = [1] + row + [1]
    for y in range(len(new_row)-1):
        index_list.append(y - 1) if xor(new_row[y] != 0, new_row[y + 1] != 0) else None
    return index_list


def if_column_transition(board, x, y):
    # x = height
    assert (x != Logic.config['rows'] - 1)
    return xor(board[x][y] != 0, board[x + 1][y] != 0)  # Make sure cells in adjacent column is different


def calc_well_cells(board, x, y):
    total = 0
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x > -1:
        if y == 0:
            # print(board[x], board[x][y + 1])
            if board[x][y + 1] != 0:
                total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:
            if board[x][y - 1] != 0:
                total += 1
            else:
                break
        else:
            if board[x][y + 1] != 0 and board[x][y - 1] != 0:
                total += 1
            else:
                break
        x -= 1
    return total

#TODO: to punish very bad moves, put into f_evaluate()

def calc_punish_wells(all_well_cells,start_well,end_well ):
    punish = [0,0,0]

    if all_well_cells.count(2) > 1: punish[0] = 1 # punish too many size 2 wells

    for i in range(len(all_well_cells)):
        if i >= start_well and i <= end_well:  # check if 1-width well size in combo well
            if all_well_cells[i] > 6:
                punish[1] = 1

        else:
            if all_well_cells[i] >= 3:
                punish[2] = 1

    return punish

# print(calc_punish_wells([5, 0, 0, 2, 0, 3, 0, 0, 0, 0],-1,1))

# MAYBE PUT THIS IN EVALUATION FUNCTION?
def one_column_sizes(columns):
    tetris_column_sizes = []
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        column_size = 0
        for j in reversed(range(0, matrix_height)):

            if i == Logic.config['cols'] - 1:
                if columns[i][j] == 0 and columns[i - 1][j] != 0:
                    column_size += 1
            elif i == 0:
                if columns[i][j] == 0 and columns[i + 1][j] != 0:
                    column_size += 1
            else:
                if columns[i][j] == 0 and columns[i + 1][j] != 0 and columns[i - 1][j] != 0:
                    column_size += 1
        tetris_column_sizes.append(column_size)
    return tetris_column_sizes


#TODO: ADD total_uneven_well_row to evals
#TODO: Make fitness function for garbage only

def h_calc_combowell(board, punish_info = 'no'):
    # only calculate well depth after first non well row and stops at next non well row
    # well max is 4
    # combo-able lines in a row
    max_well_size = 4
    total_comboable_lines = 0 # combo-able lines
    start_index_of_well = -1
    average_well_width = -1
    total_weighted_well_size = 0 # maximising tetris start?
    total_well_size = 0
    start_well = -1
    end_well = Logic.config['cols'] - 1
    top_well_height = -1
    partial_well_score = 0.6
    well_threshold = 3
    no_of_partial_lines = 0
    ls_comboable_lines = []


    past_well_size = 0
    total_uneven_well_row = 0

    for i in range(Logic.config['rows']):

        if board[i].count(0) == Logic.config['cols']: continue # don't bother if empty row
        if board[i].count(8) == Logic.config['cols'] - 1: continue # don't bother if garbage line
        row_trans = row_transition_indexes(board[i])
        well_size = row_trans[1] - row_trans[0]
        assert(len(row_trans) >= 2)



        #Iterates rows starting from first time a well is detected
        if (len(row_trans) == 2 and well_size <= max_well_size) and (row_trans[0] >= start_well and row_trans[1] <= end_well ) : # this means well does exist and #check if current row well is inside past row well

            if (well_size <= past_well_size):
                total_comboable_lines += 1 # counting height starts here
            else:
                total_uneven_well_row += 1
                total_comboable_lines += partial_well_score
            total_well_size += well_size
            average_well_width = float(total_well_size / total_comboable_lines)
            total_weighted_well_size += total_comboable_lines * (max_well_size + 1 - well_size)

            past_well_size = well_size

            if top_well_height == -1: # only care about top of well
                top_well_height = i
                start_well = row_trans[0]
                end_well = row_trans[1]

            if start_index_of_well == -1 and total_comboable_lines > 1: # well size starts from two, TEST this
                start_index_of_well = start_well + 1
            # (start_well >= row_trans[0] and end_well <= row_trans[1])


        elif (len(row_trans) == 2 and well_size <= max_well_size) and i != Logic.config['rows'] - 1: # 'detected well, but outside of stack', start new stack cycle , this is to stop the matrix from having 2 different wells
            # print('well outside stack')
            # if total_comboable_lines > 1: # problem here
            ls_comboable_lines.append(total_comboable_lines) #stop counting comboable lines and start new cycle
            total_comboable_lines = 0
            start_well = -1  # index of well before well starts
            end_well = Logic.config['cols'] - 1  # index of well before well ends

        else:
            if all(row_trans[i] >= start_well and row_trans[i] <= end_well for i in range(len(row_trans))) and start_well != -1 and total_comboable_lines >= well_threshold: # check if all row_trans is inside the well
                # print('non well row but transitions inside well')
                # total_comboable_lines += partial_well_score - (
                #             partial_well_score * (no_of_partial_lines ** 2))  # punish partial lines exponentially
                no_of_partial_lines += 1
                if total_comboable_lines < 0: total_comboable_lines = 0
            elif total_comboable_lines > 0 and i != Logic.config['rows'] - 1 : # random garbage line doesnt fit in combo well and not a well itself
                # if total_comboable_lines > 1:
                ls_comboable_lines.append(total_comboable_lines)  # stop counting comboable lines and start new cycle
                total_comboable_lines = 0
                start_well = -1  # index of well before well starts
                end_well = Logic.config['cols'] - 1  # index of well before well ends

        # print(row_trans, start_well, end_well, total_comboable_lines)
    if total_comboable_lines > 1:
        ls_comboable_lines.append(total_comboable_lines)


    # print(total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width)
    if punish_info == 'yes':
        return [ls_comboable_lines, total_comboable_lines, top_well_height, start_well, end_well]
    else:
        return [total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width]

#no well > 2
#add to eval function, counter * line clear,  number of wells, stack holes
#  HOLE PROBLEM, TEST HOLE IN STACK
#  partial lines scaling
# change scoring system, less points for line clear!

def h_punish_top_mess(board, for_eval = 'no'):
    #punish any cells above wells and any holes above the well
#TODO: CHANGE CALCULATION FOR total_comboable_lines, partial point for hole in well instead of starting new well calculation

    well_scores = h_calc_combowell(board, 'yes')
    # print(well_scores)

    ls_comboable_lines = well_scores[0]
    total_comboable_lines = well_scores[1]
    top_well_height = well_scores[2]
    start_well = well_scores[3] # index of well before well starts
    end_well = well_scores[4] # index of well before well ends

    well_threshold = 3
    columns = getAllColumns(board)
    if_cells_above_well = 0
    if_too_many_combo_wells = 0
    stack_holes = 0

    # check for any cells above lowest well
    if top_well_height > 0 and total_comboable_lines >= well_threshold:
        for i in range(0,top_well_height):
            square_abv_well = board[i][start_well + 1:end_well+1]
            if square_abv_well.count(0) != len(square_abv_well): if_cells_above_well = 1 #make sure row is all 0s

    #check for hole above well

    all_well_cells = []
    column_holes = []
    # max_column_hole_depths = []

    matrix_height = Logic.config['rows']


    # j correspond to board height and i to board row
    # TODO: STOP HOLE CALC WHEN TOP WELL REACHED
    for i in range(Logic.config['cols']):
        # if board[i].count(8) == Logic.config['cols'] - 1: continue  # don't bother if garbage line
        # test = []
        # if i == 2:
        #     test = columns[i]
        highest_cell_index = matrix_height - 1
        column_hole = 0
        column_hole_depth = 0
        # max_column_hole_depth = 0
        for j in range(matrix_height):

            # a,b = board[j][i] , board[j + 1][i]

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                if highest_cell_index == matrix_height - 1:
                    well_cells = calc_well_cells(board, j - 1, i)  # for cell well count per column
                    all_well_cells.append(well_cells)
                    highest_cell_index = j  # only change this at 1st iteration

            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1: # and top_well_height > 0 : # calculate holes above well? TODO: HOLE PROBLEM

                # c,d,e = board[j + 1][i] == 0, board[j][i] != 0, j + 1 != matrix_height - 1
                if board[j + 1][i] == 0 and board[j][i] != 0 and j + 1 != matrix_height - 1:# and not(i > start_well and i <= end_well):  # Check hole, hole at j+1 if true, don't check hole at most bottom row in case of bad starts,
                    if (i > start_well and i <= end_well) and start_well != -1:
                        stack_holes += 1 # punish hole IN well differently
                    else:
                        if board[j].count(8) == Logic.config['cols'] - 1: continue
                        column_hole += 1
                        column_hole_depth += j + 1 - highest_cell_index

                    # if column_hole_depth > max_column_hole_depth : max_column_hole_depth = column_hole_depth
        else:
            column_holes.append(column_hole)
            # max_column_hole_depths.append(max_column_hole_depth)

    # print(stack_holes, column_holes)
    #calc_punish_well returns [x , y , z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well


    if_too_many_one_column_sizes = 1 if max(one_column_sizes(columns)) > 6 else 0 #dont have more than 6 tetris holes ANYWHERE in stack

    # print(ls_comboable_lines)

    if (len(ls_comboable_lines) > 1) : if_too_many_combo_wells = 1 #punish 2 wells and above

    if for_eval == 'yes':
        return [len(ls_comboable_lines), stack_holes]

    # print([if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + calc_punish_wells(all_well_cells, start_well, end_well))
    return [if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + calc_punish_wells(all_well_cells, start_well, end_well)





# board = Logic.garbage_testboard()
#
# well_prop = h_calc_combowell(board)
# side_well = 0
# if Logic.config['cols'] - ceil(well_prop[4]) == well_prop[1]:
#     side_well = 5
#
# print(well_prop)
# print(well_prop[0], side_well)




def h_eval(columns, board):
    total_height = 0
    bumpiness = 0
    heights_list = []
    all_well_cells = []
    column_transitions = []
    column_holes = []
    weighted_column_holes = []
    column_hole_depths = []
    min_column_hole_depths = []
    max_column_hole_depths = []
    total_solid_cells = 0
    total_weighted_solid_cells = 0
    total_row_transition = 0

    matrix_height = Logic.config['rows']

    # j correspond to board height and i to board row
    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height - 1
        column_trans = 0
        column_hole = 0
        weighted_column_hole = 0
        column_hole_depth = 0
        min_column_hole_depth = Logic.config['rows'] - 1
        max_column_hole_depth = 0
        for j in range(matrix_height):

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                total_solid_cells += 1
                total_weighted_solid_cells += matrix_height - j

                if highest_cell_index == matrix_height - 1:
                    well_cells = calc_well_cells(board, j - 1, i)  # for cell well count per column
                    all_well_cells.append(well_cells)
                    highest_cell_index = j  # only change this at 1st iteration
                    total_height += matrix_height - j  # for total height
                    heights_list.append(matrix_height - j)  # for column height variance

            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1:  # calculate column transition and holes
                if if_column_transition(board, j, i): column_trans += 1

                if board[j + 1][i] == 0 and board[j][i] != 0:  # Check hole, hole at j+1 if true
                    column_hole += 1
                    weighted_column_hole += j + 1 + 1
                    column_hole_depth += j + 1 - highest_cell_index

                    if column_hole_depth < min_column_hole_depth : min_column_hole_depth = column_hole_depth
                    if column_hole_depth > max_column_hole_depth : max_column_hole_depth = column_hole_depth
        else:
            if highest_cell_index == matrix_height - 1:  # colmun of empty cells
                heights_list.append(0)  # add 0 to heights if no block detected
            column_holes.append(column_hole)
            weighted_column_holes.append(weighted_column_hole)
            column_hole_depths.append(column_hole_depth)
            column_transitions.append(column_trans)
            min_column_hole_depths.append(min_column_hole_depth)
            max_column_hole_depths.append(max_column_hole_depth)

    pile_height = max(heights_list)
    column_height_spread = max(heights_list) - min(heights_list)
    for x in range(Logic.config['cols'] - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])

    for i in range(Logic.config['rows'] - pile_height, Logic.config['rows']):
        for j in range(Logic.config['cols']):
            if if_row_transition(board, i, j): total_row_transition += 1



    # print('Total Well Cells: ',           sum(all_well_cells))
    # print('Total Deep Wells: ',           sum(1 for i in all_well_cells if i >= 3))
    # print('Total Column Holes: ',         sum(column_holes))
    # print('Total Weighted Column Holes : ', sum(weighted_column_holes))
    # print('Total Column Hole Depths :',   sum(column_hole_depths))
    # print('Min Column Hole Depth :',      min(min_column_hole_depths))
    # print('Max Column Hole Depth :',      max(max_column_hole_depths))
    # print('Total Column Transitions :',   sum(column_transitions))
    # print('Total Row Transitions :',      total_row_transition)
    # print('Total Column Heights :',       total_height)
    # print('Pile Height :',                pile_height)
    # print('Column Height Spread :',       column_height_spread)
    # print('Total Solid Cells  :',         total_solid_cells)
    # print('Total Weighted Solid Cells :', total_weighted_solid_cells)
    # print('Column Height Variance :',     bumpiness)

    return [
            sum(all_well_cells),
            sum(1 for i in all_well_cells if i >= 3),
            sum(column_holes),
            sum(weighted_column_holes),
            sum(column_hole_depths),
            min(min_column_hole_depths),
            max(max_column_hole_depths),
            sum(column_transitions),
            total_row_transition,
            total_height,
            pile_height,
            column_height_spread,
            total_solid_cells,
            total_weighted_solid_cells,
            bumpiness
            ]



def takeFirst(elem):
    return elem[0]




def h_height(columns):
    total_height = 0
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        for j in range(matrix_height):
            if columns[i][j] != 0:
                total_height += matrix_height - j
                break

    return total_height


def h_bumpiness(columns):
    bumpiness = 0
    heights_list = []
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        for j in range(matrix_height):
            if columns[i][j] != 0:
                heights_list.append(matrix_height - j)
                break
        else:
            heights_list.append(0)

    for x in range(Logic.config['cols'] - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])

    return bumpiness


def h_holes(columns):
    number_holes = 0
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        for j in reversed(range(1, matrix_height)):
            if columns[i][j] == 0:
                if columns[i][j - 1] != 0:
                    number_holes += 1
    return number_holes


def h_lineclear(board):
    pass


# line clear data in y[1]
##    for x in all_moves_second(board,(5,6):
##        for y in x:
##            print_matrix(y[0]);
##            print()
##            print(y[1])
##            print()


def h_4columncontents(board, start_row):
    # start_row = 6
    total_blocks_in_columns = []
    for i in range(4):
        column = getColumn(board, start_row + i)
        total_blocks_in_columns.append(Logic.config['rows'] - column.count(0))

    return total_blocks_in_columns


def h_death(board):
    if board[0].count(0) != Logic.config['cols']:
        return -9999999
    else:
        return 0


def f_score(line_clear, args):
    score, time_left, counter = args

    if time_left <= 0:
        counter = 0
        time_left = 0

    counter_to_lineclear = [0, 0, 0, 0, 0, 1, 1, 8, 80, 500, 5000, 30000, 30000, 30000, 30000]
    base_time = [0, 6, 4, 2, -1, -4, -8, -8, -12, -12, -18, -18, -24, -24, -24]
    bonus_time = [0, 12, 6, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    loss_time = 4

    if counter > 14:
        counter = 14
    if line_clear == 0:
        if base_time[counter] < 0:
            time_left -= loss_time - base_time[counter]
        else:
            time_left -= loss_time
        return score, time_left, counter
    else:
        counter += 1
        time_left = time_left + base_time[counter] + (bonus_time[counter] * line_clear)
        score += counter_to_lineclear[counter]
        return score, time_left, counter


# def f_compute_score(heuristics, board, line_clear, output_eval='no'):
#     if len(heuristics) == 8:
#         heuristics.append(1.0)
#     columns = getAllColumns(board)
#     height = h_height(columns)
#     bumpiness = h_bumpiness(columns)
#     holes = h_holes(columns)
#     line_clear = line_clear
#     column_contents = h_4columncontents(board, 6)
#     death = h_death(board)
#     values = [height, bumpiness, holes, line_clear, column_contents[0], column_contents[1], column_contents[2],
#               column_contents[3], death]
#     if output_eval == 'yes':
#         print('Height: ' + str(height) + ' Bumpiness: ' + str(bumpiness) + ' Holes: ' + str(
#             holes) + ' Line clear: ' + str(line_clear) + ' 2nd last col: ' + str(
#             column_contents[2]) + ' Last col: ' + str(column_contents[3]))
#     return sum(x * y for x, y in zip(heuristics, values))

def f_compute_score(heuristics, board, line_clear, counter):
    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)
    columns = getAllColumns(board)
    values = [line_clear] + h_eval(columns, board) + [counter] + h_calc_combowell(board) + [h_death(board)]
    #print(heuristics)
    assert (len(heuristics) == len(values))

    return sum(x * y for x, y in zip(heuristics, values))


def f_compute_score_combowell(heuristics, board, line_clear, counter, next_piece):
    #weights
    #[if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_comboable_lines] + [stack_holes] + [x, y, z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well
    weights = [1 ,1, 1, 1, 0.95, 1, 1, 1]
    # punish here
    punish_ls = h_punish_top_mess(board)

    assert(len(weights) == len(punish_ls))
    # if punish_var[0] != 0 and punish_var[1] != 0: return 0  # only diff for now

    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)
    columns = getAllColumns(board)
    values = [line_clear] + h_eval(columns, board) + [counter] + [next_piece] + h_calc_combowell(board) + h_punish_top_mess(board,'yes') + [h_death(board)]

    assert (len(heuristics) == len(values))

    position_score = sum(x * y for x, y in zip(heuristics, values)) #evaluation for current board state

    punish_score = sum(x * y for x, y in zip(weights, punish_ls)) # punish heavily for bad board states, 0 if there isnt any in punish_ls

    return position_score - (punish_score * abs(position_score))


def f_compute_score_comboscore(heuristics, board, line_clear, counter, next_piece):
    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)

    columns = getAllColumns(board)
    values = [line_clear] + h_eval(columns, board) + [counter] + [next_piece] + h_calc_combowell(board) + [h_death(board)]
    assert (len(heuristics) == len(values))

    return sum(x * y for x, y in zip(heuristics, values))

def f_evaluate(board_old, shape_indexes, heuristics, counter):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)
    scores_and_move = []

    for boards in firstlayer_boards:
        if h_death(boards[0]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
            continue
        new_board = Logic.all_moves_first(boards[0], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score(heuristics, new[0], new[1] + boards[1], counter) # new[1] + boards[1] is line clear from old board + line clear from new board

            # punish here

            scores_and_move.append([score, boards[1], boards[2], boards[3]])

    return scores_and_move

def f_evaluate_combowell(board_old, shape_indexes, heuristics, counter):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)
    # firstlayer_scores = [(i, f_compute_score_combowell(heuristics, firstlayer_boards[i][0], firstlayer_boards[i][1], counter, next_shape_index)) for
    #                      i in range(len(firstlayer_boards))]
    # return firstlayer_scores
    scores_and_move = []

    for boards in firstlayer_boards:
        if h_death(boards[0]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
            continue
        new_board = Logic.all_moves_first(boards[0], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score_combowell(heuristics, new[0], new[1] + boards[1], counter, next_shape_index) # new[1] + boards[1] is line clear from old board + line clear from new board



            scores_and_move.append([score, boards[1], boards[2], boards[3]])

    return scores_and_move

def f_evaluate_comboscore(board_old, shape_indexes, heuristics, counter):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)
    scores_and_move = []

    for boards in firstlayer_boards:
        if h_death(boards[0]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
            continue
        new_board = Logic.all_moves_first(boards[0], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score_comboscore(heuristics, new[0], new[1] + boards[1], counter, next_shape_index) # new[1] + boards[1] is line clear from old board + line clear from new board



            scores_and_move.append([score, boards[1], boards[2], boards[3]])

    return scores_and_move

def test_score():
    line_seq = [1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 2, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    score = 0
    time_left = 0
    counter = 0
    for i in line_seq:
        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
        score, time_left, counter = f_score(i, (score, time_left, counter))
    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))


def draw(board, canvas):
    for r in range(Logic.config['rows']):
        for c in range(Logic.config['cols']):
            if board[r][c] != 0:
                GUI.colour_in(c, r, 'red', canvas)
            ##board = Logic.garbage_testboard()
##print(getColumn(board,1))
####columns = getAllColumns(board)

####print(f_compute_score(test_heuristic, board, 2))
####print(h_bumpiness(columns))


##total = f_evaluate(board,(5,6),test_heuristic)
##for i in total:
##    print (i)
##    print ()

##test_score()
