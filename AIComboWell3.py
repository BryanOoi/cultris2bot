import Logic
from operator import xor



           # correct_well_index


evaluation_function = [

    'sum(column_holes)',
    'parity',
    'bumpiness',
    # 'stack_holes',
    'total_comboable_lines',
    'cells_above_well',
    'cells_in_well',
    # 'weighted_cells_in_well',
    'well_holes',
    'sum(all_well_hole_cells)',
    'sum_wells_above_three',
    'long_holes',
    'sz_shapes',
    'l_or_j_shapes',
    'l_and_j_shapes'



    # 'correct_well_index'

]


#
# Check for 2/3 well instead of only 2 well
#
# Allow mid stacking halfway"
# Add shapes height!

# threshold_punish = ['punish_long_hole_threshold']

weights = [
    'punish_long_tetris_hole', 'well_holes', 'punish_clearing_lines', 'punish_bad_bumpiness']

#IMPROVEMENTS:
# maybe cells_above_well can reflect only the total_comboable_lines, and not the top of the stack

# evaluation_function = [
#     'sum(column_holes)',
#     'parity',
#     'bumpiness',
#     'stack_holes',
#     'pile_height'
# ]

def getColumn(board, column):
    return [board[i][column] for i in range(len(board))]


def getAllColumns(board):
    return [getColumn(board, i) for i in range(len(board[0]))]


def if_row_transition(board, x, y):
    if y == 0 or y == Logic.config['cols'] - 1:
        return board[x][y] == 0
    else:
        return xor(board[x][y] != 0, board[x][y + 1] != 0)



def row_transition_indexes(row):
    # return index of all row transitions in row
    index_list = []
    new_row = [1] + row + [1]
    for y in range(len(new_row)-1):
        index_list.append(y - 1) if xor(new_row[y] != 0, new_row[y + 1] != 0) else None
    return index_list

# print(row_transition_indexes([1,1,1,1,1,1,0,0,0,0]))

def if_column_transition(board, x, y):
    # x = height
    assert (x != Logic.config['rows'] - 1)
    return xor(board[x][y] != 0, board[x + 1][y] != 0)  # Make sure cells in adjacent column is different


def calc_well_cells(board, x, y):
    total = 0
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x > -1:

        if y == 0:
            # print(board[x], board[x][y + 1])
            if board[x][y + 1] != 0:
                total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:
            if board[x][y - 1] != 0:
                total += 1
            else:
                break
        else:
            # if y == 3:
            #     print(board[x][y + 1], board[x][y - 1])
            if board[x][y + 1] != 0 and board[x][y - 1] != 0:
                total += 1
            else:
                break
        x -= 1
    return total

def hole_well_cells(board, x, y):
    total = 0
    first_garbage = False
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x < Logic.config['rows']:
        if board[x][y] == 0:
            total += 1
            x += 1
        else:
            break
    return total

def bad_shapes(board, x, y):
    #maybe count long shapes?
    total = 0
    count = 0
    sz_shape = 0
    l_or_j_shape = 0
    l_and_j_shape = 0

    if board[x][y] != 0: # cannot be a well when not empty
        return [0, 0, 0]
    # count till height of 17
    while x > -1:
        if count == 3: break
        if y == 0:
            # print(board[x][y + 1])
            if count == 1:
                if board[x][y + 1] == 0:
                    sz_shape += 1
            if count == 2:
                if board[x][y + 1] == 0:
                    l_or_j_shape += 1
            # print(board[x], board[x][y + 1])
            if board[x][y + 1] != 0:
                total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:

            if count == 1:
                if board[x][y - 1] == 0:
                    sz_shape += 1
            if count == 2:
                if board[x][y - 1] == 0:
                    l_or_j_shape += 1
            if board[x][y - 1] != 0:
                total += 1
            else:
                break
        else:
            if count == 1:
                if xor(board[x][y - 1] == 0, board[x][y + 1] == 0):
                    sz_shape += 1
            if count == 2:
                if xor(board[x][y - 1] == 0, board[x][y + 1] == 0):
                    l_or_j_shape += 1
                if board[x][y - 1] == 0 and board[x][y + 1] == 0:
                    l_and_j_shape += 1
            if board[x][y + 1] != 0 and board[x][y - 1] != 0:
                total += 1
            else:
                break
        x -= 1
        count += 1
    return [sz_shape, l_or_j_shape, l_and_j_shape]


def h_calc_combowell(board, punish_info = 'no'):
    # only calculate well depth after first non well row and stops at next non well row
    # well max is 4
    # combo-able lines in a row
    max_well_size = 3
    total_comboable_lines = 0 # combo-able lines
    start_index_of_well = 6
    start_well_size = 0
    average_well_width = -1
    total_weighted_well_size = 0 # maximising tetris start?
    total_well_size = 0
    start_well = start_index_of_well
    end_well = Logic.config['cols'] - 1
    top_well_height = -1
    partial_well_score = 0.6
    well_threshold = 3
    no_of_partial_lines = 0
    total_allowed_stack_holes = 0
    ls_comboable_lines = []
    highest_garbage_hole_index = [Logic.config['rows'], 0]
    allowed_tetris_lines = 2
    allowed_two_well = 5



    past_well_size = max_well_size
    total_uneven_well_row = 0

    for i in range(Logic.config['rows']):

        if board[i].count(0) == Logic.config['cols']: continue # don't bother if empty row
        if board[i].count(8) == Logic.config['cols'] - 1:
            highest_garbage_hole_index = [i, board[i].index(0)]
            break # don't bother if garbage line
        row_trans = row_transition_indexes(board[i])
        well_size = row_trans[-1] - row_trans[-2]
        assert(len(row_trans) >= 2)


        #Iterates rows starting from first time a well is detected
        # if (len(row_trans) == 4 and well_size <= max_well_size) and (row_trans[-2] >= start_well and row_trans[-1] <= end_well): #hole in row, but is a well row otherwise
        #     total_comboable_lines -= 3**total_allowed_stack_holes
        #     print(3**total_allowed_stack_holes)
        #     total_allowed_stack_holes += 1
        #
        #
        #     if top_well_height == -1: # only care about top of well
        #         top_well_height = i


        if (len(row_trans) == 2 and well_size <= max_well_size) and (row_trans[0] >= start_well and row_trans[1] <= end_well ) : # this means well does exist and #check if current row well is inside past row well
            # print(start_well, end_well)
            if (well_size <= past_well_size):
                total_comboable_lines += 1 # counting height starts here
                # print(total_comboable_lines, i )
            else:
                total_uneven_well_row += 1
                # total_comboable_lines += partial_well_score
            total_well_size += well_size

            # average_well_width = float(total_well_size / total_comboable_lines)
            total_weighted_well_size += total_comboable_lines * (max_well_size + 1 - well_size)

            past_well_size = well_size

            if top_well_height == -1: # only care about top of well
                top_well_height = i

            # if start_well == -1:
            #     start_well = row_trans[0]
            #     end_well = row_trans[1]

            start_well = row_trans[0]
            end_well = row_trans[1]

            # if start_index_of_well == -2: # well size starts from two, TEST this
            #     start_index_of_well = start_well
            #     start_well_size = well_size
            # (start_well >= row_trans[0] and end_well <= row_trans[1])


        # elif (len(row_trans) == 2 and well_size <= max_well_size) and i != Logic.config['rows'] - 1: # 'detected well, but outside of stack', start new stack cycle , this is to stop the matrix from having 2 different wells
        #     # print('well outside stack')
        #     # if total_comboable_lines > 1: # problem here
        #     ls_comboable_lines.append(total_comboable_lines) #stop counting comboable lines and start new cycle
        #     total_comboable_lines = 0 # or it can be 1?
        #     start_well = row_trans[0]
        #     end_well = row_trans[1]
        #     past_well_size = max_well_size
            # start_well = -1  # index of well before well starts
            # end_well = Logic.config['cols'] - 1  # index of well before well ends

        # else:
        #
        #     if i > top_well_height and top_well_height != -1:
        #         print(4 ** total_allowed_stack_holes)
        #         total_comboable_lines -= 4**total_allowed_stack_holes
        #         total_allowed_stack_holes += 1
            # if all(row_trans[i] >= start_well and row_trans[i] <= end_well for i in range(len(row_trans))) and start_well != -1 and total_comboable_lines >= well_threshold: # check if all row_trans is inside the well
            #     # print('non well row but transitions inside well')
            #     # total_comboable_lines += partial_well_score - (
            #     #             partial_well_score * (no_of_partial_lines ** 2))  # punish partial lines exponentially
            #     no_of_partial_lines += 1
            #     if total_comboable_lines < 0: total_comboable_lines = 0
            # elif total_comboable_lines > 0 and i != Logic.config['rows'] - 1:
            #     total_comboable_lines = 0
                # elif total_comboable_lines > 0 and i != Logic.config['rows'] - 1 : # random garbage line doesnt fit in combo well and not a well itself
            #     # if total_comboable_lines > 1:
            #     ls_comboable_lines.append(total_comboable_lines)  # stop counting comboable lines and start new cycle
            #     total_comboable_lines = 0 # or it can be 1?
            #     start_well = row_trans[0]
            #     end_well = row_trans[1]
            #     past_well_size = max_well_size
            #     # start_well = -1  # index of well before well starts
            #     # end_well = Logic.config['cols'] - 1  # index of well before well ends

        # print(row_trans, start_well, end_well, total_comboable_lines)
        # print(total_comboable_lines, i)




    for i in range(allowed_tetris_lines):
        check_row = board[highest_garbage_hole_index[0] - (i + 1)]
        if check_row.count(0) == 1 and check_row.index(0) == Logic.config['cols'] - 1:
            total_comboable_lines += 1
    for j in range(allowed_two_well):

        check_row = board[highest_garbage_hole_index[0] - allowed_tetris_lines - (j + 1)]
        # print(check_row.index(0))
        if check_row.count(0) == 2 and check_row.index(0) == Logic.config['cols'] - 2: # check_row.index(0) makes sure no other hole in row to get the reward
            total_comboable_lines += 1

    if total_comboable_lines > 0:
        ls_comboable_lines.append(total_comboable_lines)
    # print(total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width)
    if punish_info == 'yes':
        return [ls_comboable_lines, total_comboable_lines, top_well_height, start_well, end_well]
    else:
        return [total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width, no_of_partial_lines,total_uneven_well_row, ls_comboable_lines,top_well_height,start_well, end_well,start_well_size]


def punish_holes_for_comboable_lines(all_long_holes):
    total = 0
    for i in range(sum(all_long_holes)):
        # total_comboable_lines -= i+1
        total -= 3**i
    return total

def h_eval(columns, board, line_clear):
    black_squares = 0
    white_squares = 0
    total_height = 0
    bumpiness = 0
    heights_list = []
    all_well_cells = []
    all_well_hole_cells = []
    column_transitions = []
    column_holes = []
    weighted_column_holes = []
    column_hole_depths = []
    min_column_hole_depths = []
    max_column_hole_depths = []
    combo_well_cells = []
    cells_in_well = 0
    total_solid_cells = 0
    total_weighted_solid_cells = 0
    total_row_transition = 0
    weighted_cells_in_well = 0
    total_bad_shapes = [0,0,0]
    all_long_holes = []

    highest_garbage_hole_index = [Logic.config['rows'], 0]

    matrix_height = Logic.config['rows']

    well_scores = h_calc_combowell(board)

    # returns [total_comboable_lines, start_index_of_well, total_well_size, total_weighted_well_size, average_well_width,
    #  no_of_partial_lines, total_uneven_well_row, ls_comboable_lines, top_well_height, start_well, end_well]
    total_comboable_lines = well_scores[0]
    start_index_of_well = well_scores[1]
    ls_comboable_lines = well_scores[7]
    top_well_height = well_scores[8]
    start_well = start_index_of_well  # index of well before well starts , different from start_well above
    end_well = Logic.config['cols'] - 1 # index of well before well ends , different from end_well above
    correct_well_index = 0
    # if start_well >= 6:
    #     correct_well_index = 1

    sixth_col_height = 0
    seventh_col_height = 0

    well_threshold = 3

    cells_above_well = 0
    well_holes = 0

    height_to_stop_clearing_lines = 5

    # # check for any cells above well
    # print(top_well_height)
    if top_well_height > 0 and total_comboable_lines >= well_threshold:
        for i in range(0,top_well_height):
            square_abv_well = board[i][start_well + 1:end_well+1]
            cells_above_well += len(square_abv_well) - square_abv_well.count(0) #count solid cells above well

    # print(cells_above_well)
    # j correspond to board height and i to board row
    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height
        column_trans = 0
        column_hole = 0
        weighted_column_hole = 0
        column_hole_depth = 0
        min_column_hole_depth = Logic.config['rows'] - 1
        max_column_hole_depth = 0
        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                if (i > start_well and i <= end_well):
                    if columns[i][j] != 8:
                        # print(highest_garbage_hole_index, j)
                        cells_in_well += 1
                        weighted_cells_in_well += highest_garbage_hole_index[0] - j

                if highest_cell_index == matrix_height:
                    well_cells = calc_well_cells(board, j - 1, i)  # for cell well count per column

                    detect_bad_shapes = bad_shapes(board, j - 1 , i)
                    if not (i > start_well and i <= end_well):

                        all_well_cells.append(well_cells)
                        # if detect_bad_shapes == 0:
                        #     print(detect_bad_shapes)
                        total_bad_shapes = [sum(x) for x in zip(total_bad_shapes, detect_bad_shapes)]
                        total_height += matrix_height - j  # for total height
                        heights_list.append(matrix_height - j)  # for column height variance
                        if i % 2 == 0:
                            if j % 2 == 0:
                                if columns[i][j] != 0: black_squares += 1
                            if j % 2 != 0:
                                if columns[i][j] != 0: white_squares += 1
                        if i % 2 != 0:
                            if j % 2 != 0:
                                if columns[i][j] != 0: black_squares += 1
                            if j % 2 == 0:
                                if columns[i][j] != 0: white_squares += 1
                    else:
                        combo_well_cells.append(well_cells)

                    if i == 6:
                        sixth_col_height = j
                    elif i == 7:
                        seventh_col_height = j
                    highest_cell_index = j  # only change this at 1st iteration



            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1:  # calculate column transition and holes
                # if if_column_transition(board, j, i): column_trans += 1

                if board[j + 1][i] == 0 and board[j][i] != 0 :#and j + 1 != matrix_height - 1:  # Check hole, hole at j+1 if true don't check hole at most bottom row in case of bad starts,

                    if (i > start_well and i <= end_well) and start_well != -2 and board[j + 1].count(8) != Logic.config['cols'] - 1:
                        well_holes += 1 # punish hole IN well differently
                        # print('yes')
                        well_cells = hole_well_cells(board, j + 1, i)
                        # print(j + 1, i)
                        all_well_hole_cells.append(well_cells)
                    else:
                        if board[j + 1].count(8) != Logic.config['cols'] - 1:
                            column_hole += 1
                            long_holes = hole_well_cells(board, j + 1, i)
                            # print(j + 1, i)
                            all_long_holes.append(long_holes)
                        # weighted_column_hole += j + 1 + 1
                        # column_hole_depth += j + 1 - highest_cell_index


                    # if column_hole_depth < min_column_hole_depth : min_column_hole_depth = column_hole_depth
                    # if column_hole_depth > max_column_hole_depth : max_column_hole_depth = column_hole_depth
        else:

            if not (i > start_well and i <= end_well) or start_well == -2: # and start_well != -1:
                if highest_cell_index == matrix_height:  # colmun of empty cells
                    if i % 2 == 0:
                        black_squares += 1
                    if i % 2 != 0:
                        white_squares += 1
                    heights_list.append(0)  # add 0 to heights if no block detected
                    inempty_well_cells = calc_well_cells(board, Logic.config['rows'] - 1, i)
                    all_well_cells.append(inempty_well_cells)
                column_holes.append(column_hole)

            else:
                if highest_cell_index == matrix_height:
                    inempty_well_cells = calc_well_cells(board, Logic.config['rows'] - 1, i)
                    combo_well_cells.append(inempty_well_cells)
                # weighted_column_holes.append(weighted_column_hole)
                # column_hole_depths.append(column_hole_depth)
                # column_transitions.append(column_trans)
                # min_column_hole_depths.append(min_column_hole_depth)
                # max_column_hole_depths.append(max_column_hole_depth)

    if seventh_col_height <= sixth_col_height:
        heights_list.append(seventh_col_height)

    # pile_height = max(heights_list)
    average_height = float(sum(heights_list)/len(heights_list))
    # print(combo_well_cells)
    # print(all_well_cells)
    # print(start_well, end_well)
    column_height_spread = max(heights_list) - min(heights_list)



    for x in range(len(heights_list) - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])

    parity = abs(black_squares - white_squares)
    # print(all_well_cells)
    sum_wells_above_three = sum([x for x in all_well_cells if x > 2])



    # sum_wells_above_two = sum([x for x in all_well_cells if x > 1])


    # print(total_bad_shapes)
    # print(all_well_hole_cells)
    # print(weighted_cells_in_well)
    # print(sum_wells_above_three)
    # print(heights_list)
    # print(start_well, end_well)
    # if(black_squares + white_squares != 10):
    #     print(black_squares , white_squares)
    #     Logic.print_matrix(board)
    # print(parity)
    # print(heights_list)
    # print(black_squares , white_squares)
    # print(start_well, end_well)
    # print(correct_well_index)

    assert (black_squares + white_squares + (end_well-start_well)) == 10

    punish_long_tetris_hole = sum(combo_well_cells)
    if punish_long_tetris_hole <= 4:
        punish_long_tetris_hole = 0

    punish_long_well_hole = sum(all_well_hole_cells)


    punish_long_stack_holes = [x for x in all_long_holes if x > 1]

    # print(all_long_holes)

    total_comboable_lines = total_comboable_lines + punish_holes_for_comboable_lines(all_long_holes)

    # for i in range(sum(all_long_holes)):
    #     # total_comboable_lines -= i+1
    #     total_comboable_lines -= 3**i
    #     # print(i+1)

    # print(punish_long_stack_holes)

    # print(sixth_col_height, seventh_col_height)

    punish_clearing_lines = line_clear if total_comboable_lines > 3 else 0
    # print(heights_list)
    # print(bumpiness)
    punish_bad_bumpiness = bumpiness - 10 if bumpiness >= 10 else 0





    # punish_hole_threshold = 3
    # if punish_long_well_hole <= punish_hole_threshold:
    #     punish_long_well_hole = 0

    # for i in range(Logic.config['rows'] - pile_height, Logic.config['rows']):
    #     row_trans_count = len(row_transition_indexes(board[i]))
    #     total_row_transition += row_trans_count
        # for j in range(Logic.config['cols']):
        #
        #     if if_row_transition(board, i, j): total_row_transition += 1

    # all_column_sizes = one_column_sizes(columns, highest_garbage_hole_index) # combo_well_cells now obsolete
    # punishes = h_punish_top_mess(ls_comboable_lines,cells_above_well,stack_holes,column_holes, all_column_sizes, all_well_cells, start_well, end_well)
    # punishes are [if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + [x,y,z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well


    # print('Start Well : ',                start_well)
    # print('End Well : ',                  end_well)
    # print('Total Heights : ',             heights_list)
    #
    #
    # print('Total Well Cells: ',           all_well_cells)
    # print('Total Deep Wells: ',           sum(1 for i in all_well_cells if i >= 3))
    # print('Total Column Holes: ',         column_holes)
    # print('Total Weighted Column Holes : ',weighted_column_holes)
    # print('Total Column Hole Depths :',   column_hole_depths)
    # print('Min Column Hole Depth :',      min_column_hole_depths)
    # print('Max Column Hole Depth :',      max_column_hole_depths)
    # print('Total Column Transitions :',   column_transitions)
    # print('Total Row Transitions :',      total_row_transition)
    # print('Total Column Heights :',       total_height)
    # print('Pile Height :',                pile_height)
    # print('Column Height Spread :',       column_height_spread)
    # print('Total Solid Cells  :',         total_solid_cells)
    # print('Total Weighted Solid Cells :', total_weighted_solid_cells)
    # print('Column Height Variance :',     bumpiness)
    # print('Total Comboable Lines :',      total_comboable_lines)
    # print('Start Index of Well :',        start_index_of_well)
    # print('Total Well Size :',            well_scores[2])
    # print('Total Weighted Well Size :',   well_scores[3])
    # print('Average Well Width :',         well_scores[4])
    # print('Number of Partial Lines :',    well_scores[5])
    # print('Total Uneven Well Row :',      well_scores[6])
    # print('Number of Wells :',            ls_comboable_lines)
    # print('Highest number of Wells :',    max(ls_comboable_lines) if ls_comboable_lines else 0,)
    # print('Top Well Height :',            top_well_height)
    # print('Total Cells Above Well :',     cells_above_well)
    # print('Total Stack Holes :',          stack_holes)
    # print('Height Of Tetris Start :',     all_column_sizes)
    #
    #
    # print('Punishes :',                   punishes)


    return [
            sum(column_holes),
            parity,
            bumpiness,
            # stack_holes,
            total_comboable_lines,
            cells_above_well,
            cells_in_well,
            well_holes,
            sum(all_well_hole_cells),
            sum_wells_above_three,
            sum(punish_long_stack_holes)

            # correct_well_index
        ] + total_bad_shapes\
           + [[punish_long_tetris_hole, well_holes, punish_clearing_lines, punish_bad_bumpiness, sum(column_holes)]]


def bot_switch_heuristics(columns, board):

    bumpiness = 0
    heights_list = []

    highest_garbage_hole_index = [Logic.config['rows'], 0]

    matrix_height = Logic.config['rows']

    well_scores = h_calc_combowell(board)
    well_holes = 0
    all_well_hole_cells = []
    all_long_holes = []
    column_hole = 0

    total_comboable_lines = well_scores[0]
    start_index_of_well = 6
    start_well = start_index_of_well # index of well before well starts , different from start_well above
    end_well = Logic.config['cols'] - 1 # index of well before well ends , different from end_well above

    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height
        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                if highest_cell_index == matrix_height:
                    if not (i > start_well and i <= end_well):
                        heights_list.append(matrix_height - j)  # for column height variance
                    highest_cell_index = j

            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1:  # calculate column transition and holes

                if board[j + 1][i] == 0 and board[j][i] != 0 :#and j + 1 != matrix_height - 1:  # Check hole, hole at j+1 if true don't check hole at most bottom row in case of bad starts,

                    if (i > start_well and i <= end_well) and start_well != -2 and board[j + 1].count(8) != Logic.config['cols'] - 1:
                        well_holes += 1 # punish hole IN well differently
                        # print('yes')
                        well_cells = hole_well_cells(board, j + 1, i)
                        # print(j + 1, i)
                        all_well_hole_cells.append(well_cells)
                    else:
                        if board[j + 1].count(8) != Logic.config['cols'] - 1:
                            column_hole += 1
                            long_holes = hole_well_cells(board, j + 1, i)
                            # print(j + 1, i)
                            all_long_holes.append(long_holes)


        else:

            if not (i > start_well and i <= end_well) or start_well == -2: # and start_well != -1:
                if highest_cell_index == matrix_height:  # colmun of empty cells
                    heights_list.append(0)  # add 0 to heights if no block detected


    for x in range(len(heights_list) - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])

    average_height = float(sum(heights_list) / len(heights_list))


    total_comboable_lines = total_comboable_lines + punish_holes_for_comboable_lines(all_long_holes)

    # for i in range(sum(all_long_holes)):
    #     total_comboable_lines -= i+1
    # print(heights_list)
    # return [bumpiness, total_comboable_lines, matrix_height - highest_garbage_hole_index[0] ,max(heights_list)]
    return [bumpiness, total_comboable_lines, max(heights_list) , average_height, column_hole, well_holes ]

def h_death(board):
    if board[0].count(0) != Logic.config['cols']:
        return -9999999
    else:
        return 0

def f_score(line_clear, args):
    score, time_left, counter = args

    if time_left <= 0:
        counter = 0
        time_left = 0

    counter_to_lineclear = [0, 0, 0, 0, 0, 0, 1, 8, 80, 500, 5000, 30000, 30000, 30000, 30000]
    # counter_to_lineclear = [0, 0, 0, 0, 1, 1, 2, 3, 4, 6, 10, 30000, 30000, 30000, 30000]
    base_time = [0, 6, 4, 2, -1, -4, -4, -8, -8, -12, -18, -18, -24, -24, -24]
    bonus_time = [0, 12, 6, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    loss_time = 4

    if counter > 14:
        counter = 14
    if line_clear == 0:
        if base_time[counter] < 0:
            time_left -= loss_time - base_time[counter]
        else:
            time_left -= loss_time
        return score, time_left, counter
    else:
        counter += 1
        time_left = time_left + base_time[counter] + (bonus_time[counter] * line_clear)
        score += counter_to_lineclear[counter]
        return score, time_left, counter


def f_compute_score_combowell(heuristics, board, line_clear, counter):
    weights = [1, 1, 1, 1, 0.5]




    columns = getAllColumns(board)
    eval_data = h_eval(columns, board, line_clear)
    punish_ls = eval_data[-1]


    assert (len(weights) == len(punish_ls))

    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)

    values =  eval_data[:-1] + [h_death(board)]

    # print(eval_data[:-1])
    # print(punish_ls)
    assert (len(heuristics) == len(values))


    position_score = sum(x * y for x, y in zip(heuristics, values)) #evaluation for current board state

    punish_score = sum(x * y for x, y in zip(weights, punish_ls))

    # print(position_score)

    return position_score - (punish_score * 1000)

def f_evaluate_combowell(board_old, shape_indexes, heuristics, counter):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)


    first_layer_scores_and_move = []
    first_layer_depth = 5

    scores_and_move = []

    for boards in firstlayer_boards:
        score = f_compute_score_combowell(heuristics, boards[0], boards[1], counter)
        first_layer_scores_and_move.append([score] + boards)
        # first_layer_scores_and_move
        # new_board = Logic.all_moves_first(boards[0], next_shape_index)

    first_layer_scores_and_move.sort()
    top_boards = first_layer_scores_and_move[-first_layer_depth:]
    # print(top_boards)

    for boards in top_boards:
        if h_death(boards[1]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[2], boards[3], boards[4]])
            continue
        new_board = Logic.all_moves_first(boards[1], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score_combowell(heuristics, new[0], new[1] + boards[2], counter) # new[1] + boards[1] is line clear from old board + line clear from new board

            scores_and_move.append([score, boards[2], boards[3], boards[4]])

    return scores_and_move