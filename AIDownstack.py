import Logic
from random import randrange as rand
from random import sample
import GUI
import numpy as np
#from sklearn import preprocessing
from operator import xor
from math import factorial
from math import ceil

# PROBLEM WITH DOWNSTACK AI
# 1) change to emphasize low height
# 2) stack on top on own garbage hole sometimes
# 3) calculate distance between row transitions
# 3) allow ai to make hole when a hole is underneath
# 5) calculate parity






'''To add new evaluation function, add the name to the array below and add the calculation to the managing function'''



evaluation_function = ['line_clear', #managed by h_eval
                       'well_cells',
                       'deep_wells',
                       'column_holes',
                       'weighted_column_holes',
                       'column_hole_depths',
                       'min_column_hole_depth',
                       'max_column_hole_depth',
                       'total_column_transition',
                       'total_row_transitions',
                       'total_column_heights',
                       'pile_height',
                       'column_height_spread',
                       'total_solid_cells',
                       'total_weighted_solid_cells',
                       'column_height_variance',
                       'cell_hole_score',
                       'garbage_lines',
                       'pieces_lines',
                       'pieces_lines_weight',
                       'row_trans_weights',
                       'cells_above_1st_hole',
                       'cells_above_2nd_hole',
                       'cells_above_3rd_hole',
                       'cells_above_4th_hole',


                       'counter', #managed by h_calc_combowell
                       # 'total_counter_weights'
                       ]



def getColumn(board, column):
    return [board[i][column] for i in range(len(board))]


def getAllColumns(board):
    return [getColumn(board, i) for i in range(len(board[0]))]


def if_row_transition(board, x, y):
    if y == 0 or y == Logic.config['cols'] - 1:
        return board[x][y] == 0
    else:
        return xor(board[x][y] != 0, board[x][y + 1] != 0)



def row_transition_indexes(row):
    # return index of all row transitions in row
    index_list = []
    new_row = [1] + row + [1]
    for y in range(len(new_row)-1):
        index_list.append(y - 1) if xor(new_row[y] != 0, new_row[y + 1] != 0) else None
    return index_list


def if_column_transition(board, x, y):
    # x = height
    assert (x != Logic.config['rows'] - 1)
    return xor(board[x][y] != 0, board[x + 1][y] != 0)  # Make sure cells in adjacent column is different



def calc_well_cells(board, x, y):
    total = 0
    first_garbage = False
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x > -1:
        if y == 0:
            if board[x][y + 1] != 0:
                if board[x].count(8) != Logic.config['cols'] - 1:
                    total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:
            if board[x][y - 1] != 0:
                if board[x].count(8) != Logic.config['cols'] - 1:
                    total += 1
            else:
                break
        else:
            if board[x][y + 1] != 0 and board[x][y - 1] != 0 :
                if board[x].count(8) != Logic.config['cols'] - 1:
                    # print(board[x][y + 1], board[x][y + 1])
                    total += 1
            else:
                break
        x -= 1
    return total



def one_column_sizes(columns, highest_garbage_hole_index):
    tetris_column_sizes = []
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        column_size = 0
        for j in reversed(range(0, highest_garbage_hole_index[0])):

            if i == Logic.config['cols'] - 1:
                if columns[i][j] == 0 and columns[i - 1][j] != 0:
                    column_size += 1
            elif i == 0:
                if columns[i][j] == 0 and columns[i + 1][j] != 0:
                    column_size += 1
            else:
                if columns[i][j] == 0 and columns[i + 1][j] != 0 and columns[i - 1][j] != 0:
                    column_size += 1
        tetris_column_sizes.append(column_size)
    return tetris_column_sizes

def highest_cell_above_hole(col):
    for i in range(len(col)):
        if col[i] !=  0:
            return i
    else:
        return len(col)

def cell_hole_score(data):
    # row_weights = [10 ** i for i in range(4)][::-1]
    # if len(row_weights) < len(data):
    #     for i in range(len(data) - len(row_weights)):
    #         row_weights.append(1)
    row_weights = [2 ** i for i in range(len(data))][::-1]
    count = 1
    max_depth = 4
    total_score = 0
    for i in range(len(data)):
        # if count == max_depth: break
        if i != 0 and data[i][0] != data[i-1][0]:
            count += 1
        total_score += (data[i][1]) * row_weights[count - 1]
        # print(data[i][1], '*', row_weights[count - 1], '  Total score is ' ,total_score)
    # return 1
    return total_score



def four_highest_holes(data, highest_garbage_hole_index, garbage = True):
    if garbage == True:
        data = [item for item in data if highest_garbage_hole_index[0] <= item[0]]
    elif garbage == False:
        data = [item for item in data if highest_garbage_hole_index[0] > item[0]]
    count = 0
    max_depth = 4
    cells_above_holes = [0,0,0,0]
    # cells_above_holes = [0,0]
    for i in range(len(data)):
        if i != 0 and data[i][0] != data[i-1][0]:
            count += 1
        if count == max_depth: break
        cells_above_holes[count] += data[i][1]

    return cells_above_holes

def garbage_number(board):
    for i in range(len(board)):
        if board[i].count(8) == Logic.config['cols'] - 1:
            return len(board) - i
    else:
        return 0

def pile_height(board):
    for i in range(len(board)):
        if board[i].count(0) != Logic.config['cols']:
            return len(board) - i
    else:
        return 0

def h_eval(columns, board):
    total_height = 0
    bumpiness = 0
    heights_list = []
    all_well_cells = []
    column_transitions = []
    column_holes = []
    weighted_column_holes = []
    column_hole_depths = []
    min_column_hole_depths = []
    max_column_hole_depths = []
    cells_above_holes = []
    total_solid_cells = 0
    total_weighted_solid_cells = 0
    total_row_transition = 0
    row_trans_weights = 0

    highest_garbage_hole_index = [Logic.config['rows'], 0]


    matrix_height = Logic.config['rows']

    # j correspond to board height and i to board row
    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height - 1
        column_trans = 0
        column_hole = 0
        weighted_column_hole = 0
        column_hole_depth = 0
        min_column_hole_depth = Logic.config['rows'] - 1
        max_column_hole_depth = 0
        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                if board[j].count(8) != Logic.config['cols'] - 1:
                    total_solid_cells += 1
                    total_weighted_solid_cells += matrix_height - j

                if highest_cell_index == matrix_height - 1:

                    well_cells = calc_well_cells(board, j - 1, i)  # for cell well count per column
                    all_well_cells.append(well_cells)
                    highest_cell_index = j  # only change this at 1st iteration
                    heights_list.append(matrix_height - j)  # for column height variance

            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1:  # calculate column transition and holes
                if if_column_transition(board, j, i): column_trans += 1

                if board[j + 1][i] == 0 and board[j][i] != 0:  # Check hole, hole at j+1 if true
                    if board[j].count(8) != Logic.config['cols'] - 1:
                        column_hole += 1
                        weighted_column_hole += j + 1 + 1
                        column_hole_depth += j + 1 - highest_cell_index
                    hole_cell_count = j+1 - highest_cell_above_hole(columns[i][:j+1])
                    cells_above_holes.append((j + 1,hole_cell_count))

                    if column_hole_depth < min_column_hole_depth : min_column_hole_depth = column_hole_depth
                    if column_hole_depth > max_column_hole_depth : max_column_hole_depth = column_hole_depth
        else:
            if highest_cell_index == matrix_height - 1:  # colmun of empty cells
                heights_list.append(0)  # add 0 to heights if no block detected
            column_holes.append(column_hole)
            weighted_column_holes.append(weighted_column_hole)
            column_hole_depths.append(column_hole_depth)
            column_transitions.append(column_trans)
            min_column_hole_depths.append(min_column_hole_depth)
            max_column_hole_depths.append(max_column_hole_depth)


    heights_list = [height - (matrix_height - highest_garbage_hole_index[0]) for height in heights_list]
    heights_list = [height*(height>=0) for height in heights_list]
    # heights_list[highest_garbage_hole_index[1]] += 1 # each garbage line has one hole, this calc corrects for that
    pile_height = max(heights_list) + (matrix_height - highest_garbage_hole_index[0])
    column_height_spread = max(heights_list) - min(heights_list)
    for x in range(Logic.config['cols'] - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])

    row_count = 0
    for i in range(Logic.config['rows'] - pile_height, highest_garbage_hole_index[0]): # only count the pieces players placed and ignore garbage
        this_row_transitions = 0
        row_count += 1
        row_trans_count = len(row_transition_indexes(board[i]))
        total_row_transition += row_trans_count
        this_row_transitions += row_trans_count
        row_trans_weights += this_row_transitions * row_count


    # column_wells = one_column_sizes(columns, highest_garbage_hole_index)

    sorted_cell_above_holes = sorted(cells_above_holes)
    pieces_height = pile_height - (matrix_height - highest_garbage_hole_index[0])
    if pieces_height < 0:
        pieces_height = pile_height

    if all_well_cells == []:
        all_well_cells.append(0)

    all_well_cells = one_column_sizes(columns, highest_garbage_hole_index)

    four_garbage_holes = four_highest_holes(sorted_cell_above_holes, highest_garbage_hole_index)

    four_pieces_holes = four_highest_holes(sorted_cell_above_holes, highest_garbage_hole_index, False)

    two_wells = sum([1 for well in all_well_cells if well > 1])

    punishes = [sum(column_holes),
                pieces_height,
                0 if two_wells < 2 else two_wells,
                sum([1 for well in all_well_cells if well > 2]),
                matrix_height - highest_garbage_hole_index[0],
                four_garbage_holes[0],
                four_garbage_holes[1],
                sum(four_pieces_holes),
                1 if two_wells == 1 else 0
                ]


    # four_highest_holes(sorted_cell_above_holes)

    # print('Punishes: ',   punishes)
    # print('Cell hole count: ',              sorted_cell_above_holes)
    #
    #
    # print('Heights: ', heights_list)
    #
    # print('Total Well Cells: ',           all_well_cells)
    # print('Total Deep Wells: ',           sum(1 for i in all_well_cells if i >= 3))
    # print('Total Column Holes: ',         column_holes)
    # print('Total Weighted Column Holes : ', sum(weighted_column_holes))
    # print('Total Column Hole Depths :',   column_hole_depths)
    # print('Min Column Hole Depth :',      min(min_column_hole_depths))
    # print('Max Column Hole Depth :',      max(max_column_hole_depths))
    # print('Total Column Transitions :',   sum(column_transitions))
    # print('Total Row Transitions :',      total_row_transition)
    # print('Total Column Heights :',       sum(heights_list))
    # print('Pile Height :',                pile_height)
    # print('Column Height Spread :',       column_height_spread)
    # print('Total Solid Cells  :',         total_solid_cells)
    # print('Total Weighted Solid Cells :', total_weighted_solid_cells)
    # print('Column Height Variance :',     bumpiness)
    # # print('Cell hole Score: ',              cell_hole_score(sorted_cell_above_holes))
    # print('Garbage Lines :',                matrix_height - highest_garbage_hole_index[0])
    # print('Pieces lines ' , pieces_height)
    # # print('Pieces lines weight ' , factorial(pieces_height))
    # print('Row Trans Weights ' , row_trans_weights)
    # print('Cells above holes: ', four_highest_holes(sorted_cell_above_holes,highest_garbage_hole_index))

    return [
            sum(all_well_cells),
            sum(1 for i in all_well_cells if i >= 3),
            sum(column_holes),
            sum(weighted_column_holes),
            sum(column_hole_depths),
            min(min_column_hole_depths),
            max(max_column_hole_depths),
            sum(column_transitions),
            total_row_transition,
            sum(heights_list),
            pile_height,
            column_height_spread,
            total_solid_cells,
            total_weighted_solid_cells,
            bumpiness,
            cell_hole_score(sorted_cell_above_holes),
            matrix_height - highest_garbage_hole_index[0],
            pieces_height,
            factorial(pieces_height),
            row_trans_weights
            ] + four_highest_holes(sorted_cell_above_holes, highest_garbage_hole_index) + [punishes]





def h_death(board):
    if board[0].count(0) != Logic.config['cols']:
        return -9999999
    else:
        return 0


def f_score(line_clear, args):
    score, time_left, counter = args

    if time_left <= 0:
        counter = 0
        time_left = 0

    #counter_to_lineclear = [0, 0, 0, 0, 0, 0, 1, 8, 80, 500, 5000, 30000, 30000, 30000, 30000]
    counter_to_lineclear = [0, 0, 0, 0, 1, 1, 2, 3, 4, 6, 10, 30000, 30000, 30000, 30000]
    base_time = [0, 6, 4, 2, -1, -4, -8, -8, -12, -12, -18, -18, -24, -24, -24]
    bonus_time = [0, 12, 6, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    loss_time = 4

    if counter > 14:
        counter = 14
    if line_clear == 0:
        if base_time[counter] < 0:
            time_left -= loss_time - base_time[counter]
        else:
            time_left -= loss_time
        return score, time_left, counter
    else:
        counter += 1
        time_left = time_left + base_time[counter] + (bonus_time[counter] * line_clear)
        score += counter_to_lineclear[counter]
        return score, time_left, counter




def f_compute_score(heuristics, board, line_clear, counter):
    weights = [1.5 ,0.5, 1, 1, 2, 0.6, 0.3, 1, 0]
    #weights = [0 ,0, 0, 0, 0, 0, 0, 0, 0]

    columns = getAllColumns(board)
    eval_data = h_eval(columns, board)
    punish_ls = eval_data[-1]

    assert(len(weights) == len(punish_ls))

    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)

    values = [line_clear] + eval_data[:-1] + [counter] + [h_death(board)]

    assert (len(heuristics) == len(values))

    position_score = sum(x * y for x, y in zip(heuristics, values)) #evaluation for current board state

    punish_score = sum(x * y for x, y in zip(weights, punish_ls)) # punish heavily for bad board states, 0 if there isnt any in punish_ls

    # return position_score - (punish_score * abs(position_score))
    return position_score - (punish_score * 1000)




def f_evaluate2(all_moves, heuristics, counter):
    # all_moves = Logic.all_moves_second(board_old, shape_indexes)
    scores_and_move = []

    for moves in all_moves:
        # try:
        #     print(moves[0])
        # except:
        #     print(moves)
        #     raise Exception('FAILED')
        #print(moves[0])
        score = f_compute_score(heuristics, moves[0], moves[1],
                                          counter)  # new[1] + boards[1] is line clear from old board + line clear from new board

        scores_and_move.append([score, moves[1], moves[2], moves[3]])

    return scores_and_move


def f_evaluate(board_old, shape_indexes, heuristics, counter):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)
    scores_and_move = []

    for boards in firstlayer_boards:
        # if h_death(boards[0]) != 0:  # check for death at 1st layer and put score at -9999999
        #     scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
        #     continue
        new_board = Logic.all_moves_first(boards[0], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score(heuristics, new[0], new[1] + boards[1], counter) # new[1] + boards[1] is line clear from old board + line clear from new board

            scores_and_move.append([score, boards[1], boards[2], boards[3]])

    return scores_and_move

def test_score():
    line_seq = [1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 2, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    score = 0
    time_left = 0
    counter = 0
    for i in line_seq:
        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
        score, time_left, counter = f_score(i, (score, time_left, counter))
    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))


def count_cells(board):

    total_cells = 0
    for rows in board:
        for cells in rows:
            if cells != 0:
                total_cells += 1

    return total_cells