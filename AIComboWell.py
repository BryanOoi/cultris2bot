import Logic
from random import randrange as rand
from random import sample
import GUI
import numpy as np
#from sklearn import preprocessing
from operator import xor
from math import ceil

# PROBLEM WITH COMBOWELL AI
# 1) doesnt put pieces at combo well when it creates new wells
# 6) doesnt punish stacking over holes

# Make sure to tweak the punish weights so that it favors holes/breaking at combo wells


# IMPORTANT, one_column_sizes(columns) IS BUGGED AS IT COUNTS GARBAGE

'''To add new evaluation function, add the name to the array below and add the calculation to the managing function'''


evaluation_function = ['line_clear', #managed by h_eval
                       'well_cells',
                       'deep_wells',
                       'column_holes',
                       'weighted_column_holes',
                       'column_hole_depths',
                       'min_column_hole_depth',
                       'max_column_hole_depth',
                       'total_column_transition',
                       'total_row_transitions',
                       'total_column_heights',
                       'pile_height',
                       'column_height_spread',
                       'total_solid_cells',
                       'total_weighted_solid_cells',
                       'column_height_variance',


                       # 'counter',
                       # 'next_piece',
                       'total_comboable_lines',
                       'start_index_of_well',
                       'total_well_size',
                       'total_weighted_well_size',
                       'average_well_width',
                       'no_of_partial_lines',
                       'total_uneven_well_row',
                       'no_of_wells',
                       'highest_comboable_line',
                       'top_well_height',
                       'cells_above_well',
                       'stack_holes',
                       'max_tetriswell_height',
                       ]


def getColumn(board, column):
    return [board[i][column] for i in range(len(board))]


def getAllColumns(board):
    return [getColumn(board, i) for i in range(len(board[0]))]


def if_row_transition(board, x, y):
    if y == 0 or y == Logic.config['cols'] - 1:
        return board[x][y] == 0
    else:
        return xor(board[x][y] != 0, board[x][y + 1] != 0)



def row_transition_indexes(row):
    # return index of all row transitions in row
    index_list = []
    new_row = [1] + row + [1]
    for y in range(len(new_row)-1):
        index_list.append(y - 1) if xor(new_row[y] != 0, new_row[y + 1] != 0) else None
    return index_list


def if_column_transition(board, x, y):
    # x = height
    assert (x != Logic.config['rows'] - 1)
    return xor(board[x][y] != 0, board[x + 1][y] != 0)  # Make sure cells in adjacent column is different


def calc_well_cells(board, x, y):
    total = 0
    if board[x][y] != 0: # cannot be a well when not empty
        return total
    # count till height of 17
    while x > -1:
        if y == 0:
            # print(board[x], board[x][y + 1])
            if board[x][y + 1] != 0:
                total += 1
            else:
                break
        elif y == Logic.config['cols'] - 1:
            if board[x][y - 1] != 0:
                total += 1
            else:
                break
        else:
            if board[x][y + 1] != 0 and board[x][y - 1] != 0:
                total += 1
            else:
                break
        x -= 1
    return total

#TODO: to punish very bad moves, put into f_evaluate()

def calc_punish_wells(all_well_cells,start_well,end_well ):
    punish = [0,0,0]

    if all_well_cells.count(2) > 1: punish[0] = 1 # punish too many size 2 wells

    for i in range(len(all_well_cells)):
        if i >= start_well and i <= end_well:  # check if 1-width well size in combo well
            if all_well_cells[i] > 6:
                punish[1] = 1

        else:
            if all_well_cells[i] >= 3:
                punish[2] = 1

    return punish

# print(calc_punish_wells([5, 0, 0, 2, 0, 3, 0, 0, 0, 0],-1,1))

# MAYBE PUT THIS IN EVALUATION FUNCTION?
def one_column_sizes(columns, highest_garbage_hole_index):
    tetris_column_sizes = []
    matrix_height = Logic.config['rows']
    for i in range(Logic.config['cols']):
        column_size = 0
        for j in reversed(range(0, highest_garbage_hole_index[0])):

            if i == Logic.config['cols'] - 1:
                if columns[i][j] == 0 and columns[i - 1][j] != 0:
                    column_size += 1
            elif i == 0:
                if columns[i][j] == 0 and columns[i + 1][j] != 0:
                    column_size += 1
            else:
                if columns[i][j] == 0 and columns[i + 1][j] != 0 and columns[i - 1][j] != 0:
                    column_size += 1
        tetris_column_sizes.append(column_size)
    return tetris_column_sizes


#TODO: ADD total_uneven_well_row to evals
#TODO: Make fitness function for garbage only
def h_punish_top_mess(ls_comboable_lines,cells_above_well,stack_holes,column_holes, all_column_sizes, all_well_cells, start_well, end_well):



    #calc_punish_well returns [x , y , z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well
    if_cells_above_well = 1 if cells_above_well > 0 else 0

    if_too_many_one_column_sizes = 1 if max(all_column_sizes) > 6 else 0
    # if_too_many_one_column_sizes = 1 if len(combo_well_cells)!= 0 and max(combo_well_cells) > 6 else 0 #dont have more than 6 tetris holes ANYWHERE in stack

    if_too_many_combo_wells = 1 if (len(ls_comboable_lines) > 1) else 0 #punish 2 wells and above

    return [if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + calc_punish_wells(all_well_cells, start_well, end_well)




def h_calc_combowell(board, punish_info = 'no'):
    # only calculate well depth after first non well row and stops at next non well row
    # well max is 4
    # combo-able lines in a row
    max_well_size = 4
    total_comboable_lines = 0 # combo-able lines
    start_index_of_well = -2
    start_well_size = 0
    average_well_width = -1
    total_weighted_well_size = 0 # maximising tetris start?
    total_well_size = 0
    start_well = -1
    end_well = Logic.config['cols'] - 1
    top_well_height = -1
    partial_well_score = 0.6
    well_threshold = 3
    no_of_partial_lines = 0
    ls_comboable_lines = []


    past_well_size = max_well_size
    total_uneven_well_row = 0

    for i in range(Logic.config['rows']):


        if board[i].count(0) == Logic.config['cols']: continue # don't bother if empty row
        if board[i].count(8) == Logic.config['cols'] - 1: continue # don't bother if garbage line
        row_trans = row_transition_indexes(board[i])
        well_size = row_trans[1] - row_trans[0]
        assert(len(row_trans) >= 2)



        #Iterates rows starting from first time a well is detected
        if (len(row_trans) == 2 and well_size <= max_well_size) and (row_trans[0] >= start_well and row_trans[1] <= end_well ) : # this means well does exist and #check if current row well is inside past row well

            if (well_size <= past_well_size):
                total_comboable_lines += 1 # counting height starts here
            else:
                total_uneven_well_row += 1
                total_comboable_lines += partial_well_score
            total_well_size += well_size
            average_well_width = float(total_well_size / total_comboable_lines)
            total_weighted_well_size += total_comboable_lines * (max_well_size + 1 - well_size)

            past_well_size = well_size

            if top_well_height == -1: # only care about top of well
                top_well_height = i

            # if start_well == -1:
            #     start_well = row_trans[0]
            #     end_well = row_trans[1]

            start_well = row_trans[0]
            end_well = row_trans[1]

            if start_index_of_well == -2: # well size starts from two, TEST this
                start_index_of_well = start_well
                start_well_size = well_size
            # (start_well >= row_trans[0] and end_well <= row_trans[1])


        elif (len(row_trans) == 2 and well_size <= max_well_size) and i != Logic.config['rows'] - 1: # 'detected well, but outside of stack', start new stack cycle , this is to stop the matrix from having 2 different wells
            # print('well outside stack')
            # if total_comboable_lines > 1: # problem here
            ls_comboable_lines.append(total_comboable_lines) #stop counting comboable lines and start new cycle
            total_comboable_lines = 0 # or it can be 1?
            start_well = row_trans[0]
            end_well = row_trans[1]
            past_well_size = max_well_size
            # start_well = -1  # index of well before well starts
            # end_well = Logic.config['cols'] - 1  # index of well before well ends

        else:
            if all(row_trans[i] >= start_well and row_trans[i] <= end_well for i in range(len(row_trans))) and start_well != -1 and total_comboable_lines >= well_threshold: # check if all row_trans is inside the well
                # print('non well row but transitions inside well')
                # total_comboable_lines += partial_well_score - (
                #             partial_well_score * (no_of_partial_lines ** 2))  # punish partial lines exponentially
                no_of_partial_lines += 1
                if total_comboable_lines < 0: total_comboable_lines = 0
            elif total_comboable_lines > 0 and i != Logic.config['rows'] - 1 : # random garbage line doesnt fit in combo well and not a well itself
                # if total_comboable_lines > 1:
                ls_comboable_lines.append(total_comboable_lines)  # stop counting comboable lines and start new cycle
                total_comboable_lines = 0 # or it can be 1?
                start_well = row_trans[0]
                end_well = row_trans[1]
                past_well_size = max_well_size
                # start_well = -1  # index of well before well starts
                # end_well = Logic.config['cols'] - 1  # index of well before well ends

        # print(row_trans, start_well, end_well, total_comboable_lines)
    if total_comboable_lines > 0:
        ls_comboable_lines.append(total_comboable_lines)


    # print(total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width)
    if punish_info == 'yes':
        return [ls_comboable_lines, total_comboable_lines, top_well_height, start_well, end_well]
    else:
        return [total_comboable_lines,start_index_of_well,total_well_size,total_weighted_well_size, average_well_width, no_of_partial_lines,total_uneven_well_row, ls_comboable_lines,top_well_height,start_well, end_well,start_well_size]





def h_eval(columns, board):
    total_height = 0
    bumpiness = 0
    heights_list = []
    all_well_cells = []
    column_transitions = []
    column_holes = []
    weighted_column_holes = []
    column_hole_depths = []
    min_column_hole_depths = []
    max_column_hole_depths = []
    combo_well_cells = []
    total_solid_cells = 0
    total_weighted_solid_cells = 0
    total_row_transition = 0

    highest_garbage_hole_index = [Logic.config['rows'], 0]

    matrix_height = Logic.config['rows']

    well_scores = h_calc_combowell(board)

    # returns [total_comboable_lines, start_index_of_well, total_well_size, total_weighted_well_size, average_well_width,
    #  no_of_partial_lines, total_uneven_well_row, ls_comboable_lines, top_well_height, start_well, end_well]
    total_comboable_lines = well_scores[0]
    start_index_of_well = well_scores[1]
    ls_comboable_lines = well_scores[7]
    top_well_height = well_scores[8]
    start_well = start_index_of_well # index of well before well starts , different from start_well above
    end_well = start_well + well_scores[11] # index of well before well ends , different from end_well above

    well_threshold = 3

    cells_above_well = 0
    stack_holes = 0

    # check for any cells above well
    if top_well_height > 0 and total_comboable_lines >= well_threshold:
        for i in range(0,top_well_height):
            square_abv_well = board[i][start_well + 1:end_well+1]
            cells_above_well += len(square_abv_well) - square_abv_well.count(0) #count solid cells above well


    # j correspond to board height and i to board row
    for i in range(Logic.config['cols']):
        highest_cell_index = matrix_height - 1
        column_trans = 0
        column_hole = 0
        weighted_column_hole = 0
        column_hole_depth = 0
        min_column_hole_depth = Logic.config['rows'] - 1
        max_column_hole_depth = 0
        for j in range(matrix_height):
            if board[j].count(8) == Logic.config['cols'] - 1 and highest_garbage_hole_index[0] == Logic.config['rows']:
                highest_garbage_hole_index = [j, board[j].index(0)]

            if columns[i][j] != 0: # DOES NOT CALCULATE TETRIS COLUMN
                total_solid_cells += 1
                total_weighted_solid_cells += matrix_height - j

                if highest_cell_index == matrix_height - 1:
                    well_cells = calc_well_cells(board, j - 1, i)  # for cell well count per column
                    if not (i > start_well and i <= end_well):
                        all_well_cells.append(well_cells)
                        total_height += matrix_height - j  # for total height
                        heights_list.append(matrix_height - j)  # for column height variance
                    else:
                        combo_well_cells.append(well_cells)
                    highest_cell_index = j  # only change this at 1st iteration


            if highest_cell_index < matrix_height - 1 and j != matrix_height - 1:  # calculate column transition and holes
                if if_column_transition(board, j, i): column_trans += 1

                if board[j + 1][i] == 0 and board[j][i] != 0 :#and j + 1 != matrix_height - 1:  # Check hole, hole at j+1 if true don't check hole at most bottom row in case of bad starts,

                    if (i > start_well and i <= end_well) and start_well != -2 and board[j + 1].count(8) != Logic.config['cols'] - 1:
                        stack_holes += 1 # punish hole IN well differently
                    else:
                        if board[j].count(8) == Logic.config['cols'] - 1: continue #don't bother if garbage line
                        column_hole += 1
                        weighted_column_hole += j + 1 + 1
                        column_hole_depth += j + 1 - highest_cell_index


                    if column_hole_depth < min_column_hole_depth : min_column_hole_depth = column_hole_depth
                    if column_hole_depth > max_column_hole_depth : max_column_hole_depth = column_hole_depth
        else:
            if not (i > start_well and i <= end_well) or start_well == -2: # and start_well != -1:
                if highest_cell_index == matrix_height - 1:  # colmun of empty cells
                    heights_list.append(0)  # add 0 to heights if no block detected
                column_holes.append(column_hole)
                weighted_column_holes.append(weighted_column_hole)
                column_hole_depths.append(column_hole_depth)
                column_transitions.append(column_trans)
                min_column_hole_depths.append(min_column_hole_depth)
                max_column_hole_depths.append(max_column_hole_depth)


    pile_height = max(heights_list)
    column_height_spread = max(heights_list) - min(heights_list)

    for x in range(len(heights_list) - 1):
        bumpiness += abs(heights_list[x] - heights_list[x + 1])


    for i in range(Logic.config['rows'] - pile_height, Logic.config['rows']):
        row_trans_count = len(row_transition_indexes(board[i]))
        total_row_transition += row_trans_count
        # for j in range(Logic.config['cols']):
        #
        #     if if_row_transition(board, i, j): total_row_transition += 1

    all_column_sizes = one_column_sizes(columns, highest_garbage_hole_index) # combo_well_cells now obsolete
    punishes = h_punish_top_mess(ls_comboable_lines,cells_above_well,stack_holes,column_holes, all_column_sizes, all_well_cells, start_well, end_well)
    # punishes are [if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + [x,y,z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well


    # print('Start Well : ',                start_well)
    # print('End Well : ',                  end_well)
    # print('Total Heights : ',             heights_list)
    #
    #
    # print('Total Well Cells: ',           all_well_cells)
    # print('Total Deep Wells: ',           sum(1 for i in all_well_cells if i >= 3))
    # print('Total Column Holes: ',         column_holes)
    # print('Total Weighted Column Holes : ',weighted_column_holes)
    # print('Total Column Hole Depths :',   column_hole_depths)
    # print('Min Column Hole Depth :',      min_column_hole_depths)
    # print('Max Column Hole Depth :',      max_column_hole_depths)
    # print('Total Column Transitions :',   column_transitions)
    # print('Total Row Transitions :',      total_row_transition)
    # print('Total Column Heights :',       total_height)
    # print('Pile Height :',                pile_height)
    # print('Column Height Spread :',       column_height_spread)
    # print('Total Solid Cells  :',         total_solid_cells)
    # print('Total Weighted Solid Cells :', total_weighted_solid_cells)
    # print('Column Height Variance :',     bumpiness)
    # print('Total Comboable Lines :',      total_comboable_lines)
    # print('Start Index of Well :',        start_index_of_well)
    # print('Total Well Size :',            well_scores[2])
    # print('Total Weighted Well Size :',   well_scores[3])
    # print('Average Well Width :',         well_scores[4])
    # print('Number of Partial Lines :',    well_scores[5])
    # print('Total Uneven Well Row :',      well_scores[6])
    # print('Number of Wells :',            ls_comboable_lines)
    # print('Highest number of Wells :',    max(ls_comboable_lines) if ls_comboable_lines else 0,)
    # print('Top Well Height :',            top_well_height)
    # print('Total Cells Above Well :',     cells_above_well)
    # print('Total Stack Holes :',          stack_holes)
    # print('Height Of Tetris Start :',     all_column_sizes)
    #
    #
    # print('Punishes :',                   punishes)


    return [
            sum(all_well_cells),
            sum(1 for i in all_well_cells if i >= 3),
            sum(column_holes),
            sum(weighted_column_holes),
            sum(column_hole_depths),
            min(min_column_hole_depths),
            max(max_column_hole_depths),
            sum(column_transitions),
            total_row_transition,
            total_height,
            pile_height,
            column_height_spread,
            total_solid_cells,
            total_weighted_solid_cells,
            bumpiness,
            total_comboable_lines,
            start_index_of_well,
            well_scores[2],
            well_scores[3],
            well_scores[4],
            well_scores[5],
            well_scores[6],
            len(ls_comboable_lines),
            max(ls_comboable_lines) if ls_comboable_lines else 0,
            top_well_height,
            cells_above_well,
            stack_holes,
            max(all_column_sizes) if all_column_sizes else 0,
            punishes  # ONLY ONE HERE NOT AN EVAL
        ]



def takeFirst(elem):
    return elem[0]



def h_death(board):
    if board[0].count(0) != Logic.config['cols']:
        return -9999999
    else:
        return 0


def f_score(line_clear, args):
    score, time_left, counter = args

    if time_left <= 0:
        counter = 0
        time_left = 0

    counter_to_lineclear = [0, 0, 0, 0, 0, 0, 1, 8, 80, 500, 5000, 30000, 30000, 30000, 30000]
    base_time = [0, 6, 4, 2, -1, -4, -8, -8, -12, -12, -18, -18, -24, -24, -24]
    bonus_time = [0, 12, 6, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    loss_time = 4

    if counter > 14:
        counter = 14
    if line_clear == 0:
        if base_time[counter] < 0:
            time_left -= loss_time - base_time[counter]
        else:
            time_left -= loss_time
        return score, time_left, counter
    else:
        counter += 1
        time_left = time_left + base_time[counter] + (bonus_time[counter] * line_clear)
        score += counter_to_lineclear[counter]
        return score, time_left, counter




def f_compute_score_combowell(heuristics, board, line_clear, counter):
    #weights
    #return [if_cells_above_well] + [sum(column_holes)] + [if_too_many_one_column_sizes] + [if_too_many_combo_wells] + [stack_holes] + [x,y,z]
    # x is 1 if no. of 1width and deep wells exceed 2
    # y is 1 if no. of tetris lines in combo well exceed 6
    # z is 1 if there exist tetris lines not in well
    # weights = [1 ,1, 1, 1, 0.95]
    weights = [1 ,1, 1, 1, 0.95, 1, 1, 1]

    columns = getAllColumns(board)
    eval_data = h_eval(columns, board)
    punish_ls = eval_data[-1]

    assert(len(weights) == len(punish_ls))

    if len(heuristics) == len(evaluation_function):
        heuristics.append(1.0)

    values = [line_clear] + eval_data[:-1] + [h_death(board)]

    assert (len(heuristics) == len(values))

    position_score = sum(x * y for x, y in zip(heuristics, values)) #evaluation for current board state

    punish_score = sum(x * y for x, y in zip(weights, punish_ls)) # punish heavily for bad board states, 0 if there isnt any in punish_ls

    # return position_score - (punish_score * abs(position_score))
    return position_score - (punish_score * 1000)



def f_evaluate_combowell2(all_moves, heuristics, counter):
    # all_moves = Logic.all_moves_second(board_old, shape_indexes)
    scores_and_move = []

    for moves in all_moves:
        if h_death(moves[0]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, moves[1], moves[2], moves[3]])
            continue
        score = f_compute_score_combowell(heuristics, moves[0], moves[1] + moves[1],
                                          counter)  # new[1] + boards[1] is line clear from old board + line clear from new board

        scores_and_move.append([score, moves[1], moves[2], moves[3]])

    return scores_and_move

def f_evaluate_combowell(board_old, shape_indexes, heuristics, counter, diagnostics = False):

    # eval 1 layer deep, test this
    current_shape_index, next_shape_index = shape_indexes
    firstlayer_boards = Logic.all_moves_first(board_old, current_shape_index)
    # firstlayer_scores = [(i, f_compute_score_combowell(heuristics, firstlayer_boards[i][0], firstlayer_boards[i][1], counter, next_shape_index)) for
    #                      i in range(len(firstlayer_boards))]
    # return firstlayer_scores
    scores_and_move = []

    for boards in firstlayer_boards:
        if h_death(boards[0]) != 0:  # check for death at 1st layer and put score at -9999999
            scores_and_move.append([-9999999, boards[1], boards[2], boards[3]])
            continue
        new_board = Logic.all_moves_first(boards[0], next_shape_index)
        for new in new_board:
            # eval here
            score = f_compute_score_combowell(heuristics, new[0], new[1] + boards[1], counter) # new[1] + boards[1] is line clear from old board + line clear from new board

            if not diagnostics:
                scores_and_move.append([score, boards[1], boards[2], boards[3]])
            else:
                scores_and_move.append([score, boards[1], boards[2], boards[3], new[2], new[3]])


    return scores_and_move


def test_score():
    line_seq = [1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 2, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    ##    line_seq = [4,2,1,0,1,1,0,1,1,0,1,0,1,1]
    score = 0
    time_left = 0
    counter = 0
    for i in line_seq:
        print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))
        score, time_left, counter = f_score(i, (score, time_left, counter))
    print('Score: ' + str(score) + ' Time: ' + str(time_left) + ' Counter: ' + str(counter))


def draw(board, canvas):
    for r in range(Logic.config['rows']):
        for c in range(Logic.config['cols']):
            if board[r][c] != 0:
                GUI.colour_in(c, r, 'red', canvas)
            ##board = Logic.garbage_testboard()
##print(getColumn(board,1))
####columns = getAllColumns(board)

####print(f_compute_score(test_heuristic, board, 2))
####print(h_bumpiness(columns))


##total = f_evaluate(board,(5,6),test_heuristic)
##for i in total:
##    print (i)
##    print ()

##test_score()
